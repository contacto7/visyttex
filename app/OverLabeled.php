<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OverLabeled
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $code
 * @property string $code_visyttex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereCodeVisyttex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OverLabeled whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OverLabeled extends Model
{
    protected $fillable = [
        'name', 'price', 'code', 'code_visyttex',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

    public function twoOverLabeledQuotations(){
        return $this->hasMany(Quotation::class, 'second_over_labeled_id');
    }
}
