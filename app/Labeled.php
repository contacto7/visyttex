<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Labeled
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property float $price
 * @property string $code
 * @property string $code_visyttex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereCodeVisyttex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labeled whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Labeled extends Model
{
    protected $fillable = [
        'name', 'text', 'price', 'code', 'code_visyttex',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

    public function twoLabeledQuotations(){
        return $this->hasMany(Quotation::class, 'second_labeled_id');
    }
}
