<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can view this order.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function viewThis(User $user, Order $order)
    {
        return $user->id === $order->proforma->user_id || $user->role_id === Role::ADMIN || $user->role_id === Role::WORKER || $user->role_id === Role::SUPERVISOR;
    }

    /**
     * Determine whether the user can view only his orders.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function viewOnlyHis(User $user)
    {
        return $user->role_id === Role::EXTERNAL_WORKER;
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function update(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function delete(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can view this order.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function updateThis(User $user, Order $order)
    {
        return $user->id === $order->proforma->user_id || $user->role_id === Role::ADMIN || $user->role_id === Role::WORKER || $user->role_id === Role::SUPERVISOR;
    }

    /**
     * Determine whether the user can restore the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function restore(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the order.
     *
     * @param  \App\User  $user
     * @param  \App\Order  $order
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can view any calculation.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewCalculation(User $user)
    {
        return $user->role_id === Role::ADMIN || $user->role_id === Role::SUPERVISOR;
    }
}
