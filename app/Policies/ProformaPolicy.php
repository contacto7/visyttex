<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Proforma;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProformaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any proformas.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function view(User $user, Proforma $proforma)
    {
        //
    }

    /**
     * Determine whether the user can view this proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function viewThis(User $user, Proforma $proforma)
    {
        return $user->id === $proforma->user_id || $user->role_id === Role::ADMIN || $user->role_id === Role::WORKER || $user->role_id === Role::SUPERVISOR;
    }

    /**
     * Determine whether the user can view only his  proformas.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function viewOnlyHis(User $user)
    {
        return $user->role_id === Role::EXTERNAL_WORKER;
    }

    /**
     * Determine whether the user can create proformas.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function update(User $user, Proforma $proforma)
    {
        //
    }

    /**
     * Determine whether the user can delete the proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function delete(User $user, Proforma $proforma)
    {
        //
    }

    /**
     * Determine whether the user can restore the proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function restore(User $user, Proforma $proforma)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the proforma.
     *
     * @param  \App\User  $user
     * @param  \App\Proforma  $proforma
     * @return mixed
     */
    public function forceDelete(User $user, Proforma $proforma)
    {
        //
    }
}
