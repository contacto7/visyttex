<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Measure
 *
 * @property int $id
 * @property float $height
 * @property float $width
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure whereWidth($value)
 * @mixin \Eloquent
 */
class Measure extends Model
{
    protected $fillable = [
        'height', 'width', 'code',
    ];

}
