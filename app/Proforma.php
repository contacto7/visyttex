<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Proforma
 *
 * @property int $id
 * @property string $code
 * @property string $date
 * @property int $delivery_time
 * @property int $validity_time
 * @property string $additional_details
 * @property float $subtotal
 * @property int $customer_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereAdditionalDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proforma whereValidityTime($value)
 * @mixin \Eloquent
 */
class Proforma extends Model
{
    protected $fillable = [
        'code', 'date', 'proforma_name', 'proforma_mail', 'delivery_time', 'validity_time',
        'additional_details', 'subtotal', 'customer_id', 'user_id'
    ];
    public static function boot()
    {
        parent::boot();

        static::saved(function (Proforma $proforma) {

            $proforma_id = $proforma->id;

            $correlative_number = str_pad($proforma_id, 4, '0', STR_PAD_LEFT);

            $correlative_serie = "PRESUPUESTO N° ".$correlative_number." - ".now()->year;

            $proforma::whereId($proforma_id)->update(['code' => $correlative_serie]);

        });

    }


    static function numberToRomanRepresentation($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function quotations(){
        return $this->hasMany(Quotation::class);
    }
    public function orders(){
        return $this->hasOne(Order::class);
    }
}
