<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mod
 *
 * @property int $id
 * @property string $name
 * @property float $amount
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mod whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mod extends Model
{
    const BASE = 1;
    const STICKER = 2;

    protected $fillable = [
        'name', 'amount', 'type'
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

    public function work(){
        return $this->belongsTo(Work::class);
    }
}
