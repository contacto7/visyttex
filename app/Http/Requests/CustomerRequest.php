<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'company_name' => 'required',
                    'tax_number' => 'required',
                    'address' => 'nullable',
                    'phone' => 'nullable',
                    'contact_name' => 'nullable',
                    'contact_last_name' => 'nullable',
                    'contact_cellphone' => 'nullable',
                    'contact_email' => 'nullable'
                    ];
            }
        }
    }
}
