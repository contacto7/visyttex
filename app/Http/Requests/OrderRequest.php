<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':{
                return [
                    'order_date' => 'required|date_format:Y-m-d H:i:s',
                    'delivered_date' => 'nullable|date_format:Y-m-d H:i:s',
                    'additional_details' => 'nullable',
                ];
            }
            case 'POST':{
                return [
                    'proforma_id' => [
                        'required',
                        Rule::exists('proformas','id')
                    ],
                    'order_date' => 'required|date_format:Y-m-d H:i:s',
                    'delivered_date' => 'nullable|date_format:Y-m-d H:i:s',
                    'additional_details' => 'nullable',
                ];
            }
        }
    }
}
