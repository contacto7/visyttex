<?php

namespace App\Http\Requests;

use App\Role;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role_id === Role::ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'name' => 'required',
                    'last_name' => 'required',
                    'slug' => 'nullable',
                    'email' => 'required|email',
                    'password' => 'required|min:6',
                    'phone' => 'nullable',
                    'cellphone' => 'nullable',
                    'position' => 'nullable',
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ]),
                    'remember_token' => 'nullable',



                ];
            }
        }
    }


}
