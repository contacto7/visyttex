<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'signal_id' => [
                        'required',
                        Rule::exists('signals','id')
                    ],
                    'proforma_code' => 'nullable',
                    'description' => 'nullable',
                    'height' => 'numeric',
                    'width' => 'numeric',
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'quantity' => 'numeric',
                    'unitary_amount' => 'numeric',
                    'margin_id' => [
                        'required',
                        Rule::exists('margins','id')
                    ],
                    'work_id' => [
                        'required',
                        Rule::exists('works','id')
                    ],
                    'base_id' => [
                        'required',
                        Rule::exists('bases','id')
                    ],
                    'bottom_id' => [
                        'required',
                        Rule::exists('bottoms','id')
                    ],
                    'labeled_id' => [
                        'required',
                        Rule::exists('labeleds','id')
                    ],
                    'second_labeled_id' => [
                        'required',
                        Rule::exists('labeleds','id')
                    ],
                    'over_labeled_id' => [
                        'required',
                        Rule::exists('over_labeleds','id')
                    ],
                    'second_over_labeled_id' => [
                        'required',
                        Rule::exists('over_labeleds','id')
                    ],
                    'additional_id' => [
                        'required',
                        Rule::exists('additionals','id')
                    ],
                    'mod_id' => [
                        'required',
                        Rule::exists('mods','id')
                    ],
                    'proforma_id' => [
                        'required',
                        Rule::exists('proformas','id')
                    ],
                    'packaging_supplies' => 'nullable',
                    'decrease_base' => 'nullable',
                    'decrease_bottom' => 'nullable',
                    'decrease_labeled' => 'nullable',
                    'decrease_second_labeled' => 'nullable',
                    'decrease_over_labeled' => 'nullable',
                    'decrease_second_over_labeled' => 'nullable',
                ];
            }
        }
    }
}
