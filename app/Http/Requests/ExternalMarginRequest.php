<?php

namespace App\Http\Requests;

use App\Role;
use Illuminate\Foundation\Http\FormRequest;

class ExternalMarginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role_id === Role::ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'percentage' => 'numeric|required|min:1',
                ];
            }
        }
    }


    public function messages()
    {
        return [
            'percentage.required' => 'Debes ingresar el porcentaje!',
            'percentage.min' => 'Debe ser mayor que uno!',
        ];
    }

}
