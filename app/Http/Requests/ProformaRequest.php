<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProformaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'code' => 'nullable',
                    'date' => 'required|date_format:Y-m-d H:i:s',
                    'delivery_time' => 'numeric',
                    'validity_time' => 'numeric',
                    'additional_details' => 'nullable',
                    'subtotal' => 'nullable',
                    'customer_id' => [
                        'required',
                        Rule::exists('customers','id')
                    ],
                    'user_id' => [
                        'nullable',
                        Rule::exists('customers','id')
                    ],
                ];
            }
        }
    }

}
