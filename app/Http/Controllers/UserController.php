<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Proforma;
use App\Quotation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function list(){
        $users = User::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('users.list', compact('users') );
    }

    public function admin($id){
        $user = User::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('users.admin', compact('user'));
    }

    public function filter(Request $request, User $users){

        $name = $request->has('name') ? $request->input('name'): null;
        $last_name = $request->has('last_name') ? $request->input('last_name'): null;


        $users = $users->newQuery();
        //$proformas = $proformas::with([]);

        if ($name) {
            $users->where("name",'like','%'.$name.'%');
        }

        if ($last_name) {
            $users->where("last_name",'like','%'.$last_name.'%');
        }

        $users = $users->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('users.list', compact('users'));

    }

    public function create(){
        $user = new User();

        $btnText = __("Crear usuario");
        return view('users.form', compact('user','btnText'));
    }

    public function store(UserRequest $userRequest){

        //$userRequest->merge(['user_id' => auth()->user()->id ]);
        $slug = $userRequest->input('name').'-'.$userRequest->input('last_name').'-'.Str::random(4);

        $userRequest->merge(['remember_token' => Str::random(10) ]);
        $userRequest->merge(['slug' => $slug ]);

        try {

            User::create($userRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó el usuario correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el usuario,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }

    public function edit($id){
        $user = User::where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($accounting);
        $btnText = __("Editar usuario");
        return view('users.form', compact('user','btnText'));
    }

    public function update(UserRequest $userRequest, User $user){

        try {

            $user->fill($userRequest->input())->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el usuario.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error editando el usuario,
                por favor verifique que está colocando los datos requeridos.")]);


        }
    }


}
