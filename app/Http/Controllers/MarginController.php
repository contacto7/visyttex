<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarginRequest;
use App\Margin;
use Illuminate\Http\Request;

class MarginController extends Controller
{
    public function list(){
        $margins = Margin::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('margins.list', compact('margins') );
    }
    public function create(){
        $margin = new Margin();

        return view('margins.form', compact('margin'));
    }

    public function store(MarginRequest $marginRequest){

        try {

            Margin::create($marginRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó el margen correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el margen,
                por favor verifique que está colocando correctamente el margen.")]);


        }

    }


}
