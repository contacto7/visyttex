<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Order;
use App\Proform;
use App\Proforma;
use App\Quotation;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function list(){
        $orders = Order::with([]);
        $user_auth = auth()->user();

        //POLICIES
        $viewOnlyHis = $user_auth->can('viewOnlyHis', Order::class );
        $viewCalculation = auth()->user()->can('viewCalculation', [Order::class] );
        //END POLICIES

        if($viewOnlyHis){
            $orders = $orders->whereHas("proforma", function($q) use($user_auth){
                $q->where("user_id",$user_auth->id);
            });
        }

        $orders = $orders->orderBy('id', 'desc')->paginate(12);

        //dd($vouchers);
        return view('orders.list', compact('orders', 'viewCalculation') );
    }

    public function admin($id){
        $proformas = Proforma::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->paginate(1);

        $quotations = Quotation::with([])
            ->where('proforma_id',$id)
            ->orderBy('id', 'desc')
            ->get();

        //dd($companies);
        return view('proformas.admin', compact('proformas', 'quotations'));
    }

    public function filter(Request $request, Order $orders){
        $user_auth = auth()->user();

        $tax_number = $request->has('tax_number') ? $request->input('tax_number'): null;
        $company_name = $request->has('company_name') ? $request->input('company_name'): null;
        $code = $request->has('code') ? $request->input('code'): null;


        //POLICIES
        $viewOnlyHis = $user_auth->can('viewOnlyHis', Order::class );
        $viewCalculation = auth()->user()->can('viewCalculation', [Order::class] );
        //END POLICIES

        $orders = $orders->newQuery();
        //$proformas = $proformas::with([]);

        if ($company_name) {
            $orders
                ->whereHas("proforma", function($q) use($company_name){
                    $q->whereHas("customer", function($qu) use($company_name){
                        $qu->where("company_name",$company_name);
                    });
                });
        }

        if ($tax_number) {
            $orders
                ->whereHas("proforma", function($q) use($tax_number){
                    $q->whereHas("customer", function($qu) use($tax_number){
                        $qu->where("tax_number",$tax_number);
                    });
                });
        }

        // Search for a user based on their document number.
        if ($code) {
            $orders
                ->whereHas("proforma", function($q) use($code){
                    $q->where("code",$code);
                });
        }

        if($viewOnlyHis){
            $orders = $orders->whereHas("proforma", function($q) use($user_auth){
                $q->where("user_id",$user_auth->id);
            });
        }

        $orders = $orders->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('orders.list', compact('orders', 'viewCalculation'));

    }

    public function downloadMaterialsPDF($proforma_id){
        //POLICIES
        $viewCalculation = auth()->user()->can('viewCalculation', [Order::class] );
        //END POLICIES

        if(!$viewCalculation){
            return "Usted no puede ver este cálculo";
        }

        $proforma = Proforma::whereId($proforma_id)->first();

        $quotations = Quotation::where('proforma_id',$proforma_id)
            //->select(\DB::raw('sum(unitary_amount*) as total_sum'))
            ->orderBy('id', 'desc')
            ->get();

        $pdf = PDF::loadView('proformas.materialsPdf', compact('proforma','quotations'));
        return $pdf->setPaper('a5')->download('proforma.pdf');

    }

    public function create(){
        $extra = new Quotation();

        $btnText = __("Crear señal");
        return view('proformas.form', compact('extra','btnText'));
    }

    public function store(OrderRequest $orderRequest){

        try {

            Order::create($orderRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó la orden correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando la orden,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }

    public function edit($id){
        $order = Order::where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($accounting);
        return view('orders.form', compact('order'));
    }

    public function update(OrderRequest $orderRequest, Order $order){
        $user_auth = auth()->user();

        //POLICIES
        $updateThis = $user_auth->can('updateThis', [Order::class, $order] );
        //END POLICIES

        if(!$updateThis){
            return "No puedes actualizar esta orden";
        }

        try {

            $order->fill($orderRequest->input())->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el servicio.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            /*return back()->with('message',['danger',
                __("Hubo un error agregando la nota,
                por favor verifique que está colocando los datos requeridos.")]);
            */

        }
    }

    public function modalEditForm(Request $request){

        $order_id = (int) $request->has('order_id') ? $request->input('order_id'): null;

        return self::edit($order_id);
    }

    public function modalMaterialsOrder(Request $request){

        //POLICIES
        $viewCalculation = auth()->user()->can('viewCalculation', [Order::class] );
        //END POLICIES

        if(!$viewCalculation){
            return "Usted no puede ver este cálculo";
        }

        $proforma_id = (int) $request->has('proforma_id') ? $request->input('proforma_id'): null;

        $proforma = Proforma::whereId($proforma_id)->first();

        $quotations = Quotation::where('proforma_id',$proforma_id)
            //->select(\DB::raw('sum(unitary_amount*) as total_sum'))
            ->orderBy('id', 'desc')
            ->get();


        //dd($accounting);
        return view('partials.orders.materialsModal', compact('proforma','quotations'));

    }

    public static function addToObject(&$object_layer,&$object_calc_layer, $code_layer, $area_layer, $decrease_layer, $quantity){
        $total_area = $area_layer*$quantity;
        $total_area_with_decrease= $total_area*(1+0.01*$decrease_layer);

        //INSERTAMOS LOS VALORES EN ARREGLOS DONDE LOS CODIGOS SON LOS INDICES
        //SI EL CODIGO YA EXISTE EN EL INDICE, SE SUMA
        //SINO, SE AÑADE
        if($code_layer != "NN" && $code_layer != "00"){
            if (isset($object_layer[$code_layer])){
                //SUMA
                $object_layer[$code_layer]+=$total_area_with_decrease;
                //ADICIÓN AL CÁLCULO QUE VA EN POPOVER
                $object_calc_layer[$code_layer].=(" + $area_layer * (1 + 0.1*$decrease_layer) * $quantity");
            }else{
                //SUMA
                $object_layer[$code_layer]=$total_area_with_decrease;
                //ADICIÓN AL CÁLCULO QUE VA EN POPOVER
                $object_calc_layer[$code_layer]=("$area_layer * (1 + 0.1*$decrease_layer) * $quantity");
            }
        }

    }


}
