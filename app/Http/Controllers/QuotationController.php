<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuotationRequest;
use App\Quotation;
use Illuminate\Http\Request;

class QuotationController extends Controller
{

    public function admin($id){
        //POLICIES
        $viewAny = auth()->user()->can('viewAny', [Quotation::class] );
        //END POLICIES

        if($viewAny){
            $quotation = Quotation::with([])
                ->where('id',$id)
                ->orderBy('id', 'desc')
                ->first();
        }else{
            return "Usted no puede ver esta señal";
        }

        //dd($companies);
        return view('quotations.admin', compact('quotation'));
    }

    public function historicList(){
        //POLICIES
        $viewAny = auth()->user()->can('viewAny', [Quotation::class] );
        //END POLICIES

        if($viewAny){
            $quotation = Quotation::with([])
                ->where('id',$id)
                ->orderBy('id', 'desc')
                ->first();
        }else{
            return "Usted no puede ver esta señal";
        }

        //dd($companies);
        return view('quotations.admin', compact('quotation'));
    }


    public function create(){
        $extra = new Quotation();

        $btnText = __("Crear señal");
        return view('quotations.form', compact('extra','btnText'));
    }

    public function store(QuotationRequest $quotationRequest){

        //$quotationRequest->merge(['unitary_amount' => 1500 ]);

        //dd($accountingFlowRequest);

        try {

            Quotation::create($quotationRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó la señal correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            /*return back()->with('message',['danger',
                __("Hubo un error agregando la nota,
                por favor verifique que está colocando los datos requeridos.")]);
            */

        }

    }

    public function delete(Quotation $quotation){

        try {

            $quotation->delete();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se eliminó la señal.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error eliminando la señal,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }

    public function quantityUpdate(Request $request, Quotation $quotation){
        $model_id = $request->has('model_id') ? $request->input('model_id'): null;
        $model_value = (int) $request->has('model_value') ? $request->input('model_value'): null;

        $quotation = $quotation->newQuery();
        try {
            $quotation->find($model_id)->update(['quantity' => $model_value]);
            return 1;
        } catch(\Illuminate\Database\QueryException $e){
            return 2;
        }

    }
}
