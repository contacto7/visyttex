<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProformaRequest;
use App\Margin;
use App\Proform;
use App\Role;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use App\Proforma;
use App\Quotation;
use Illuminate\Http\Request;

class ProformaController extends Controller
{
    public function list(){
        $proformas = Proforma::with([]);
        $user_auth = auth()->user();

        //POLICIES
        $viewOnlyHis = $user_auth->can('viewOnlyHis', Proforma::class );
        //END POLICIES

        if($viewOnlyHis){
            $proformas = $proformas->where('user_id', $user_auth->id);
        }

        $proformas = $proformas->orderBy('id', 'desc')->paginate(12);

        //dd($vouchers);
        return view('proformas.list', compact('proformas') );
    }

    public function admin($id){
        $proforma = Proforma::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $quotations = Quotation::with([])
            ->where('proforma_id',$id)
            ->orderBy('id', 'asc')
            ->get();

        $margin_type = 1;

        if(auth()->user()->role_id === Role::EXTERNAL_WORKER){
            $margin_type = 2;
        }

        $margins = Margin::with([])
            ->where('type',$margin_type)
            ->orderBy('id', 'desc')
            ->get();

        $user_auth = auth()->user();

        //POLICIES
        $viewThis = $user_auth->can('viewThis', [Proforma::class, $proforma] );
        //END POLICIES

        if(!$viewThis){
            return "Usted no puede ver esta cotización";
        }

        //dd($companies);
        return view('proformas.admin', compact('proforma', 'quotations', 'margins'));
    }

    public function filter(Request $request, Proforma $proformas){

        $company_name = $request->has('company_name') ? $request->input('company_name'): null;
        $tax_number = $request->has('tax_number') ? $request->input('tax_number'): null;
        $code = $request->has('code') ? $request->input('code'): null;


        $proformas = $proformas->newQuery();
        //$proformas = $proformas::with([]);

        if ($company_name) {
            $proformas
                ->whereHas("customer", function($q) use($company_name){
                    $q->where("company_name",$company_name);
                });
        }

        if ($tax_number) {
            $proformas
                ->whereHas("customer", function($q) use($tax_number){
                    $q->where("tax_number",$tax_number);
                });
        }

        // Search for a user based on their document number.
        if ($code) {
            $proformas->where("code",$code);
        }

        $user_auth = auth()->user();

        //POLICIES
        $viewOnlyHis = $user_auth->can('viewOnlyHis', Proforma::class );
        //END POLICIES

        if($viewOnlyHis){
            $proformas = $proformas->where('user_id', $user_auth->id);
        }

        $proformas = $proformas->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('proformas.list', compact('proformas'));

    }

    public function downloadPDF($id){
        $proforma = Proforma::with([
        ])->find($id);

        $quotations = Quotation::where('proforma_id', $id)->get();


        $pdf = PDF::loadView('proformas.pdf', compact('proforma', 'quotations'));
        return $pdf->setPaper('a4')->download('proforma.pdf');

    }


    public function create(){
        $proforma = new Proforma();

        $btnText = __("Crear Proforma");
        return view('proformas.form', compact('proforma','btnText'));
    }

    public function store(ProformaRequest $proformaRequest){

        $proformaRequest->merge(['code' => 'transitional' ]);
        $proformaRequest->merge(['user_id' => auth()->user()->id ]);
        $proformaRequest->merge(['subtotal' => 0 ]);

        try {

            Proforma::create($proformaRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó la proforma correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando la proforma,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }

    public function edit($id){
        $proforma = Proforma::where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($accounting);
        $btnText = __("Editar Proforma");
        return view('proformas.form', compact('proforma','btnText'));
    }

    public function update(ProformaRequest $proformaRequest, Proforma $proforma){

        try {

            $proforma->fill($proformaRequest->input())->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó la proforma.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error editando la proforma,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }
}
