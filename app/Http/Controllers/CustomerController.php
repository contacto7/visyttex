<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use App\Proforma;
use App\Quotation;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function list(){
        $customers = Customer::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('customers.list', compact('customers') );
    }

    public function admin($id){
        $customer = Customer::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('customers.admin', compact('customer'));
    }

    public function rucSearch(Request $request, Customer $customer){

        $company_name = $request->has('company_name') ? $request->input('company_name'): null;
        $tax_number = $request->has('tax_number') ? $request->input('tax_number'): null;


        $customer = $customer->newQuery();
        //$proformas = $proformas::with([]);

        if ($tax_number) {
            $customer->where("tax_number",$tax_number);
        }

        if ($company_name) {
            $customer->where("company_name", 'like', '%'.$company_name.'%');
        }

        $customer = $customer->first();
        //dd($company);
        return $customer;

    }

    public function filter(Request $request, Customer $customers){

        $company_name = $request->has('company_name') ? $request->input('company_name'): null;
        $tax_number = $request->has('tax_number') ? $request->input('tax_number'): null;


        $customers = $customers->newQuery();
        //$proformas = $proformas::with([]);

        if ($company_name) {
            $customers->where("company_name",$company_name);
        }

        if ($tax_number) {
            $customers->where("tax_number",$tax_number);
        }

        $customers = $customers->orderBy('id', 'desc')->paginate(12);

        //dd($movements);
        return view('customers.list', compact('customers'));

    }

    public function create(){
        $customer = new Customer();

        $btnText = __("Crear cliente");
        return view('customers.form', compact('customer','btnText'));
    }

    public function store(CustomerRequest $customerRequest){

        $customerRequest->merge(['user_id' => auth()->user()->id ]);

        try {

            Customer::create($customerRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó el cliente correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el cliente,
                por favor verifique que está colocando los datos requeridos.")]);


        }

    }

    public function edit($id){
        $customer = Customer::where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($accounting);
        $btnText = __("Editar Cliente");
        return view('customers.form', compact('customer','btnText'));
    }

    public function update(CustomerRequest $customerRequest, Customer $customer){

        try {

            $customer->fill($customerRequest->input())->save();

            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el cliente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error editando el cliente,
                por favor verifique que está colocando los datos requeridos.")]);


        }
    }


}
