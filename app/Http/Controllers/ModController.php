<?php

namespace App\Http\Controllers;

use App\Http\Requests\ModRequest;
use App\Mod;
use Illuminate\Http\Request;

class ModController extends Controller
{
    public function list(){
        $mods = Mod::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('mods.list', compact('mods') );
    }

    public function admin($id){
        $mod = Mod::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('mods.admin', compact('mod'));
    }

    public function update(ModRequest $modRequest, Mod $mod){
        try {
            $mod->fill($modRequest->input())->save();
            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el dato correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error actualizando los datos,
                por favor verifique que está colocando el dato correctamente.")]);
        }
    }

    public function modalUpdate(Request $request, Mod $mod){
        $model_id = $request->has('model_id') ? $request->input('model_id'): null;
        $model_value = (int) $request->has('model_value') ? $request->input('model_value'): null;

        $mod = $mod->newQuery();
        //$proformas = $proformas::with([]);

        if ($model_id && $model_value) {
            $mod->whereId($model_id)->update(['amount' => $model_value]);
        }

        return back()->with('message',['success',
            __("Se actualizó el dato correctamente.")]);
    }
}
