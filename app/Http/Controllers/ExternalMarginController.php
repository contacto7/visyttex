<?php

namespace App\Http\Controllers;

use App\ExternalMargin;
use App\Http\Requests\ExternalMarginRequest;
use Illuminate\Http\Request;

class ExternalMarginController extends Controller
{
    public function list(){
        $margins = ExternalMargin::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('margins.list', compact('margins') );
    }
    public function create(){
        $margin = new ExternalMargin();

        return view('margins.form', compact('margin'));
    }

    public function store(ExternalMarginRequest $externalMarginRequest){

        try {

            ExternalMargin::create($externalMarginRequest->input());

            //dd($customerRequest);
            return back()->with('message',['success', __("Se agregó el margen correctamente.")]);

        } catch(\Illuminate\Database\QueryException $e){
            dd($e);
            return back()->with('message',['danger',
                __("Hubo un error agregando el margen,
                por favor verifique que está colocando correctamente el margen.")]);


        }

    }


}
