<?php

namespace App\Http\Controllers;

use App\Exports\ModelsExport;
use App\OverLabeled;
use Illuminate\Http\Request;

class OverLabeledController extends Controller
{
    public function list(){
        $overLabeleds = OverLabeled::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('overLabeleds.list', compact('overLabeleds') );
    }

    function excel()
    {
        $models = OverLabeled::orderBy('id', 'asc')->get();

        return \Maatwebsite\Excel\Facades\Excel::download(new ModelsExport($models), 'Sobrelaminados.xlsx');
    }

    public function modalUpdate(Request $request, OverLabeled $overLabeled){
        $model_id = $request->has('model_id') ? $request->input('model_id'): null;
        $model_name = (int) $request->has('model_name') ? $request->input('model_name'): null;
        $model_code = (int) $request->has('model_abbr') ? $request->input('model_code'): null;
        $model_abbr = (int) $request->has('model_abbr') ? $request->input('model_abbr'): null;
        $model_value = (int) $request->has('model_value') ? $request->input('model_value'): null;

        $overLabeled = $overLabeled->newQuery();
        //$proformas = $proformas::with([]);

        if ($model_id && $model_value) {
            $overLabeled->whereId($model_id)->update([
                'name' => $model_name,
                'code' => $model_code,
                'code_visyttex' => $model_abbr,
                'price' => $model_value,
            ]);
        }

        return back()->with('message',['success',
            __("Se actualizó el dato correctamente.")]);
    }

    public function modalAdd(Request $request, OverLabeled $material){

        $model_name = $request->has('add_model_name') ? $request->input('add_model_name'): null;
        $model_code = $request->has('add_model_abbr') ? $request->input('add_model_code'): null;
        $model_abbr = $request->has('add_model_abbr') ? $request->input('add_model_abbr'): null;
        $model_value = (int) $request->has('add_model_value') ? $request->input('add_model_value'): null;

        $material = $material->newQuery();
        //$proformas = $proformas::with([]);

        if ($model_name && $model_abbr && $model_value) {
            $material->create([
                'name' => $model_name,
                'code' => $model_code,
                'code_visyttex' => $model_abbr,
                'price' => $model_value,
            ]);
        }

        return back()->with('message',['success',
            __("Se añadió correctamente.")]);
    }
}
