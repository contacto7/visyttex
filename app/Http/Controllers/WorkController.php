<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkRequest;
use App\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    public function list(){
        $works = Work::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($vouchers);
        return view('works.list', compact('works') );
    }

    public function admin($id){
        $work = Work::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        //dd($companies);
        return view('works.admin', compact('work'));
    }


    public function update(WorkRequest $workRequest, Work $work){

        try {
            $work->fill($workRequest->input())->save();
            //dd($customerRequest);
            return back()->with('message',['success', __("Se actualizó el dato correctamente.")]);
        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);
            return back()->with('message',['danger',
                __("Hubo un error actualizando los datos,
                por favor verifique que está colocando el dato correctamente.")]);
        }
    }

    public function modalUpdate(Request $request, Work $work){
        $model_id = $request->has('model_id') ? $request->input('model_id'): null;
        $model_value = (int) $request->has('model_value') ? $request->input('model_value'): null;

        $work = $work->newQuery();
        //$proformas = $proformas::with([]);

        if ($model_id && $model_value) {
            $work->whereId($model_id)->update(['machine_price' => $model_value]);
        }

        return back()->with('message',['success',
            __("Se actualizó el dato correctamente.")]);
    }
}
