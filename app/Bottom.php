<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bottom
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $code
 * @property string $code_visyttex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereCodeVisyttex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bottom whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bottom extends Model
{
    protected $fillable = [
        'name', 'price', 'code', 'code_visyttex',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }
}
