<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SignalType
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string|null $picture
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Signal[] $signals
 * @property-read int|null $signals_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SignalType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SignalType extends Model
{
    //VIALES
    const REGLAMENTARIAS = 1;
    const PREVENTIVAS = 2;
    const INFORMATIVAS = 3;
    const DEOBRA = 4;
    //SEGURIDAD
    const CONTRA_INCEDIOS = 5;
    const ADVERTENCIA = 6;
    const OBLIGACION = 7;
    const EVACUACION = 8;


    protected $fillable = [
        'name', 'description', 'picture',
    ];

    public function signals(){
        return $this->hasMany(Signal::class);
    }
}
