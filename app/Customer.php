<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Customer
 *
 * @property int $id
 * @property string $company_name
 * @property string $tax_number
 * @property string $address
 * @property string $phone
 * @property int $user_id
 * @property string $contact_name
 * @property string $contact_last_name
 * @property string $contact_cellphone
 * @property string $contact_email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Proforma[] $proformas
 * @property-read int|null $proformas_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereContactCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereContactLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereTaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Customer whereUserId($value)
 * @mixin \Eloquent
 */
class Customer extends Model
{
    protected $fillable = [
        'company_name', 'tax_number', 'address', 'phone', 'user_id',
        'contact_name', 'contact_last_name', 'contact_cellphone', 'contact_email',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function proformas(){
        return $this->hasMany(Proforma::class);
    }
}
