<?php

namespace App\Exports;

use App\Base;
use Maatwebsite\Excel\Concerns\FromCollection;

class InvoicesExport implements FromCollection
{
    public function collection()
    {
        return Base::all();
    }
}

?>
