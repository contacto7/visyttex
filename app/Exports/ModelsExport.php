<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ModelsExport implements FromView
{
    protected $models;

    public function __construct($models)
    {
        $this->models = $models;
    }

    public function view(): View
    {
        return view('exports.models', [
            'models' => $this->models
        ]);
    }
}

