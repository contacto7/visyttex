<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Work
 *
 * @property int $id
 * @property string $name
 * @property float $machine_price
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereMachinePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Work whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Work extends Model
{
    protected $fillable = [
        'name', 'machine_price', 'code',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

    public function mods(){
        return $this->hasMany(Mod::class);
    }
}
