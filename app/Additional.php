<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Additional
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $code
 * @property string $code_visyttex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereCodeVisyttex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Additional whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Additional extends Model
{
    protected $fillable = [
        'name', 'price', 'code', 'code_visyttex',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }
}
