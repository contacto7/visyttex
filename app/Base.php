<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Base
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $code
 * @property string $code_visyttex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereCodeVisyttex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Base whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Base extends Model
{
    protected $fillable = [
        'name', 'price', 'code', 'code_visyttex',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }
}
