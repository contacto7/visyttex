<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Quotation
 *
 * @property int $id
 * @property int $signal_id
 * @property string $proforma_code
 * @property string $description
 * @property float $height
 * @property float $width
 * @property int $currency_id
 * @property int $quantity
 * @property float $unitary_amount
 * @property int $margin_id
 * @property int $work_id
 * @property int $base_id
 * @property int $bottom_id
 * @property int|null $labeled_id
 * @property int|null $over_labeled_id
 * @property int|null $additional_id
 * @property int $measure_id
 * @property int $mod_id
 * @property int $proforma_id
 * @property int $decrease_base
 * @property int $decrease_bottom
 * @property int|null $decrease_labeled
 * @property int|null $decrease_over_labeled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Customer[] $customers
 * @property-read int|null $customers_count
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereAdditionalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereBaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereBottomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereDecreaseBase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereDecreaseBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereDecreaseLabeled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereDecreaseOverLabeled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereLabeledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereMarginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereMeasureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereModId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereOverLabeledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereProformaCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereProformaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereSignalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereUnitaryAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation whereWorkId($value)
 * @mixin \Eloquent
 * @property-read \App\Additional|null $additional
 * @property-read \App\Base $base
 * @property-read \App\Bottom $bottom
 * @property-read \App\Currency $currency
 * @property-read \App\Labeled|null $labeled
 * @property-read \App\Margin $margin
 * @property-read \App\Measure $measure
 * @property-read \App\Mod $mod
 * @property-read \App\OverLabeled|null $overLabeled
 * @property-read \App\Proforma $proforma
 * @property-read \App\Signal $signal
 * @property-read \App\Work $work
 * @property float $packaging_supplies
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quotation wherePackagingSupplies($value)
 */
class Quotation extends Model
{
    protected $fillable = [
        'signal_id', 'proforma_code', 'description', 'height', 'width',
        'currency_id', 'quantity', 'unitary_amount', 'margin_id', 'work_id',
        'base_id', 'bottom_id', 'labeled_id', 'second_labeled_id', 'over_labeled_id',
        'second_over_labeled_id', 'additional_id', 'mod_id', 'proforma_id',  'packaging_supplies', 'decrease_base',
        'decrease_bottom', 'decrease_labeled', 'decrease_second_labeled', 'decrease_over_labeled', 'decrease_second_over_labeled',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function (Quotation $quotation) {

            $proforma = new Proforma();

            $pre_description="";

            $quotation_id = $quotation->id;
            $proforma_id = $quotation->proforma_id;

            //TRANSFORMAMOS LA DESCRIPCION
            $description = $quotation->description;
            $height = $quotation->height;
            $width = $quotation->width;
            $signal_code = $quotation->signal->code;
            $signal_name = $quotation->signal->name;
            $base_abbreviation = $quotation->base->code_visyttex;
            $bottom_abbreviation = $quotation->bottom->code_visyttex;
            $labeled_abbreviation = $quotation->labeled->code_visyttex;
            $over_labeled_abbreviation = $quotation->overLabeled->code_visyttex;
            $additional_abbreviation = $quotation->additional->code_visyttex;

            //$pre_description = "Señal '".$signal_code." ".$signal_name."' de ".$width." X ".$height." MTS. ";

            //DEFINIMOS SI ES SEÑAL O STICJER
            if($base_abbreviation == 'NN'){
                $pre_description.="STICKER ";
            }else{
                $pre_description.="SEÑAL ";
            }
            //AGREGAMOS LAS MEDIDAS
            $pre_description .= ("DE ".$height." x ".$width." M ");


            //AGREGAMOS LAS ABREVIATURAS
            $pre_description .= ($signal_code." ".$signal_name." ");
            $pre_description .= ($base_abbreviation."/");
            $pre_description .= ($bottom_abbreviation."/");
            $pre_description .= ($labeled_abbreviation."/");
            $pre_description .= ($over_labeled_abbreviation."/");
            $pre_description .= ($additional_abbreviation);

            $post_description = $quotation->proforma_code;

            //AUMENTAMOS UN PUNTO DESPUES DE LAS ABREVIACIONES
            if($description){
                $description=". ".$description;
            }

            //JUNTAMOS LA DESCRICION
            $description = $pre_description.$description.". CÓDIGO: ".$post_description;

            //PASAMOS A MAYUSCULAS
            $description = mb_strtoupper ( $description );

            $quotation::whereId($quotation_id)->update(['description'=> $description]);

            //MONTO TOTAL
            $total_amount = Quotation::where('proforma_id',$proforma_id)
                ->select(\DB::raw('sum(unitary_amount*quantity) as total_sum'))
                ->first();

            $total_amount = $total_amount->total_sum;

            $proforma::whereId($proforma_id)->update(['subtotal'=> $total_amount]);

        });
        static::deleted(function (Quotation $quotation) {

            $proforma = new Proforma();

            $quotation_id = $quotation->id;
            $proforma_id = $quotation->proforma_id;

            //MONTO TOTAL
            $total_amount = Quotation::where('proforma_id',$proforma_id)
                ->select(\DB::raw('sum(unitary_amount*quantity) as total_sum'))
                ->first();

            $total_amount = $total_amount->total_sum;

            if(!$total_amount){
                $total_amount = 0;
            }

            $proforma::whereId($proforma_id)->update(['subtotal'=> $total_amount]);

        });
        static::updated(function (Quotation $quotation) {

            $proforma = new Proforma();

            $quotation_id = $quotation->id;
            $proforma_id = $quotation->proforma_id;

            //MONTO TOTAL
            $total_amount = Quotation::where('proforma_id',$proforma_id)
                ->select(\DB::raw('sum(unitary_amount*quantity) as total_sum'))
                ->first();

            $total_amount = $total_amount->total_sum;

            $proforma::whereId($proforma_id)->update(['subtotal'=> $total_amount]);

        });



    }

    public function signal(){
        return $this->belongsTo(Signal::class);
    }
    public function currency(){
        return $this->belongsTo(Currency::class);
    }
    public function margin(){
        return $this->belongsTo(Margin::class);
    }
    public function work(){
        return $this->belongsTo(Work::class);
    }
    public function base(){
        return $this->belongsTo(Base::class);
    }
    public function bottom(){
        return $this->belongsTo(Bottom::class);
    }
    public function labeled(){
        return $this->belongsTo(Labeled::class);
    }
    public function secondLabeled(){
        return $this->belongsTo(Labeled::class,'second_labeled_id');
    }
    public function overLabeled(){
        return $this->belongsTo(OverLabeled::class);
    }
    public function secondOverLabeled(){
        return $this->belongsTo(OverLabeled::class,'second_over_labeled_id');
    }
    public function additional(){
        return $this->belongsTo(Additional::class);
    }
    public function mod(){
        return $this->belongsTo(Mod::class);
    }
    public function proforma(){
        return $this->belongsTo(Proforma::class);
    }

}
