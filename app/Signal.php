<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Signal
 *
 * @property int $id
 * @property int $signal_type_id
 * @property string $name
 * @property string|null $description
 * @property string|null $picture
 * @property string $signal_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Additional $additional
 * @property-read \App\Base $base
 * @property-read \App\Bottom $bottom
 * @property-read \App\Currency $currency
 * @property-read \App\Labeled $labeled
 * @property-read \App\Margin $margin
 * @property-read \App\Measure $measure
 * @property-read \App\Mod $mod
 * @property-read \App\OverLabeled $overLabeled
 * @property-read \App\Proforma $proforma
 * @property-read \App\Signal $signal
 * @property-read \App\Work $work
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereSignalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereSignalTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @property-read \App\SignalType $signalType
 * @property string $code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Signal whereCode($value)
 */
class Signal extends Model
{
    protected $fillable = [
        'signal_type_id', 'name', 'description',
        'picture', 'code',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function (Signal $signal) {

            $signal_id = (int) $signal->id;
            $signal_type = (int) $signal->signal_type_id;

            $signal_picture_code = 0;

            $reglamentarias = 86;
            $preventivas = 80;
            $informativas = 1;
            $de_obra = 1;
            $equipo_contra_incendios = 48;
            $advertencia = 40;
            $obligacion = 31;
            switch ($signal_type){
                case 1://REGLAMENTARIAS
                    $signal_picture_code=$signal_id;
                    break;
                case 2://PREVENTIVAS
                    $signal_picture_code=$signal_id - $reglamentarias;
                    break;
                case 3://INFORMATIVAS
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas );
                    break;
                case 4://DE OBRA
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas + $informativas );
                    break;
                case 5://EQUIPO CONTRA INCENDIOS
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas + $informativas + $de_obra );
                    break;
                case 6://ADVERTENCIA
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas + $informativas + $de_obra + $equipo_contra_incendios );
                    break;
                case 7://OBLIGACIÓN
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas + $informativas + $de_obra + $equipo_contra_incendios + $advertencia );
                    break;
                case 8://EVACUACIÓN Y EMERGENCIA
                    $signal_picture_code=$signal_id - ($reglamentarias + $preventivas + $informativas + $de_obra + $equipo_contra_incendios + $advertencia + $obligacion );
                    break;
            }
            $picture = $signal_picture_code . ".png";

            $signal::whereId($signal_id)->update(['picture' => $picture]);

        });

    }

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }
    public function signalType(){
        return $this->belongsTo(SignalType::class);
    }

}
