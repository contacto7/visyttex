<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Margin
 *
 * @property int $id
 * @property int $percentage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quotation[] $quotations
 * @property-read int|null $quotations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Margin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Margin extends Model
{
    const INTERNO = 1;
    const EXTERNO = 2;

    protected $fillable = [
        'percentage', 'type',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

}
