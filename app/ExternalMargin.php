<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExternalMargin extends Model
{
    protected $fillable = [
        'percentage',
    ];

    public function quotations(){
        return $this->hasMany(Quotation::class);
    }

}
