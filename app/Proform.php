<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Proform
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proform whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Proform extends Model
{
    //
}
