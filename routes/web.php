<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function (){

    Route::group(['prefix' => 'orders'], function (){
        Route::get('/list','OrderController@list')
            ->name('orders.list');
        Route::get('/admin/{id}','OrderController@admin')
            ->name('orders.admin');
        Route::get('/search','OrderController@filter')
            ->name('orders.search');
        Route::get('/create','OrderController@create')
            ->name('orders.create');
        Route::post('/store','OrderController@store')
            ->name('orders.store');
        Route::get('/edit/{id}','OrderController@edit')
            ->name('orders.edit');
        Route::put('/update/{order}','OrderController@update')
            ->name('orders.update');
        Route::get('/modalEditForm','OrderController@modalEditForm')
            ->name('orders.modalEditForm');
        Route::get('/modalMaterialsOrder','OrderController@modalMaterialsOrder')
            ->name('orders.modalMaterialsOrder');
        Route::get('/downloadMaterialsPDF/{id}','OrderController@downloadMaterialsPDF')
            ->name('orders.downloadMaterialsPDF');
    });
    Route::group(['prefix' => 'proformas'], function (){
        Route::get('/list','ProformaController@list')
            ->name('proformas.list');
        Route::get('/admin/{id}','ProformaController@admin')
            ->name('proformas.admin');
        Route::get('/search','ProformaController@filter')
            ->name('proformas.search');
        Route::get('/create','ProformaController@create')
            ->name('proformas.create');
        Route::post('/store','ProformaController@store')
            ->name('proformas.store');
        Route::get('/edit/{id}','ProformaController@edit')
            ->name('proformas.edit');
        Route::put('/update/{proforma}','ProformaController@update')
            ->name('proformas.update');
        Route::get('/downloadPDF/{id}','ProformaController@downloadPDF')
            ->name('proformas.downloadPDF');
    });
    Route::group(['prefix' => 'customers'], function (){
        Route::get('/list','CustomerController@list')
            ->name('customers.list');
        Route::get('/admin/{id}','CustomerController@admin')
            ->name('customers.admin');
        Route::get('/search','CustomerController@filter')
            ->name('customers.search');
        Route::get('/ruc-search','CustomerController@rucSearch')
            ->name('customers.rucSearch');
        Route::get('/create','CustomerController@create')
            ->name('customers.create');
        Route::post('/store','CustomerController@store')
            ->name('customers.store');
        Route::get('/edit/{id}','CustomerController@edit')
            ->name('customers.edit');
        Route::put('/update/{customer}','CustomerController@update')
            ->name('customers.update');
    });
    Route::group(['prefix' => 'quotations'], function (){
        Route::get('/list','QuotationController@list')
            ->name('quotations.list');
        Route::get('/historicList','QuotationController@historicList')
            ->name('quotations.historicList');
        Route::get('/admin/{id}','QuotationController@admin')
            ->name('quotations.admin');
        Route::get('/search','QuotationController@filter')
            ->name('quotations.search');
        Route::get('/create','QuotationController@create')
            ->name('quotations.create');
        Route::post('/store','QuotationController@store')
            ->name('quotations.store');
        Route::get('/edit/{id}','QuotationController@edit')
            ->name('quotations.edit');
        Route::get('/quantityUpdate','QuotationController@quantityUpdate')
            ->name('quotations.quantityUpdate');
        Route::put('/update/{extra}','QuotationController@update')
            ->name('quotations.update');
        Route::get('/delete/{quotation}','QuotationController@delete')
            ->name('quotations.delete');
    });
    Route::group(['prefix' => 'users'], function (){
        Route::get('/list','UserController@list')
            ->name('users.list');
        Route::get('/admin/{id}','UserController@admin')
            ->name('users.admin');
        Route::get('/search','UserController@filter')
            ->name('users.search');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/create','UserController@create')
                ->name('users.create');
            Route::post('/store','UserController@store')
                ->name('users.store');
            Route::get('/edit/{id}','UserController@edit')
                ->name('users.edit');
            Route::put('/update/{user}','UserController@update')
                ->name('users.update');
        });
    });
    Route::group(['prefix' => 'margins'], function (){
        Route::get('/list','MarginController@list')
            ->name('margins.list');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/create','MarginController@create')
                ->name('margins.create');
            Route::post('/store','MarginController@store')
                ->name('margins.store');
            Route::get('/edit/{id}','MarginController@edit')
                ->name('margins.edit');
            Route::put('/update/{user}','MarginController@update')
                ->name('margins.update');
        });
    });
    Route::group(['prefix' => 'works'], function (){
        Route::get('/list','WorkController@list')
            ->name('works.list');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/create','WorkController@create')
                ->name('works.create');
            Route::post('/store','WorkController@store')
                ->name('works.store');
            Route::get('/edit/{id}','WorkController@edit')
                ->name('works.edit');
            Route::put('/update/{user}','WorkController@update')
                ->name('works.update');
            Route::post('/modalUpdate','WorkController@modalUpdate')
                ->name('works.modalUpdate');
        });
    });
    Route::group(['prefix' => 'mods'], function (){
        Route::get('/list','ModController@list')
            ->name('mods.list');
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/create','ModController@create')
                ->name('mods.create');
            Route::post('/store','ModController@store')
                ->name('mods.store');
            Route::get('/edit/{id}','ModController@edit')
                ->name('mods.edit');
            Route::put('/update/{user}','ModController@update')
                ->name('mods.update');
            Route::post('/modalUpdate','ModController@modalUpdate')
                ->name('mods.modalUpdate');
        });
    });
    Route::group(['prefix' => 'bases'], function (){
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/list','BaseController@list')
                ->name('bases.list');
            Route::get('/excel','BaseController@excel')
                ->name('bases.excel');
            Route::post('/modalUpdate','BaseController@modalUpdate')
                ->name('bases.modalUpdate');
            Route::post('/modalAdd','BaseController@modalAdd')
                ->name('bases.modalAdd');
        });
    });
    Route::group(['prefix' => 'bottoms'], function (){
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/list','BottomController@list')
                ->name('bottoms.list');
            Route::get('/excel','BottomController@excel')
                ->name('bottoms.excel');
            Route::post('/modalUpdate','BottomController@modalUpdate')
                ->name('bottoms.modalUpdate');
            Route::post('/modalAdd','BottomController@modalAdd')
                ->name('bottoms.modalAdd');
        });
    });
    Route::group(['prefix' => 'labeleds'], function (){
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/list','LabeledController@list')
                ->name('labeleds.list');
            Route::get('/excel','LabeledController@excel')
                ->name('labeleds.excel');
            Route::post('/modalUpdate','LabeledController@modalUpdate')
                ->name('labeleds.modalUpdate');
            Route::post('/modalAdd','LabeledController@modalAdd')
                ->name('labeleds.modalAdd');
        });
    });
    Route::group(['prefix' => 'over-labeleds'], function (){
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/list','OverLabeledController@list')
                ->name('overLabeleds.list');
            Route::get('/excel','OverLabeledController@excel')
                ->name('overLabeleds.excel');
            Route::post('/modalUpdate','OverLabeledController@modalUpdate')
                ->name('overLabeleds.modalUpdate');
            Route::post('/modalAdd','OverLabeledController@modalAdd')
                ->name('overLabeleds.modalAdd');
        });
    });
    Route::group(['prefix' => 'additionals'], function (){
        Route::group(['middleware' => 'is.admin'], function () {
            Route::get('/list','AdditionalController@list')
                ->name('additionals.list');
            Route::get('/excel','AdditionalController@excel')
                ->name('additionals.excel');
            Route::post('/modalUpdate','AdditionalController@modalUpdate')
                ->name('additionals.modalUpdate');
            Route::post('/modalAdd','AdditionalController@modalAdd')
                ->name('additionals.modalAdd');
        });
    });

});

Route::get('images/{path}/{attachment}', function ($path, $attachment){
    $file = sprintf('storage/%s/%s', $path, $attachment);

    if(File::exists($file)){
        return \Intervention\Image\Facades\Image::make($file)->response();
    }else{
        return \Intervention\Image\Facades\Image::make('image/not-found.png')->response();
    }
});
