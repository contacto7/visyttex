@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("MOD"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">MOD</th>
                    <th scope="col">Trabajo</th>
                    <th scope="col">Monto ($)</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $temp=0; @endphp
                @forelse($mods as $mod)
                    @php $temp++; @endphp
                    <tr style="@if($mod->work_id == 1) display:none @endif">
                        <td>{{ $temp }}</td>
                        <td>{{ $mod->name }}</td>
                        <td>{{ $mod->work->name }}</td>
                        <td>{{ number_format($mod->amount, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info modificar"
                                data-toggle="modal"
                                data-target="#modalEdit"
                                href="#modalEdit"
                                data-id="{{ $mod->id }}"
                                data-value="{{ $mod->amount }}"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalEdit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('mods.modalUpdate') }}"
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="model_id">Id de modelo:</label>
                                <input type="number" class="form-control" name="model_id" id="model_id" required>
                            </div>
                            <div class="form-group">
                                <label for="model_value">Monto:</label>
                                <input type="number" class="form-control" name="model_value" id="model_value" step="0.50" required>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Modificar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $mods->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        /*
        * Función para poner el id de la proforma
        * en el modal
        */
        $(document).on('click', '.modificar', function(){
            let data_id = $(this).data('id');
            let data_value = $(this).data('value');

            console.log(data_id);
            console.log(data_value);

            $("#model_id").val(data_id);
            $("#model_value").val(data_value);
        });

    </script>
@endpush
