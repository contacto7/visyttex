@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Ordenes"),
        'icon' => "shopping-cart"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.orders.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Proforma</th>
                    <th scope="col">Orden</th>
                    <th scope="col">Entrega</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Trabajador</th>
                    <th scope="col">Detalles adicionales</th>
                </tr>
                </thead>
                <tbody>
                @forelse($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->proforma->code }}</td>
                        <td>{{ $order->order_date }}</td>
                        <td>{{ $order->delivered_date }}</td>
                        <td>{{ $order->proforma->customer->company_name }}</td>
                        <td>{{ $order->proforma->user->name }}</td>
                        <td>{{ $order->additional_details }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('proformas.admin', $order->proforma->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver proforma"
                            >
                                <i class="fa fa-file-text-o"></i>
                            </a>
                            <a
                                class="btn btn-outline-info editar-orden-btn"
                                data-toggle="modal"
                                data-target="#modalOrder"
                                href="#modalOrder"
                                data-id="{{ $order->id }}"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                            @if($viewCalculation)
                            <a
                                class="btn btn-outline-info materials-order-btn"
                                data-toggle="modal"
                                data-target="#modalMaterials"
                                href="#modalMaterials"
                                data-id="{{ $order->proforma_id }}"
                            >
                                <i class="fa fa-list"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{action('OrderController@downloadMaterialsPDF', $order->proforma_id)}}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Descargar lista de materiales"

                            >
                                <i class="fa fa-download"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay órdenes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para editar las ordenes -->
        <!-- The Modal -->
        <div class="modal" id="modalOrder">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Orden</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <!-- Modal para ver los materiales de las ordenes -->
        <!-- The Modal -->
        <div class="modal" id="modalMaterials">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Materiales</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $orders->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection


@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush

@push('scripts')
<script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script>

    $(document).on('click', '.editar-orden-btn', function(){
        $(".modal-ajax-content").html('Cargando los datos...');

        let order_id = $(this).attr('data-id');

        ajaxEditOrder(order_id);
    });

    $(document).on('click', '.materials-order-btn', function(){
        $(".modal-ajax-content").html('Cargando los datos...');

        console.log("materials button click");

        let proforma_id = $(this).attr('data-id');

        console.log("proforma id: "+proforma_id);

        ajaxMaterialsOrder(proforma_id);
    });

    function ajaxEditOrder(order_id){
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{ route('orders.modalEditForm') }}',
            data: {
                order_id: order_id
            },
            success: function (data) {
                if(data.includes("Error")){
                    alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                }else{
                    $("#modalOrder .modal-ajax-content").html(data);
                    $('#order_date_tpu').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        locale: 'es'
                    });
                    $('#delivered_date_tpu').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        locale: 'es'
                    });
                }

            },error:function(){
                console.log(data);
            }
        });

    }

    function ajaxMaterialsOrder(proforma_id){
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{ route('orders.modalMaterialsOrder') }}',
            data: {
                proforma_id: proforma_id
            },
            success: function (data) {
                if(data.includes("Error")){
                    alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                }else{
                    $("#modalMaterials .modal-ajax-content").html(data);
                }

            },error:function(){
                console.log(data);
            }
        });

    }

</script>
@endpush
