@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Rotulados"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row text-center p-2">
            <div class="addmaterial-wrapp">
                <a
                    class="btn btn-visyttex"
                    data-toggle="modal"
                    data-target="#modalAdd"
                    href="#modalAdd"
                >
                    Añadir
                </a>
                <a
                    class="btn btn-visyttex"
                    href="{{ route('labeleds.excel') }}"
                >Exportar a Excel</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Rotulado</th>
                    <th scope="col">Abreviatura</th>
                    <th scope="col">Código</th>
                    <th scope="col">Precio ($)</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $temp=0; @endphp
                @forelse($labeleds as $labeled)
                    @php $temp++; @endphp
                    <tr>
                        <td>{{ $temp }}</td>
                        <td>{{ $labeled->name }}</td>
                        <td>{{ $labeled->code_visyttex }}</td>
                        <td>{{ $labeled->code }}</td>
                        <td>{{ number_format($labeled->price, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info modificar"
                                data-toggle="modal"
                                data-target="#modalEdit"
                                href="#modalEdit"
                                data-id="{{ $labeled->id }}"
                                data-value="{{ $labeled->price }}"
                                data-name="{{ $labeled->name }}"
                                data-code="{{ $labeled->code }}"
                                data-abbr="{{ $labeled->code_visyttex }}"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para agregar -->
        <!-- The Modal -->
        <div class="modal" id="modalAdd">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Añadir</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('labeleds.modalAdd') }}"
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group">
                                <label for="add_model_name">Nombre:</label>
                                <input type="text" class="form-control" name="add_model_name" id="add_model_name" required>
                            </div>
                            <div class="form-group">
                                <label for="add_model_abbr">Abreviatura:</label>
                                <input type="text" class="form-control" name="add_model_abbr" id="add_model_abbr" required>
                            </div>
                            <div class="form-group">
                                <label for="add_model_code">Código:</label>
                                <input type="text" class="form-control" name="add_model_code" id="add_model_code" required>
                            </div>
                            <div class="form-group">
                                <label for="add_model_value">Precio:</label>
                                <input type="number" class="form-control" step="0.01" name="add_model_value" id="add_model_value" required>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Añadir
                                </button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <!-- Modal para agregar -->
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalEdit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('labeleds.modalUpdate') }}"
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="model_id">Id de modelo:</label>
                                <input type="number" class="form-control" name="model_id" id="model_id" required>
                            </div>
                            <div class="form-group">
                                <label for="model_name">Nombre:</label>
                                <input type="text" class="form-control" name="model_name" id="model_name" required>
                            </div>
                            <div class="form-group">
                                <label for="model_code">Código:</label>
                                <input type="text" class="form-control" name="model_code" id="model_code" required>
                            </div>
                            <div class="form-group">
                                <label for="model_abbr">Abreviatura:</label>
                                <input type="text" class="form-control" name="model_abbr" id="model_abbr" required>
                            </div>
                            <div class="form-group">
                                <label for="model_value">Precio:</label>
                                <input type="number" class="form-control" name="model_value" id="model_value" required step="0.01">
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Modificar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $labeleds->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        /*
        * Función para poner el id de la proforma
        * en el modal
        */
        $(document).on('click', '.modificar', function(){
            let data_id = $(this).data('id');
            let data_name = $(this).data('name');
            let data_code = $(this).data('code');
            let data_abbr = $(this).data('abbr');
            let data_value = $(this).data('value');

            console.log(data_id);
            console.log(data_value);

            $("#model_id").val(data_id);
            $("#model_name").val(data_name);
            $("#model_code").val(data_code);
            $("#model_abbr").val(data_abbr);
            $("#model_value").val(data_value);
        });

    </script>
@endpush
