@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Trabajos"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Trabajo</th>
                    <th scope="col">Código</th>
                    <th scope="col">Costo de máquina ($)</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($works as $work)
                    <tr>
                        <td>{{ $work->id }}</td>
                        <td>{{ $work->name }}</td>
                        <td>{{ $work->code }}</td>
                        <td>{{ number_format($work->machine_price, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info modificar"
                                data-toggle="modal"
                                data-target="#modalEdit"
                                href="#modalEdit"
                                data-id="{{ $work->id }}"
                                data-value="{{ $work->machine_price }}"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalEdit">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('works.modalUpdate') }}"
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="model_id">Id de modelo:</label>
                                <input type="number" class="form-control" name="model_id" id="model_id" required>
                            </div>
                            <div class="form-group">
                                <label for="model_value">Monto:</label>
                                <input type="number" class="form-control" name="model_value" id="model_value" step="0.5" required>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Modificar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $works->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        /*
        * Función para poner el id de la proforma
        * en el modal
        */
        $(document).on('click', '.modificar', function(){
            let data_id = $(this).data('id');
            let data_value = $(this).data('value');

            console.log(data_id);
            console.log(data_value);

            $("#model_id").val(data_id);
            $("#model_value").val(data_value);
        });

    </script>
@endpush
