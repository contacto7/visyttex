<form
    method="POST"
    action="{{ route('works.update', $work->id) }}"
    novalidate
    style="border: 0"
>
    @method('PUT')
    @csrf
    <div class="form-group">
        <label for="order_date">Día de orden:</label>
        <div class="input-group date" id="order_date_tpu" data-target-input="nearest">
            <input
                type="text"
                name="order_date"
                id="order_date"
                class="form-control datetimepicker-input"
                data-target="#order_date_tpu"
                data-toggle="datetimepicker"
                value="{{ $order->order_date }}"
            />
            <div class="input-group-append" data-target="#order_date_tpu" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="delivered_date">Día que se entregó:</label>
        <div class="input-group date" id="delivered_date_tpu" data-target-input="nearest">
            <input
                type="text"
                name="delivered_date"
                id="delivered_date"
                class="form-control datetimepicker-input"
                data-target="#delivered_date_tpu"
                data-toggle="datetimepicker"
                value="{{ $order->delivered_date }}"
            />
            <div class="input-group-append" data-target="#delivered_date_tpu" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="additional_details">Detalles adicionales:</label>
        <textarea class="form-control" name="additional_details" id="additional_details">{{ $order->additional_details }}</textarea>
    </div>
    <div class="form-group">
        <button
            type="submit"
            class="btn btn-outline-info">
            Modificar
        </button>
    </div>
</form>
