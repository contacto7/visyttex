@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Proforma"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <div class="row text-center flex-column">
            <h2 class="flex-row">{{ $proforma->code }}</h2>
        </div>
        <div class="row mt-3 mb-3">
            <div class="info-wrapper">
                <div class="info-row">
                    <span><b>CLIENTE:</b></span>

                    <a
                        href="{{ route('customers.admin', $proforma->customer->id) }}"
                    >
                        <span>
                            {{ $proforma->customer->company_name }}
                        </span>
                    </a>
                </div>
                <div class="info-row">
                    <span><b>CONTACTO:</b></span>
                    <span>{{ $proforma->proforma_name }}</span>
                </div>
                <div class="info-row">
                    <span><b>CORREO:</b></span>
                    <span>{{ $proforma->proforma_mail }}</span>
                </div>
                <div class="info-row">
                    <span><b>FECHA:</b></span>
                    <span>{{ $proforma->date }}</span>
                </div>
                <div class="info-row">
                    <span><b>TIEMPO DE ENTREGA:</b></span>
                    <span>{{ $proforma->delivery_time }} días</span>
                </div>
                <div class="info-row">
                    <span><b>TIEMPO DE VALIDEZ:</b></span>
                    <span>{{ $proforma->validity_time }} días</span>
                </div>
                <div class="info-row">
                    <span><b>DETALLES ADICIONALES:</b></span>
                    <span>{{ $proforma->additional_details }}</span>
                </div>
                <div class="info-row">
                    <span><b>SUBTOTAL:</b></span>
                    <div>$ <span class="subtotal">{{ $proforma->subtotal }}</span></div>
                </div>
                <div class="info-row">
                    <span><b>REGISTRÓ:</b></span>
                    <span>{{ $proforma->user->name." ".$proforma->user->last_name }}</span>
                </div>
            </div>
        </div>

        <div class="row mt-3 mb-3">
            <div class="info-wrapper">
                <a
                    class="btn btn-visyttex"
                    data-toggle="modal"
                    data-target="#modalSignal"
                    href="#modalSignal"
                >
                    Añadir señal
                </a>

            </div>
        </div>


        <div class="row mt-3 mb-3">
            <div class="info-wrapper">
                @include('partials.proformas.quotations', ['quotations' => $quotations])
            </div>
        </div>
    </div>

    @include('partials.proformas.newSignalModal')

@endsection

@push('scripts')
     <script>
        /*
        * Función para mostrar el siguiente paso,
        * y ocultar el anterior
        */
        $(document).on('click', '.pasos-cotizacion', function(){
            let paso_ocultar = $(this).data('hide');
            let paso_mostrar = $(this).data('target');

            $("."+paso_ocultar).hide();
            $("."+paso_mostrar).show();

        });
        /*
        * Función para poner el valor de la señal
        * seleccionada en el campo signal_id, escondido
        * en el formulario
        */
        $(document).on('click', '.senal', function(){
            let signal_id = $(this).data('id');
            let signal_code = $(this).data('code');
            let signal_name = $(this).find('.senal-txt').text();
            let src_signal =  $(this).find('.senal-img').attr('src');

            console.log(signal_id);
            console.log(signal_code);
            console.log(signal_name);

            $("#signal_id").val(signal_id);
            $("#signal_id").attr('data-code',signal_code);
            $("#nombre-senal").text(signal_name);
            $("#img-senal").attr('src', src_signal);

            let codigo_proforma = changeSignalCode();

            $("#proforma_code").val(codigo_proforma);
        });

        /*
        * Función para cambiar el código de cotización
         * cuando se cambio el selector de trabajo
        */
        $(document).on('change', '#work_id,#base_id,#bottom_id,#labeled_id,#over_labeled_id,#additional_id', function(){
            console.log("cambio");

            let codigo_proforma = changeSignalCode();

            $("#proforma_code").val(codigo_proforma);
        });

        /*
        * Función para cambiar el MOD
        */
        $(document).on('change', '.modificar-mod', function(){
            console.log("cambio trabajo");
            let work = $(this).val();

            $("#mod_id > option").hide();


            let first_optiion = 0;
            $("#mod_id > option").each(function() {
                let work_option = $(this).data("work");

                if(work == work_option){
                    $(this).show();

                    if(!first_optiion){
                        $(this).prop('selected', true);
                    }

                    first_optiion++;
                }
            });

        });

        /*
        * Función para calcular el monto unitario
        */
        $(document).on('change input click', '.modificador-precio', function(){

            let unitary_amount = calculateUnitaryAmount();

            $("#unitary_amount").val(unitary_amount);
        });
        /*
        * Función para retornar el código de la cotización
        * CODIGO = CODIGO_SEÑAL+'-'+BASE+FONDO+TRABAJO+ROTULADO+(SOBRELAMINADO O ADICIONAL)
        */
        function changeSignalCode(){
            let proforma_code;
            let signal_code = $("#signal_id").attr('data-code');
            signal_code = signal_code.replace('-', '');
            let work = $("#work_id option:selected").attr('data-code');
            let base = $("#base_id option:selected").attr('data-code');
            let bottom = $("#bottom_id option:selected").attr('data-code');
            let labeled = $("#labeled_id option:selected").attr('data-code');
            let over_labeled = $("#over_labeled_id option:selected").attr('data-code');
            let additional = $("#additional_id option:selected").attr('data-code');
            let height = $("#height").val();
            let width = $("#width").val();

            proforma_code = signal_code+"-"+base+bottom+work+labeled;

            if(over_labeled == "00" || over_labeled == "NN"){
                proforma_code+=additional;
            }else{
                proforma_code+=over_labeled;
            }
            proforma_code+= ("-"+height+"X"+width);

            return proforma_code;


        }

        /*
        * función para calcular el costo unitario de la señal
        * */
        function calculateUnitaryAmount() {
            console.log("--------------------------------------------------");

            //Area
            let ancho = parseFloat($("#width").val()) || 0;
            let alto = parseFloat($("#height").val()) || 0;
            let total_area =  ancho*alto;
            total_area = truncateDecimals(total_area * 1000) / 1000; // = 5.46

            //Declaro las variables
            let base_cost = parseFloat($("#base_id option:selected").attr('data-price'))*total_area || 0;
            let bottom_cost = parseFloat($("#bottom_id option:selected").attr('data-price'))*total_area || 0;
            let machine_cost = parseFloat($("#work_id option:selected").attr('data-price'))*total_area || 0;
            let labeled_cost = parseFloat($("#labeled_id option:selected").attr('data-price'))*total_area || 0;
            let second_labeled_cost = parseFloat($("#second_labeled_id option:selected").attr('data-price'))*total_area || 0;
            let over_labeled_cost = parseFloat($("#over_labeled_id option:selected").attr('data-price'))*total_area || 0;
            let second_over_labeled_cost = parseFloat($("#second_over_labeled_id option:selected").attr('data-price'))*total_area || 0;
            let additional_cost = parseFloat($("#additional_id option:selected").attr('data-price'))*total_area || 0;
            let mod_cost = parseFloat($("#mod_id option:selected").attr('data-price'))*total_area || 0;
            let packaging_supplies_cost = parseFloat($("#packaging_supplies").val()) || 0;

            let decrease_base = parseFloat($("#decrease_base").val()) || 0;
            let decrease_bottom = parseFloat($("#decrease_bottom").val()) || 0;
            let decrease_labeled = parseFloat($("#decrease_labeled").val()) || 0;
            let decrease_second_labeled = parseFloat($("#decrease_second_labeled").val()) || 0;
            let decrease_over_labeled = parseFloat($("#decrease_over_labeled").val()) || 0;
            let decrease_second_over_labeled = parseFloat($("#decrease_second_over_labeled").val()) || 0;

            let margin = parseFloat($("#margin_id option:selected").text()) || 0;

            let unitary_amount;

            //Defino las variables

            let base_total_cost = base_cost*(1+0.01*decrease_base);
            let bottom_total_cost = bottom_cost*(1+0.01*decrease_bottom);
            let labeled_total_cost = labeled_cost*(1+0.01*decrease_labeled);
            let second_labeled_total_cost = second_labeled_cost*(1+0.01*decrease_second_labeled);
            let over_labeled_total_cost = over_labeled_cost*(1+0.01*decrease_over_labeled);
            let second_over_labeled_total_cost = second_over_labeled_cost*(1+0.01*decrease_second_over_labeled);
            //let additional__total_cost = additional_cost;

            //Monto unitario
            // MAAAAAAAAAAAAAAAAARGEEEEEEEEEEENr
            unitary_amount = base_total_cost+bottom_total_cost+labeled_total_cost+second_labeled_total_cost+over_labeled_total_cost+second_over_labeled_total_cost+additional_cost+mod_cost+packaging_supplies_cost+machine_cost;

            console.log("Total sin margen: "+unitary_amount);

            unitary_amount = unitary_amount*(1+0.01*margin);
            console.log("Total con margen: "+unitary_amount);

            mermaBase(total_area);
            mermaFondo(total_area);
            mermaRotulado();
            mermaSobrelaminado(total_area);

            console.log("Area total: "+total_area);
            console.log("Merma base: "+decrease_base);
            console.log("Merma fondo: "+decrease_bottom);
            console.log("Merma rotulado: "+decrease_labeled);
            console.log("Merma 2 rotulado: "+decrease_second_labeled);
            console.log("Merma sobrelaminado: "+decrease_over_labeled);
            console.log("Merma 2 sobrelaminado: "+decrease_second_over_labeled);
            console.log("Costo de trabajo: "+machine_cost);
            console.log("Costo de base: "+base_total_cost);
            console.log("Costo de fondo: "+bottom_total_cost);
            console.log("Costo de rotulado: "+labeled_total_cost);
            console.log("Costo de segundo rotulado: "+second_labeled_total_cost);
            console.log("Costo de sobrelaminado: "+over_labeled_total_cost);
            console.log("Costo de segundo sobrelaminado: "+second_over_labeled_total_cost);
            console.log("Costo de adicional: "+additional_cost);
            console.log("Costo de MOD: "+mod_cost);
            console.log("Costo de paquetes: "+packaging_supplies_cost);

            return unitary_amount;
        }

        function mermaBase(total_area) {

            let merma = 10;

            switch (total_area) {
                case 0.203:
                    merma = 15;
                    break;
                case 0.540:
                    merma = 15;
                    break;
                case 0.563:
                    merma = 15;
                    break;
                case 0.640:
                    merma = 18;
                    break;
                case 0.810:
                    merma = 18;
                    break;
                case 1:
                    merma = 20;
                    break;
            }

            $("#decrease_base").val(merma);
        }

        function mermaFondo(total_area) {

            let merma = 15;

            switch (total_area) {
                case 0.540:
                    merma = 20;
                    break;
                case 0.563:
                    merma = 25;
                    break;
            }

            $("#decrease_bottom").val(merma);
        }

        function mermaRotulado() {

            let merma = 25;

            //console.log(total_area);

            $("#decrease_labeled").val(merma);
            $("#decrease_second_labeled").val(merma);
        }

        function mermaSobrelaminado(total_area) {

            let merma = 15;

            switch (total_area) {
                case 0.540:
                    merma = 20;
                    break;
                case 0.563:
                    merma = 25;
                    break;
            }

            $("#decrease_over_labeled").val(merma);
            $("#decrease_second_over_labeled").val(merma);
        }

        truncateDecimals = function (number) {
            return Math[number < 0 ? 'ceil' : 'floor'](number);
        };



        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('input', '.quantity-quotation-inp', function(){
            let quotation_id = $(this).data('id');
            let quantity = $(this).val();

            console.log(quotation_id);
            console.log(quantity);

            $(".subtotal").html("...");
            $(this).closest( ".signal-row" ).find(".total-amount").html("...");

            quantityUpdateAjax(quotation_id, quantity);

        });
        function quantityUpdateAjax(model_id, model_value) {
            console.log("Copañia");
            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('quotations.quantityUpdate') }}',
                data: {
                    model_id: model_id,
                    model_value: model_value,
                },
                success: function (data) {
                    if (data == 1){
                        console.log("Se actualizó correctamente la señal con id: "+model_id);
                    }else{
                        alert("Hubo un error al actualizar la señal. Por favor inténtelo de nuevo.");
                    }

                    calcSubtotal();
                },error:function(){
                    console.log(data);
                }
            });
        }
        function calcSubtotal() {
            let subtotal = parseFloat(0);
            $( ".signal-row" ).each(function() {
                let signal_quantity = parseFloat($(this).find( ".quantity-quotation-inp" ).val());
                let signal_unitary_amount = parseFloat($(this).find( ".unit-amount" ).html());
                let subtotal_signal = 0;

                console.log("cantidad de señal: "+signal_quantity);
                console.log("costo unitario de señal: "+signal_unitary_amount);

                subtotal_signal = signal_quantity*signal_unitary_amount;
                subtotal_signal = subtotal_signal.toFixed(2);

                console.log("costo total de señal: "+subtotal_signal);

                $(this).find( ".total-amount" ).html(subtotal_signal);

                subtotal = ( parseFloat(subtotal) + parseFloat(subtotal_signal));

                console.log("Subtotal: "+subtotal);
            });

            console.log("Subtotal total: "+subtotal);
            $(".subtotal").html(parseFloat(subtotal).toFixed(2));
        }

    </script>
@endpush
