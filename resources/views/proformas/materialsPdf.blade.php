@php
    $objeto_bases = array();
    $objeto_fondos = array();
    $objeto_rotulados = array();
    $objeto_sobrelaminados = array();
    $objeto_adicionales = array();

    $work_cost = 0;
    $mod_cost = 0;
@endphp
@forelse($quotations as $quotation)
    @php
        //OBTENEMOS LOS DATOS DE MODELO
        $alto = $quotation->height;
        $ancho = $quotation->width;
        $area = $quotation->width*$quotation->height;
        $cantidad = $quotation->quantity;

        $base_codigo = $quotation->base->code;
        $base_codigo = $quotation->base->code;
        $fondo_codigo = $quotation->bottom->code;
        $rotulado_codigo = $quotation->labeled->code;
        $segundo_rotulado_codigo = $quotation->secondLabeled->code;
        $sobrelaminado_codigo = $quotation->overLabeled->code;
        $segundo_sobrelaminado_codigo = $quotation->secondOverLabeled->code;
        //$adicional_codigo = $quotation->additional->code;

        $merma_base = $quotation->decrease_base;
        $merma_fondo = $quotation->decrease_bottom;
        $merma_rotulado = $quotation->decrease_labeled;
        $merma_segundo_rotulado = $quotation->decrease_second_labeled;
        $merma_sobrelaminado = $quotation->decrease_over_labeled;
        $merma_segundo_sobrelaminado = $quotation->decrease_second_over_labeled;

        //DEFINIMOS LAS AREAS TOTALES CON MERMA
        $area_base = $area*(1 + 0.01*$merma_base)*$cantidad;
        $area_fondo = $area*(1 + 0.01*$merma_fondo)*$cantidad;
        $area_rotulado = $area*(1 + 0.01*$merma_rotulado)*$cantidad;
        $area_segundo_rotulado = $area*(1 + 0.01*$merma_segundo_rotulado)*$cantidad;
        $area_sobrelaminado = $area*(1 + 0.01*$merma_sobrelaminado)*$cantidad;
        $area_segundo_sobrelaminado = $area*(1 + 0.01*$merma_segundo_sobrelaminado)*$cantidad;

        //INSERTAMOS LOS VALORES EN ARREGLOS DONDE LOS CODIGOS SON LOS INDICES
        //SI EL CODIGO YA EXISTE EN EL INDICE, SE SUMA
        //SINO, SE AÑADE
        if($base_codigo != "NN" && $base_codigo != "00"){
            isset($objeto_bases[$base_codigo]) ?
            $objeto_bases[$base_codigo]+=$area_base :
            $objeto_bases[$base_codigo]=$area_base;
        }
        if($fondo_codigo != "NN" && $fondo_codigo != "00"){
            isset($objeto_fondos[$fondo_codigo]) ?
            $objeto_fondos[$fondo_codigo]+=$area_fondo :
            $objeto_fondos[$fondo_codigo]=$area_fondo;
        }
        if($rotulado_codigo != "NN" && $rotulado_codigo != "00"){
            isset($objeto_rotulados[$rotulado_codigo]) ?
            $objeto_rotulados[$rotulado_codigo]+=$area_rotulado :
            $objeto_rotulados[$rotulado_codigo]=$area_rotulado;
        }
        if($segundo_rotulado_codigo != "NN" && $segundo_rotulado_codigo != "00"){
            isset($objeto_rotulados[$segundo_rotulado_codigo]) ?
            $objeto_rotulados[$segundo_rotulado_codigo]+=$area_segundo_rotulado :
            $objeto_rotulados[$segundo_rotulado_codigo]=$area_segundo_rotulado;
        }
        if($sobrelaminado_codigo != "NN" && $sobrelaminado_codigo != "00"){
           isset($objeto_sobrelaminados[$sobrelaminado_codigo]) ?
           $objeto_sobrelaminados[$sobrelaminado_codigo]+=$area_sobrelaminado :
           $objeto_sobrelaminados[$sobrelaminado_codigo]=$area_sobrelaminado;
        }
        if($segundo_sobrelaminado_codigo != "NN" && $segundo_sobrelaminado_codigo != "00"){
           isset($objeto_sobrelaminados[$segundo_sobrelaminado_codigo]) ?
           $objeto_sobrelaminados[$segundo_sobrelaminado_codigo]+=$area_segundo_sobrelaminado :
           $objeto_sobrelaminados[$segundo_sobrelaminado_codigo]=$area_segundo_sobrelaminado;
        }

    //AUMENTAMOS EL COSTO DEL MOD Y TRABAJO

    $work_partial_cost = ($quotation->work->machine_price)*$area;
    $mod_partial_cost = ($quotation->mod->amount)*$area;

    $work_cost += $work_partial_cost;
    $mod_cost += $mod_partial_cost;


    @endphp
@empty

@endforelse

<style>

    .table-proforma, .table-proforma th, .table-proforma td{
        border: 2px solid #000;
    }

    .table-proforma{
        border-collapse: collapse;
        margin: 0 auto;
    }

    .table-proforma th, .table-proforma td{
        padding: 3px
    }
</style>
<div style="margin-bottom: 15px; font-weight: bold;text-align: center"> {{ $proforma->code }}</div>
<div style="margin-bottom: 15px;text-align: center"><b>Costo Trabajo:</b>  ${{ number_format($work_cost, 2, '.', "'") }}</div>
<div style="margin-bottom: 15px;text-align: center"><b>Costo MOD:</b>  ${{ number_format($mod_cost, 2, '.', "'") }}</div>
<table class="table table-proforma">
    <thead>
    <tr>
        <th scope="col">Tipo</th>
        <th scope="col">Código</th>
        <th scope="col">Area total (m2)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($objeto_bases as $codigo => $objeto)
        <tr>
            <td>Base</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}</td>
        </tr>
    @endforeach
    @foreach($objeto_fondos as $codigo => $objeto)
        <tr>
            <td>Fondo</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}</td>
        </tr>
    @endforeach
    @foreach($objeto_rotulados as $codigo => $objeto)
        <tr>
            <td>Rotulado</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}</td>
        </tr>
    @endforeach
    @foreach($objeto_sobrelaminados as $codigo => $objeto)
        <tr>
            <td>Sobrelaminado</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
