@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Proformas"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.proformas.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Código</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Validez</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Registró</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($proformas as $proforma)
                    <tr>
                        <td>{{ $proforma->id }}</td>
                        <td>{{ $proforma->code }}</td>
                        <td>{{ $proforma->date }}</td>
                        <td>{{ $proforma->validity_time }}</td>
                        <td>{{ $proforma->subtotal }}</td>
                        <td>{{ $proforma->customer->company_name }}</td>
                        <td>{{ $proforma->user->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('proformas.admin', $proforma->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Más información"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                            @if( !isset($proforma->orders->id))
                                <a
                                    class="btn btn-outline-info crear-orden"
                                    data-toggle="modal"
                                    data-target="#modalOrder"
                                    href="#modalOrder"
                                    data-id="{{ $proforma->id }}"
                                >
                                    <i class="fa fa-check-square-o"></i>
                                </a>
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('proformas.edit', $proforma->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            @endisset
                            <a
                                class="btn btn-outline-info"
                                href="{{action('ProformaController@downloadPDF', $proforma->id)}}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Descargar"

                            >
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalOrder">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Nueva orden</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content">

                        <form class="modal-content"
                              method="POST"
                              action="{{ route('orders.store') }}"
                              novalidate
                              style="border: 0"
                        >
                            @csrf
                            <div class="form-group" style="display: none">
                                <label for="proforma_id">Id de proforma:</label>
                                <input type="number" class="form-control" name="proforma_id" id="proforma_id">
                            </div>
                            <div class="form-group">
                                <label for="order_date">Día de orden:</label>
                                <div class="input-group date" id="order_date_tp" data-target-input="nearest">
                                    <input
                                        type="text"
                                        name="order_date"
                                        id="order_date"
                                        class="form-control datetimepicker-input"
                                        data-target="#order_date_tp"
                                        data-toggle="datetimepicker"
                                    />
                                    <div class="input-group-append" data-target="#order_date_tp" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="delivered_date">Día que se entregó:</label>
                                <div class="input-group date" id="delivered_date_tp" data-target-input="nearest">
                                    <input
                                        type="text"
                                        name="delivered_date"
                                        id="delivered_date"
                                        class="form-control datetimepicker-input"
                                        data-target="#delivered_date_tp"
                                        data-toggle="datetimepicker"
                                    />
                                    <div class="input-group-append" data-target="#delivered_date_tp" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="additional_details">Detalles adicionales:</label>
                                <textarea class="form-control" name="additional_details" id="additional_details"></textarea>
                            </div>
                            <div class="form-group">
                                <button
                                    type="submit"
                                    class="btn btn-outline-info">
                                    Agregar
                                </button>
                            </div>

                        </form>


                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $proformas->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#order_date_tp').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
            $('#delivered_date_tp').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        /*
        * Función para poner el id de la proforma
        * en el modal
        */
        $(document).on('click', '.crear-orden', function(){
            let proforma_id = $(this).data('id');

            console.log(proforma_id);

            $("#proforma_id").val(proforma_id);
        });

    </script>
@endpush
