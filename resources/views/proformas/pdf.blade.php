<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PRUEBAS</title>
</head>

<style>

    body{
        font-size: 11px;
    }

    .container{
        padding: 50px;
    }

    .header{
        background-color: #fff;
    }

    .logo-wrapp{
        display: inline-block;
        width: 32%;
        text-align: center
    }

    .img-header{
        max-width: 100%;
    }

    .direccion-header{
        text-align: center;
    }

    .direccion-header{
        padding: 5px;
        font-weight: bold;
    }

    .title-wrapp{
        text-align: center;
        text-decoration: underline;
        font-weight: bold;
        padding-top: 30px;
        padding-bottom: 30px;
        font-size: 14px;
    }

    .data-row{
        padding-bottom: 5px;
    }

    .data-title, .data-dscr{
        display: inline-block;
    }

    .data-title{
        width: 100px;
        font-weight: bold;
    }

    .saludo-wrapp{
        padding-top: 35px;
        font-size: 12px;
    }

    .saludo-dscr{
        padding-top: 8px;
    }

    .table-wrapp{
        padding-top: 18px;
    }

    .table-proforma, .table-proforma th, .table-proforma td{
        border: 2px solid #000;
        border-bottom: 0;
        border-top: 0;
    }

    .table-proforma{
        border-collapse: collapse;
        border-bottom: 0px solid #fff;
        border-left: 0;
        border-right: 0;
    }

    .table-proforma th, .table-proforma td{
        padding: 3px
    }

    .table-proforma th{
        border: 2px solid #000;
    }

    .table-proforma td:nth-child(1), .table-proforma td:nth-child(2), .table-proforma td:nth-child(3){
        text-align: center;
    }

    .table-proforma td:nth-child(5), .table-proforma td:nth-child(6){
        text-align: right;
    }

    .table-last-row td{
        border: 0px solid #fff;
    }

    .sub-total-row td{
        border: 0px solid #fff;
        border-top: 2px solid #000;
    }

    .sub-total-row td:nth-child(5), .sub-total-row td:nth-child(6), .sub-total-row td:nth-child(7),
    .table-last-row td:nth-child(5), .table-last-row td:nth-child(6), .table-last-row td:nth-child(7){
        border: 2px solid #000;
    }

    .table-proforma thead{
        text-align: center;
    }

    .table-proforma tbody{
        font-size: 9px;
    }

    .condiciones-row{
        padding-bottom: 5px;
    }

    .condiciones-titulo{
        text-align: center;
        font-weight: bold;
        padding: 15px
    }

    .condiciones-title, .condiciones-dscr{
        display: inline-block;
    }

    .condiciones-title{
        width: 120px;
        font-weight: bold;
    }

    .despedida-titulo{
        font-size: 12px;
    }

    .despedida-wrapp{
        padding-top: 35px;
        text-align: center;
    }
    .despedida-descripcion{
        padding-top: 15px;
        font-size: 12px;
    }
    /* table banks*/
    .table-banks-wrapp{
        padding-top: 18px;
    }

    .table-banks-proforma, .table-banks-proforma th, .table-banks-proforma td{
        border: 2px solid #000;
        border-bottom: 2px solid #000;
        border-top: 2px solid #000;
    }

    .table-banks-proforma{
        border-collapse: collapse;
        border-bottom: 1px solid #fff;
        border-left: 1px solid #fff;
        border-right: 1px solid #fff;
    }

    .table-banks-proforma th, .table-banks-proforma td{
        padding: 3px
    }

    .table-banks-proforma th{
        border: 2px solid #000;
    }

    .table-banks-proforma tbody{
        font-size: 9px;
    }
    .table-inn-dscr{
        padding-bottom: 4px;
    }

</style>

<body>



<div class="container">

    <div class="row">

        <div class="header">

            <div class="logo-wrapp">
                <img class="img-header ih-left" src="{{ asset('image/enne.png') }}">
            </div>
            <div class="logo-wrapp">
                <img class="img-header ih-left" src="{{ asset('image/visyttex.png') }}">
            </div>
            <div class="logo-wrapp">
                <img class="img-header ih-left" src="{{ asset('image/apex.png') }}">
            </div>
            <div class="direccion-header">
                Av. Aviacion 1584 - La Victoria / Ruc: 20554027360
            </div>

        </div>

    </div>

    <div class="title-wrapp">

        <div class="title">
            {{ $proforma->code }}
        </div>

    </div>

    <div class="data-wrapp">

        <div class="data-row">
            <div class="data-title">CLIENTE</div>:
            <div class="data-dscr">{{ $proforma->customer->company_name }}</div>
        </div>

        <div class="data-row">
            <div class="data-title">ATENCIÓN</div>:
            <div class="data-dscr">{{ $proforma->proforma_name }}</div>
        </div>

        <div class="data-row">
            <div class="data-title">MAIL</div>:
            <div class="data-dscr">{{ $proforma->proforma_mail }}</div>
        </div>

        <div class="data-row">
            <div class="data-title">FECHA</div>:
            <div class="data-dscr">
                @php

                    \Jenssegers\Date\Date::setLocale('es');

                    $date = $proforma->date;

                    $dateObj   = \Jenssegers\Date\Date::createFromDate($date);
                    $dateFormated = $dateObj->format('d \d\e F \d\e\l Y'); // March

                @endphp
                {{ $dateFormated }}
            </div>
        </div>

    </div>


    <div class="saludo-wrapp">

        <div class="saludo-inicial">
            De nuestra consideración:
        </div>
        <div class="saludo-dscr">
            Por medio de la presente, les hacemos llegar nuestra mejor oferta, de acuerdo a su solicitud.
        </div>

    </div>

    <div class="table-wrapp">
        <div class="table-inn">


            <table class="table-proforma">
                <thead>
                <tr>
                    <th style="width: 16px;">ITEM</th>
                    <th style="width: 16px;">CANT.<br>UNDS.</th>
                    <th style="width: 50px;">SEÑAL</th>
                    <th style="width: 315px;">DESCRIPCIÓN</th>
                    <th style="width: 40px;">P. UNIT. <br>US$</th>
                    <th style="width: 49px;">P. TOTAL. <br>US$ - SIN<br>IGV</th>
                </tr>
                </thead>
                <tbody>

                @php
                    $temp_count = 0;
                    $roman_temp = '';

                    $quotation_amount=0;
                    $total_amount = 0;
                    $igv = 0;
                    $total_amount_igv = 0;
                @endphp
                @foreach($quotations as $quotation)
                    @php
                        $quotation_amount=$quotation->unitary_amount*$quotation->quantity;
                        $total_amount+=$quotation_amount;

                        $temp_count++;
                        //$roman_temp = \App\Proforma::numberToRomanRepresentation($temp_count);
                    @endphp
                    <tr>
                        <td>{{ $temp_count }}</td>
                        <td>{{ $quotation->quantity }}</td>
                        <td style="text-align:center">
                            @php
                                $pre_img='image/signals/';

                                switch ($quotation->signal->signal_type_id) {
                                    case 1:
                                        $pre_img.='reglamentarias';
                                        break;
                                    case 2:
                                        $pre_img.='preventivas';
                                        break;
                                    case 3:
                                        $pre_img.='informativas';
                                        break;
                                    case 4:
                                        $pre_img.='de_obra';
                                        break;
                                    case 5:
                                        $pre_img.='incendios';
                                        break;
                                    case 6:
                                        $pre_img.='advertencia';
                                        break;
                                    case 7:
                                        $pre_img.='obligacion';
                                        break;
                                    case 8:
                                        $pre_img.='evacuacion';
                                        break;
                                    default:
                                        $pre_img.='reglamentarias';
                                }
                                $pre_img.='/';

                            @endphp

                            <img style="height: 50px" src="{{ asset($pre_img.$quotation->signal->picture ) }}">
                        </td>
                        <td>{{ $quotation->description }}</td>
                        <td>{{ number_format($quotation->unitary_amount, 2, '.', "'") }}</td>
                        <td>{{ number_format($quotation_amount, 2, '.', "'") }}</td>
                    </tr>
                @endforeach

                @php
                    $igv = $total_amount*0.18;
                    $total_amount_igv = $total_amount+$igv;
                @endphp
                <tr class="sub-total-row">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Sub-total</b></td>
                    <td>{{ number_format($total_amount, 2, '.', "'") }}</td>
                </tr>
                <tr class="table-last-row">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>IGV 18 %</b></td>
                    <td>{{ number_format($igv, 2, '.', "'") }}</td>
                </tr>
                <tr class="table-last-row">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Total US$</b></td>
                    <td>{{ number_format($total_amount_igv, 2, '.', "'") }}</td>
                </tr>
                </tbody>
            </table>


        </div>
    </div>


    <div class="condiciones-wrapp">

        <div class="condiciones-inn">

            <div class="condiciones-titulo">
                CONDICIONES COMERCIALES
            </div>

            <div class="condiciones-descripcion">

                <div class="condiciones-row">
                    <div class="condiciones-title">Forma de pago</div>:
                    <div class="condiciones-dscr">Contado</div>
                </div>

                <div class="condiciones-row">
                    Los precios están expresados en Dólares Americanos (US$)
                </div>

                <div class="condiciones-row">
                    <div class="condiciones-title">Tiempo de entrega</div>:
                    <div class="data-dscr">{{ $proforma->delivery_time }} días</div>
                </div>

                <div class="condiciones-row">
                    <div class="condiciones-title">Validez de oferta</div>:
                    <div class="condiciones-dscr">{{ $proforma->validity_time }} días</div>
                </div>

            </div>

        </div>

    </div>

    <div class="condiciones-wrapp">

        <div class="condiciones-inn">

            <div class="condiciones-titulo">
                CUENTAS BANCARIAS
            </div>


            <div class="table-wrapp">
                <div class="table-inn">
                    <div class="table-inn-dscr">
                        FEDSEG SAC RUC 20602534457 
                    </div>
                    
                    <table class="table-banks-proforma">
                        <thead>
                        <tr>
                            <th style="width: 16px;">TIPO</th>
                            <th style="width: 200px;">SOLES</th>
                            <th style="width: 200px;">DÓLARES</th>
                            <th style="width: 16px;">BANCO</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>CTA.CTE</td>
                          <td>0011-0360-0100060599</td>
                          <td>0011-0360-0100060602</td>
                          <td>BBVA</td>
                        </tr>
                        <tr>
                          <td>CCI</td>
                          <td>011-36000010006059956</td>
                          <td>011-36000010006060258</td>
                          <td>BBVA</td>
                        </tr>
                        <tr>
                          <td>CTA.CTE</td>
                          <td>193-2464192-0-27</td>
                          <td>193-2460874-1-21</td>
                          <td>BCP</td>
                        </tr>
                        <tr>
                          <td>CCI</td>
                          <td>002-19300246419202717</td>
                          <td>002-19300246087412111</td>
                          <td>BCP</td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            </div>

        </div>

    </div>

    <div class="despedida-wrapp">

        <div class="despedida-inn">

            <div class="despedida-titulo" style="font-size: 13px">
                Atentamente,
            </div>

            <div class="despedida-descripcion">

                <div class="despedida-row">
                    <b>{{ $proforma->user->name." ".$proforma->user->last_name }}</b>
                </div>
                <div class="despedida-row">
                    Teléfonos: {{ $proforma->user->phone }} / Celular: {{ $proforma->user->cellphone }}
                </div>
                <div class="despedida-row">
                    Email: {{ $proforma->user->email }}
                </div>

            </div>

        </div>

    </div>



</div>




</body>
</html>
