@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Proforma"),
        'icon' => "file-text-o"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $proforma->id ? route('proformas.store'): route('proformas.update', $proforma->id) }}"
            novalidate
        >
            @if($proforma->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group company-group">
                <label>Empresa*</label>
                <a
                    class="btn btn-outline-info"
                    data-toggle="modal"
                    data-target="#modalCompany"
                    href="#modalCompany"
                >
                    Seleccionar empresa
                </a>
            </div>
            <div class="form-group" style="display: none">
                <input
                    type="text"
                    class="form-control {{ $errors->has('customer_id') ? 'is-invalid': '' }}"
                    name="customer_id"
                    id="customer_id"
                    placeholder="Id de compañía"
                    value="{{ old('customer_id') ?: $proforma->customer_id }}"
                    required
                >
            </div>
            <div class="form-group company-group">
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_id') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    placeholder="Empresa"
                    value="@if(old('company_name')){{ old('company_name')  }}
                    @elseif($proforma->customer_id)@php
                        echo $proforma->customer->company_name;
                    @endphp
                    @endif"
                    readonly
                >
                @if($errors->has('company_id'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('company_id') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="proforma_name">Nombre completo de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('proforma_name') ? 'is-invalid': '' }}"
                    name="proforma_name"
                    id="proforma_name"
                    value="{{ old('proforma_name') ?: $proforma->proforma_name }}"
                >
                @if($errors->has('proforma_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('proforma_name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="proforma_mail">Correo de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('proforma_mail') ? 'is-invalid': '' }}"
                    name="proforma_mail"
                    id="proforma_mail"
                    value="{{ old('proforma_mail') ?: $proforma->proforma_mail }}"
                >
                @if($errors->has('proforma_mail'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('proforma_mail') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="additional_details">Detalles adicionales</label>
                <textarea
                    type="text"
                    class="form-control {{ $errors->has('additional_details') ? 'is-invalid': '' }}"
                    name="additional_details"
                    id="additional_details">{{ old('additional_details') ?: $proforma->additional_details }}</textarea>
                @if($errors->has('additional_details'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('additional_details') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="date">Día de cotización</label>
                <div class="input-group date" id="date_tp" data-target-input="nearest">
                    <input
                        type="text"
                        name="date"
                        id="date"
                        class="form-control datetimepicker-input {{ $errors->has('date') ? 'is-invalid': '' }}"
                        data-target="#date_tp"
                        data-toggle="datetimepicker"
                        value="{{ old('date') ?: $proforma->date }}"
                    />
                    <div class="input-group-append" data-target="#date_tp" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    @if($errors->has('date'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="delivery_time">Tiempo de entrega (días)</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('delivery_time') ? 'is-invalid': '' }}"
                    name="delivery_time"
                    id="delivery_time"
                    value="{{ old('delivery_time') ?: $proforma->delivery_time }}"
                >
                @if($errors->has('delivery_time'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('delivery_time') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="validity_time">Tiempo de validez (días)</label>
                <input
                    type="number"
                    class="form-control {{ $errors->has('validity_time') ? 'is-invalid': '' }}"
                    name="validity_time"
                    id="validity_time"
                    value="{{ old('validity_time') ?: $proforma->validity_time }}"
                >
                @if($errors->has('validity_time'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('validity_time') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
    <!-- Modal para la empresa -->
    @include('partials.proformas.companyModal')

@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#date_tp').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                locale: 'es'
            });
        });
        /*
        * Función para obtener el id y el nombre de la compañía,
        * según el RUC brindado
        */
        $(document).on('click', '.company-search-btn', function(){

            let company_search;
            let company_search_name;

            company_search = $("#modalCompany .company-search").val();
            company_search_name = $("#modalCompany .company-search-name").val();
            companyRucAjax(company_search, company_search_name);

        });

        /*
        * Función para añadir el id del servicio y el nombre del cliente
        * al input del formulario
        */
        $(document).on('click', '.company-add-btn', function(){

            let company_id;
            let contact_name;
            let contact_email;
            let company_name;

            //Limpiamos los datos de companía
            $("#customer_id").val("");
            $("#company_name").val("");

            company_id = $("#modalCompany .id-filtered").val();
            contact_name = $("#modalCompany .contact-name").val();
            contact_email = $("#modalCompany .contact-email").val();
            company_name = $("#modalCompany .company-filtered").val();

            $("#customer_id").val(company_id);
            $("#proforma_name").val(contact_name);
            $("#proforma_mail").val(contact_email);
            $("#company_name").val(company_name);

            $(".company-search").val("");
            $(".company-search-name").val("");
            $(".company-filtered").val("");
            $(".id-filtered").val("");
            $(".contact-name").val("");
            $(".contact-email").val("");
        });

        function companyRucAjax(company_search, company_search_name) {
            console.log("Copañia");
            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('customers.rucSearch') }}',
                data: {
                    tax_number: company_search,
                    company_name: company_search_name,
                },
                success: function (data) {
                    let company_name = "";
                    let contact_name = "";
                    let contact_last_name = "";
                    let contact_email = "";
                    let company_id = null;

                    /*
                    * Obtenemos los datos, sólo si existen
                    */
                    if (
                        typeof(data['id']) != "undefined"
                        &&
                        data['name'] !== null){

                        company_id = data['id'];
                        company_name = data['company_name'];
                        contact_name = data['contact_name'];
                        contact_last_name = data['contact_last_name'];
                        contact_email = data['contact_email'];

                    }else{
                        console.log("No se encontró compañia");
                        return;
                    }

                    /*
                    * Si es servicio es contablidad o constitución,
                    * el id del servicio y en nombre de la empresa
                    * se guarda en un input escondido y en un input
                    * no editable, dentro del modal
                    */

                    $("#modalCompany .company-filtered").val(company_name);
                    $("#modalCompany .contact-name").val(contact_name+" "+contact_last_name);
                    $("#modalCompany .contact-email").val(contact_email);
                    $("#modalCompany .id-filtered").val(company_id);
                },error:function(){
                    console.log(data);
                }
            });
        }
    </script>
@endpush
