@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Clientes"),
        'icon' => "building"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.customers.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Contacto</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Registró</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->company_name }}</td>
                        <td>{{ $customer->contact_name." ".$customer->contact_lat_name }}</td>
                        <td>{{ $customer->contact_cellphone }}</td>
                        <td>{{ $customer->contact_email }}</td>
                        <td>{{ $customer->user->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('customers.admin', $customer->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Más información"
                            >
                                <i class="fa fa-info-circle"></i>
                            </a>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('customers.edit', $customer->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Editar"
                            >
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay clientes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $customers->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
