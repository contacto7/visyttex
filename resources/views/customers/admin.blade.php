@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("CLIENTE"),
        'icon' => "building"
    ])
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <div class="row mt-3 mb-3">
            <table class="table table-striped table-light">
                <tbody>
                <tr>
                    <th>EMPRESA</th>
                    <td>{{ $customer->company_name }}</td>
                </tr>
                <tr>
                    <th>RUC</th>
                    <td>{{ $customer->tax_number }}</td>
                </tr>
                <tr>
                    <th>DIRECCIÓN</th>
                    <td>{{ $customer->address }}</td>
                </tr>
                <tr>
                    <th>TELÉFONO</th>
                    <td>{{ $customer->phone }}</td>
                </tr>
                <tr>
                    <th>CONTACTO</th>
                    <td>{{ $customer->contact_name." ".$customer->contact_last_name }}</td>
                </tr>
                <tr>
                    <th>TELÉFONO DE CONTACTO</th>
                    <td>{{ $customer->contact_cellphone }}</td>
                </tr>
                <tr>
                    <th>CORREO DE CONTACTO</th>
                    <td>{{ $customer->contact_email }}</td>
                </tr>
                <tr>
                    <th>REGISTRÓ</th>
                    <td>{{ $customer->user->name }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
