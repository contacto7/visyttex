@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Customer"),
        'icon' => "file-text-o"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $customer->id ? route('customers.store'): route('customers.update', $customer->id) }}"
            novalidate
        >
            @if($customer->id)
                @method('PUT')
            @endif

            @csrf



            <div class="form-group">
                <label for="company_name">Razón Social</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_name') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    value="{{ old('company_name') ?: $customer->company_name }}"
                >
                @if($errors->has('company_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="tax_number">RUC</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('tax_number') ? 'is-invalid': '' }}"
                    name="tax_number"
                    id="tax_number"
                    value="{{ old('tax_number') ?: $customer->tax_number }}"
                >
                @if($errors->has('tax_number'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('tax_number') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="address">Dirección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    value="{{ old('address') ?: $customer->address }}"
                >
                @if($errors->has('address'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="phone">Teléfono</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('phone') ? 'is-invalid': '' }}"
                    name="phone"
                    id="phone"
                    value="{{ old('phone') ?: $customer->phone }}"
                >
                @if($errors->has('phone'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="contact_name">Nombres de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_name') ? 'is-invalid': '' }}"
                    name="contact_name"
                    id="contact_name"
                    value="{{ old('contact_name') ?: $customer->contact_name }}"
                >
                @if($errors->has('contact_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="contact_last_name">Apellidos de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_last_name') ? 'is-invalid': '' }}"
                    name="contact_last_name"
                    id="contact_last_name"
                    value="{{ old('contact_last_name') ?: $customer->contact_last_name }}"
                >
                @if($errors->has('contact_last_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_last_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="contact_cellphone">Teléfono de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_cellphone') ? 'is-invalid': '' }}"
                    name="contact_cellphone"
                    id="contact_cellphone"
                    value="{{ old('contact_cellphone') ?: $customer->contact_cellphone }}"
                >
                @if($errors->has('contact_cellphone'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_cellphone') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="contact_email">Correo de contacto</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('contact_email') ? 'is-invalid': '' }}"
                    name="contact_email"
                    id="contact_email"
                    value="{{ old('contact_email') ?: $customer->contact_email }}"
                >
                @if($errors->has('contact_email'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('contact_email') }}</strong>
                </span>
                @endif
            </div>



            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
@endsection
