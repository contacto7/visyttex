@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("USUARIO"),
        'icon' => "user"
    ])
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <div class="row mt-3 mb-3">
            <table class="table table-striped table-light">
                <tbody>
                <tr>
                    <th>NOMBRES</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th>APELLIDOS</th>
                    <td>{{ $user->last_name }}</td>
                </tr>
                <tr>
                    <th>CORREO</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>TELÉFONO</th>
                    <td>{{ $user->phone }}</td>
                </tr>
                <tr>
                    <th>CELULAR</th>
                    <td>{{ $user->cellphone }}</td>
                </tr>
                <tr>
                    <th>PUESTO</th>
                    <td>{{ $user->position }}</td>
                </tr>
                <tr>
                    <th>CREADO</th>
                    <td>{{ $user->created_at }}</td>
                </tr>
                <tr>
                    <th>MODIFICADO</th>
                    <td>{{ $user->updated_at }}</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
