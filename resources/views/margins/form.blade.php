@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("Márgenes"),
        'icon' => "fa-user"
    ])
@endsection


@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ route('margins.store') }}"
            novalidate
        >
            @csrf


            <div class="form-group">
                <label for="percentage">Margen (%)</label>
                <input
                    type="number"
                    min="1"
                    step="1"
                    class="form-control {{ $errors->has('percentage') ? 'is-invalid': '' }}"
                    name="percentage"
                    id="percentage"
                    value="{{ old('percentage') ?: "" }}"
                >
                @if($errors->has('percentage'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('percentage') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="type">Vendor</label>
                <select
                    class="form-control {{ $errors->has('type') ? 'is-invalid': '' }}"
                    name="type"
                    id="type"
                >
                    <option
                        {{
                            (int) old('type') === \App\Margin::INTERNO ?'selected' : ''
                        }}
                        value="{{ \App\Margin::INTERNO }}"
                    >Interno</option>
                    <option
                        {{
                            (int) old('type') === \App\Margin::EXTERNO ?'selected' : ''
                        }}
                        value="{{ \App\Margin::EXTERNO }}"
                    >Externo</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __("Crear margen") }}
                </button>
            </div>

        </form>

    </div>
@endsection
