@extends('layouts.app')


@section('title')
    @include('partials.title', [
        'title' => __("Margenes"),
        'icon' => "user"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="form-group row">
            <div class="col-sm-3 btn-group">
                <a
                    class="form-control btn btn-visyttex"
                    href="{{ route('margins.create') }}"
                >
                    Añadir
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Margen (%)</th>
                    <th scope="col">Vendedor</th>
                    <th scope="col">Añadido</th>
                </tr>
                </thead>
                <tbody>
                @forelse($margins as $margin)
                    <tr>
                        <td>{{ $margin->id }}</td>
                        <td>
                            {!! $margin->percentage !!}
                        </td>
                        <td>{{ $margin->type ==1?"Interno":"Externo" }}</td>
                        <td>{{ $margin->created_at }}</td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay márgenes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $margins->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
