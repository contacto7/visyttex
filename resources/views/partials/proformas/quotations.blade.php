
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Señal</th>
        <th scope="col">Cantidad</th>
        <th scope="col">Código</th>
        <th scope="col">Descripción</th>
        <th scope="col">Trabajo</th>
        <th scope="col">P.Unit.<br>US$</th>
        <th scope="col">P.Tot.<br>US$</th>
        <th scope="col">Ver</th>
    </tr>
    </thead>
    <tbody>
    @forelse($quotations as $quotation)
        <tr class="signal-row">
            <td>{{ $quotation->id }}</td>
            <td>
                <div class="senal-img-wrp">

                    @php
                        $pre_img='image/signals/';

                        switch ($quotation->signal->signal_type_id) {
                            case 1:
                                $pre_img.='reglamentarias';
                                break;
                            case 2:
                                $pre_img.='preventivas';
                                break;
                            case 3:
                                $pre_img.='informativas';
                                break;
                            case 4:
                                $pre_img.='de_obra';
                                break;
                            case 5:
                                $pre_img.='incendios';
                                break;
                            case 6:
                                $pre_img.='advertencia';
                                break;
                            case 7:
                                $pre_img.='obligacion';
                                break;
                            case 8:
                                $pre_img.='evacuacion';
                                break;
                            default:
                                $pre_img.='reglamentarias';
                        }
                        $pre_img.='/';

                    @endphp

                    <img class="senal-img" src="{{ asset($pre_img.$quotation->signal->picture ) }}">
                </div>
                <div style="text-align: center">{{ $quotation->signal->name }}</div>
            </td>
            <td>
                <input
                    type="number"
                    class="form-control quantity-quotation-inp"
                    data-id="{{ $quotation->id }}"
                    value="{{ $quotation->quantity }}">
            </td>
            <td>{{ $quotation->proforma_code }}</td>
            <td>{{ $quotation->description }}</td>
            <td>{{ $quotation->work->name }}</td>
            <td class="unit-amount">{{ $quotation->unitary_amount }}</td>
            <td class="total-amount">{{ $quotation->unitary_amount*$quotation->quantity }}</td>
            <td>
                @can('viewAny', [\App\Quotation::class] )
                <a
                    class="btn btn-outline-info"
                    href="{{ route('quotations.admin', $quotation->id) }}"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Más información"
                >
                    <i class="fa fa-info-circle"></i>
                </a>
                @endcan
                <a
                    class="btn btn-outline-info"
                    href="{{ route('quotations.delete', $quotation->id) }}"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Eliminar"
                >
                    <i class="fa fa-times"></i>
                </a>
            </td>
        </tr>
    @empty
        <tr>
            <td>{{ __("No hay clientes disponibles")}}</td>
        </tr>
    @endforelse
    </tbody>
</table>
