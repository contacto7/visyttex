
<!-- The Modal -->
<div class="modal" id="modalSignal" data-type="1">
    <div class="modal-dialog modal-dialog-centered">
        <form class="modal-content"
              method="POST"
              action="{{ route('quotations.store') }}"
              novalidate
        >
        @csrf
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Agregar Señal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <!-- PARTE 0 -->
                <div class="supertipo-senal-wrapp">
                    <div class="supertipo-senal-inn">
                        <div
                            class="supertipo-senal pasos-cotizacion"
                            data-hide="supertipo-senal-wrapp"
                            data-target="tipo-senal-wrapp"
                        >
                            <a class="txt-tipo-senal">-> Señales viales</a>
                        </div>
                        <div
                            class="supertipo-senal pasos-cotizacion"
                            data-hide="supertipo-senal-wrapp"
                            data-target="tipo1-senal-wrapp"
                        >
                            <a class="txt-tipo-senal">-> Señales de seguridad</a>
                        </div>
                    </div>

                </div>
                <!-- PARTE 1 -->
                <div class="tipo-senal-wrapp">
                    <div class="tipo-senal-inn">
                        <div class="btn-block text-left mb-2">
                            <a class="pasos-cotizacion"
                               data-hide="tipo-senal-wrapp"
                               data-target="supertipo-senal-wrapp"
                               href="#"
                            >
                                <- Regresar
                            </a>
                        </div>
                        <div
                            class="tipo-senal senal-regla pasos-cotizacion"
                            data-hide="tipo-senal-wrapp"
                            data-target="senales-regla"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/reglamentarias.png') }}">
                            <div class="txt-tipo-senal">Reglamentarias</div>
                        </div>
                        <div
                            class="tipo-senal senal-preve pasos-cotizacion"
                            data-hide="tipo-senal-wrapp"
                            data-target="senales-preve"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/preventivas.png') }}">
                            <div class="txt-tipo-senal">Preventivas</div>
                        </div>
                        <div
                            class="tipo-senal senal-info pasos-cotizacion"
                            data-hide="tipo-senal-wrapp"
                            data-target="senales-info"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/informativas.png') }}">
                            <div class="txt-tipo-senal">Informativas</div>
                        </div>
                        <div
                            class="tipo-senal senal-obra pasos-cotizacion"
                            data-hide="tipo-senal-wrapp"
                            data-target="senales-obra"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/de_obra.png') }}">
                            <div class="txt-tipo-senal">De obra</div>
                        </div>

                    </div>

                </div>
                <!-- PARTE 1-2 -->
                <div class="tipo1-senal-wrapp">
                    <div class="tipo-senal-inn">
                        <div class="btn-block text-left mb-2">
                            <a class="pasos-cotizacion"
                               data-hide="tipo1-senal-wrapp"
                               data-target="supertipo-senal-wrapp"
                               href="#"
                            >
                                <- Regresar
                            </a>
                        </div>
                        <div
                            class="tipo-senal senal-regla pasos-cotizacion"
                            data-hide="tipo1-senal-wrapp"
                            data-target="senales-incendios"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/contra_incendios.png') }}">
                            <div class="txt-tipo-senal">Equipo contra incendios</div>
                        </div>
                        <div
                            class="tipo-senal senal-preve pasos-cotizacion"
                            data-hide="tipo1-senal-wrapp"
                            data-target="senales-advertencia"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/advertencia.png') }}">
                            <div class="txt-tipo-senal">Advertencia</div>
                        </div>
                        <div
                            class="tipo-senal senal-info pasos-cotizacion"
                            data-hide="tipo1-senal-wrapp"
                            data-target="senales-obligacion"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/obligacion.png') }}">
                            <div class="txt-tipo-senal">Obligación</div>
                        </div>
                        <div
                            class="tipo-senal senal-obra pasos-cotizacion"
                            data-hide="tipo1-senal-wrapp"
                            data-target="senales-evacuacion"
                        >
                            <img class="img-tipo-senal" src="{{ asset('image/evacuacion.png') }}">
                            <div class="txt-tipo-senal">Evacuación y emergencia</div>
                        </div>

                    </div>

                </div>

                <!-- PARTE 2 -->
                <div class="senales-wrapp">

                    <!-- SEÑALES VIALES -->
                    <div
                        class="senales-regla justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-regla"
                                   data-target="tipo-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::REGLAMENTARIAS)->get() as $signal)
                                <div
                                    class="senal pasos-cotizacion"
                                    data-id="{{ $signal->id }}"
                                    data-hide="senales-regla"
                                    data-target="properties-wrapp"
                                    data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/reglamentarias/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>

                            @endforeach
                        </div>

                    </div>
                    <div
                        class="senales-preve justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-preve"
                                   data-target="tipo-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::PREVENTIVAS)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-preve"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/preventivas/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div
                        class="senales-info justify-content-center"
                        data-hide="senales-info"
                        data-target="properties-wrapp"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-info"
                                   data-target="tipo-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::INFORMATIVAS)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-info"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/informativas/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div
                        class="senales-inn senales-obra justify-content-center"
                        data-hide="senales-obra"
                        data-target="properties-wrapp"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-obra"
                                   data-target="tipo-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::DEOBRA)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-obra"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/de_obra/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- SEÑALES DE SEGURIDAD -->
                    <div
                        class="senales-incendios justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-incendios"
                                   data-target="tipo1-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::CONTRA_INCEDIOS)->get() as $signal)
                                <div
                                    class="senal pasos-cotizacion"
                                    data-id="{{ $signal->id }}"
                                    data-hide="senales-incendios"
                                    data-target="properties-wrapp"
                                    data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/incendios/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>

                            @endforeach
                        </div>

                    </div>
                    <div
                        class="senales-advertencia justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-advertencia"
                                   data-target="tipo1-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::ADVERTENCIA)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-advertencia"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/advertencia/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div
                        class="senales-obligacion justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-obligacion"
                                   data-target="tipo1-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::OBLIGACION)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-obligacion"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/obligacion/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div
                        class="senales-evacuacion justify-content-center"
                    >
                        <div class="senales-inn">
                            <div class="btn-block text-left mb-2">
                                <a class="pasos-cotizacion"
                                   data-hide="senales-evacuacion"
                                   data-target="tipo1-senal-wrapp"
                                   href="#"
                                >
                                    <- Regresar
                                </a>
                            </div>
                            @foreach(\App\Signal::where('signal_type_id', \App\SignalType::EVACUACION)->get() as $signal)
                                <div class="senal pasos-cotizacion" data-id="{{ $signal->id }}"
                                     data-hide="senales-evacuacion"
                                     data-target="properties-wrapp"
                                     data-code="{{ $signal->code }}"
                                >
                                    <div class="senal-img-wrp">
                                        <img class="senal-img" src="{{ asset('image/signals/evacuacion/'.$signal->picture ) }}">
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->code }}
                                    </div>
                                    <div class="senal-txt">
                                        {{ $signal->name }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- PARTE 3 -->
                <div class="properties-wrapp">
                    <div class="properties-inn">
                        <div class="btn-block text-left mb-2">
                            <a class="pasos-cotizacion"
                               data-hide="properties-wrapp"
                               data-target="supertipo-senal-wrapp"
                               href="#"
                            >
                                <- Regresar
                            </a>
                        </div>
                        <div class="form-group">
                            <label>Señal:</label>
                            <label><b id="nombre-senal"></b></label>
                            <div style="text-align: center">
                                <div class="senal-img-wrp">
                                    <img class="senal-img" id="img-senal" src="{{ asset('image/logo-png' ) }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="proforma_id">Id de proforma:</label>
                            <input type="number" class="form-control" name="proforma_id" id="proforma_id" value="{{ $proforma->id }}">
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="proforma_code">Código de proforma:</label>
                            <input type="text" class="form-control" name="proforma_code" id="proforma_code" value="{{ $proforma->proforma_code }}">
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="currency_id">Id de moneda:</label>
                            <select
                                class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                                name="currency_id"
                                id="currency_id"
                                required
                            >
                                @foreach(\App\Currency::get() as $currency)
                                    <option
                                        {{ (int) old('currency_id') === $currency->id || $proforma->currency_id === $currency->id ? 'selected' : '' }}
                                        value="{{ $currency->id }}"
                                    >{{ $currency->symbol }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="signal_id">Id de señal:</label>
                            <input type="number" class="form-control" name="signal_id" id="signal_id" data-code="">
                        </div>
                        <div
                            class="form-group"
                            style="{{ auth()->user()->role_id === \App\Role::EXTERNAL_WORKER ? "display:none":"" }}"
                        >
                            <label for="margin_id">Margen de ganancia (%):</label>
                            <select
                                class="form-control {{ $errors->has('margin_id') ? 'is-invalid': '' }}"
                                name="margin_id"
                                id="margin_id"
                                required
                            >
                                @foreach($margins as $margin)
                                    <option
                                        {{ (int) old('additional_id') === $margin->id || $proforma->additional_id === $margin->id ? 'selected' : '' }}
                                        value="{{ $margin->id }}"
                                    >{{ $margin->percentage }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Descripción adicional:</label>
                            <textarea type="text" class="form-control" name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Cantidad:</label>
                            <input type="number" class="form-control" name="quantity" id="quantity">
                        </div>
                        <div class="form-group">
                            <label for="height">Altura (m):</label>
                            <input type="number" class="form-control modificador-precio" name="height" id="height">
                        </div>
                        <div class="form-group">
                            <label for="width">Ancho (m):</label>
                            <input type="number" class="form-control modificador-precio" name="width" id="width">
                        </div>
                        <div class="form-group">
                            <label for="work_id">Trabajo:</label>
                            <select
                                class="form-control {{ $errors->has('work') ? 'is-invalid': '' }} modificador-precio modificar-mod"
                                name="work_id"
                                id="work_id"
                                required
                            >
                                @foreach(\App\Work::get() as $work)
                                    <option
                                        {{ (int) old('work_id') === $work->id || $proforma->work_id === $work->id ? 'selected' : '' }}
                                        value="{{ $work->id }}"
                                        data-code="{{ $work->code }}"
                                        data-price="{{ $work->machine_price }}"
                                    >{{ $work->code." - ".$work->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="packaging_supplies">Embalaje e insumos ($):</label>
                            <input type="number" class="form-control modificador-precio" name="packaging_supplies" id="packaging_supplies" value="0.1">
                        </div>

                        <div class="form-group">
                            <label for="mod_id">MOD:</label>
                            <select
                                class="form-control {{ $errors->has('mod_id') ? 'is-invalid': '' }} modificador-precio"
                                name="mod_id"
                                id="mod_id"
                                required
                            >
                                @foreach(\App\Mod::get() as $mod)
                                    <option
                                        data-work="{{ $mod->work->id }}"
                                        {{ (int) old('mod_id') === $mod->id || $proforma->mod_id === $mod->id ? 'selected' : '' }}
                                        value="{{ $mod->id }}"
                                        data-price="{{ $mod->amount }}"
                                        style="display: none"
                                    >{{ $mod->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <a class="btn btn-outline-info pasos-cotizacion modificador-precio"
                               data-hide="properties-wrapp"
                               data-target="works-wrapp"
                            >
                                Siguiente
                            </a>
                        </div>
                    </div>
                </div>
                <!-- PARTE 4 -->
                <div class="works-wrapp">
                    <div class="works-inn">
                        <div class="btn-block text-left mb-2">
                            <a class="pasos-cotizacion"
                               data-hide="works-wrapp"
                               data-target="properties-wrapp"
                               href="#"
                            >
                                <- Regresar
                            </a>
                        </div>
                        <div class="form-group">
                            <label for="base_id">Base:</label>
                            <select
                                class="form-control {{ $errors->has('base_id') ? 'is-invalid': '' }} modificador-precio"
                                name="base_id"
                                id="base_id"
                                required
                            >
                                @foreach(\App\Base::get() as $base)
                                    <option
                                        {{ (int) old('base_id') === $base->id || $proforma->base_id === $base->id ? 'selected' : '' }}
                                        value="{{ $base->id }}"
                                        data-code="{{ $base->code }}"
                                        data-price="{{ $base->price }}"
                                    >{{ $base->code." - ".$base->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_base">Merma base (%):</label>
                            <input
                                type="number"
                                class="form-control modificador-precio"
                                name="decrease_base"
                                id="decrease_base"
                            >
                        </div>

                        <div class="form-group">
                            <label for="bottom_id">Fondo:</label>
                            <select
                                class="form-control {{ $errors->has('bottom_id') ? 'is-invalid': '' }} modificador-precio"
                                name="bottom_id"
                                id="bottom_id"
                                required
                            >
                                @foreach(\App\Bottom::get() as $bottom)
                                    <option
                                        {{ (int) old('bottom_id') === $bottom->id || $proforma->bottom_id === $bottom->id ? 'selected' : '' }}
                                        value="{{ $bottom->id }}"
                                        data-code="{{ $bottom->code }}"
                                        data-price="{{ $bottom->price }}"
                                    >{{ $bottom->code." - ".$bottom->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_bottom">Merma fondo (%):</label>
                            <input type="number" class="form-control modificador-precio" name="decrease_bottom" id="decrease_bottom">
                        </div>

                        <div class="form-group">
                            <label for="labeled_id">Rotulado:</label>
                            <select
                                class="form-control {{ $errors->has('labeled_id') ? 'is-invalid': '' }} modificador-precio"
                                name="labeled_id"
                                id="labeled_id"
                                required
                            >
                                @foreach(\App\Labeled::get() as $labeled)
                                    <option
                                        {{ (int) old('labeled_id') === $labeled->id || $proforma->labeled_id === $labeled->id ? 'selected' : '' }}
                                        value="{{ $labeled->id }}"
                                        data-code="{{ $labeled->code }}"
                                        data-price="{{ $labeled->price }}"
                                    >{{ $labeled->code." - ".$labeled->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_labeled">Merma rotulado (%):</label>
                            <input type="number" class="form-control modificador-precio" name="decrease_labeled" id="decrease_labeled">
                        </div>

                        <div class="form-group">
                            <label for="second_labeled_id">Segundo rotulado:</label>
                            <select
                                class="form-control {{ $errors->has('second_labeled_id') ? 'is-invalid': '' }} modificador-precio"
                                name="second_labeled_id"
                                id="second_labeled_id"
                                required
                            >
                                @foreach(\App\Labeled::get() as $labeled)
                                    <option
                                        {{ (int) old('second_labeled_id') === $labeled->id || $proforma->second_labeled_id === $labeled->id ? 'selected' : '' }}
                                        value="{{ $labeled->id }}"
                                        data-code="{{ $labeled->code }}"
                                        data-price="{{ $labeled->price }}"
                                    >{{ $labeled->code." - ".$labeled->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_second_labeled">Merma rotulado (%):</label>
                            <input type="number" class="form-control modificador-precio" name="decrease_second_labeled" id="decrease_second_labeled">
                        </div>

                        <div class="form-group">
                            <label for="over_labeled_id">Sobrelaminado:</label>
                            <select
                                class="form-control {{ $errors->has('over_labeled_id') ? 'is-invalid': '' }} modificador-precio"
                                name="over_labeled_id"
                                id="over_labeled_id"
                                required
                            >
                                @foreach(\App\OverLabeled::get() as $overLabeled)
                                    <option
                                        {{ (int) old('over_labeled_id') === $overLabeled->id || $proforma->over_labeled_id === $overLabeled->id ? 'selected' : '' }}
                                        value="{{ $overLabeled->id }}"
                                        data-code="{{ $overLabeled->code }}"
                                        data-price="{{ $overLabeled->price }}"
                                    >{{ $overLabeled->code." - ".$overLabeled->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_over_labeled">Merma sobrelaminado(%):</label>
                            <input type="number" class="form-control modificador-precio" name="decrease_over_labeled" id="decrease_over_labeled">
                        </div>
                        <div class="form-group">
                            <label for="second_over_labeled_id">Segundo sobrelaminado:</label>
                            <select
                                class="form-control {{ $errors->has('second_over_labeled_id') ? 'is-invalid': '' }} modificador-precio"
                                name="second_over_labeled_id"
                                id="second_over_labeled_id"
                                required
                            >
                                @foreach(\App\OverLabeled::get() as $secOverLabeled)
                                    <option
                                        {{ (int) old('second_over_labeled_id') === $secOverLabeled->id || $proforma->second_over_labeled_id === $secOverLabeled->id ? 'selected' : '' }}
                                        value="{{ $secOverLabeled->id }}"
                                        data-code="{{ $secOverLabeled->code }}"
                                        data-price="{{ $secOverLabeled->price }}"
                                    >{{ $secOverLabeled->code." - ".$secOverLabeled->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" style="display:none">
                            <label for="decrease_second_over_labeled">Merma segundo sobrelaminado(%):</label>
                            <input type="number" class="form-control modificador-precio" name="decrease_second_over_labeled" id="decrease_second_over_labeled">
                        </div>

                        <div class="form-group">
                            <label for="additional_id">Adicional:</label>
                            <select
                                class="form-control {{ $errors->has('additional_id') ? 'is-invalid': '' }} modificador-precio"
                                name="additional_id"
                                id="additional_id"
                                required
                            >
                                @foreach(\App\Additional::get() as $additional)
                                    <option
                                        {{ (int) old('additional_id') === $additional->id || $proforma->additional_id === $additional->id ? 'selected' : '' }}
                                        value="{{ $additional->id }}"
                                        data-code="{{ $additional->code }}"
                                        data-price="{{ $additional->price }}"
                                    >{{ $additional->code." - ".$additional->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="unitary_amount">Monto unitario:</label>
                            <input type="number" readonly class="form-control" name="unitary_amount" id="unitary_amount" data-code="">
                        </div>

                        <div class="form-group">
                            <button
                                type="submit"
                                class="btn btn-outline-info company-add-btn">
                                Agregar
                            </button>
                        </div>

                    </div>
                </div>

            </div>

        </form>
    </div>
</div>
