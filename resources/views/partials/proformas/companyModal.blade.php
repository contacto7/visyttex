<!-- The Modal -->
<div class="modal" id="modalCompany">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Empresa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <label for="company-search">RUC</label>
                    <input
                        type="text"
                        class="form-control company-search"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <label for="company-search-name">Razón Social</label>
                    <input
                        type="text"
                        class="form-control company-search-name"
                        placeholder=""
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-search-btn" data-type="2">
                        Buscar
                    </a>
                </div>
                <div class="form-group">
                    <label for="company-filtered">Empresa</label>
                    <input
                        type="text"
                        class="form-control company-filtered"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">ID empresa</label>
                    <input
                        type="text"
                        class="form-control id-filtered"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">Nombre contacto</label>
                    <input
                        type="text"
                        class="form-control contact-name"
                        readonly
                    >
                </div>
                <div class="form-group" style="display: none">
                    <label for="id-filtered">Correo contacto</label>
                    <input
                        type="text"
                        class="form-control contact-email"
                        readonly
                    >
                </div>
                <div class="form-group">
                    <a class="btn btn-outline-info company-add-btn" data-dismiss="modal">
                        Agregar
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
