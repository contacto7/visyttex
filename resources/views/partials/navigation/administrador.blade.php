<li><a class="nav-link" href="{{ route('orders.list') }}">{{ __("Ordenes")  }}</a></li>
<li><a class="nav-link" href="{{ route('proformas.list') }}">{{ __("Proformas")  }}</a></li>
<li><a class="nav-link" href="{{ route('customers.list') }}">{{ __("Clientes")  }}</a></li>
<li class="nav-item dropdown">
    <a
        class="nav-link dropdown-toggle"
        href="#"
        id="navbarDropdownMenuLink"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
    >
        {{ __("Modificar") }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="{{ route('works.list') }}">{{ __("Trabajos") }}</a>
        <a class="dropdown-item" href="{{ route('mods.list') }}">{{ __("MOD") }}</a>
        <a class="dropdown-item" href="{{ route('margins.list') }}">{{ __("Márgenes")  }}</a>
        <a class="dropdown-item" href="{{ route('users.list') }}">{{ __("Usuarios") }}</a>
        <hr>
        <a class="dropdown-item" href="{{ route('bases.list') }}">{{ __("Bases") }}</a>
        <a class="dropdown-item" href="{{ route('bottoms.list') }}">{{ __("Fondos") }}</a>
        <a class="dropdown-item" href="{{ route('labeleds.list') }}">{{ __("Rotulados") }}</a>
        <a class="dropdown-item" href="{{ route('overLabeleds.list') }}">{{ __("Sobrelaminados") }}</a>
        <a class="dropdown-item" href="{{ route('additionals.list') }}">{{ __("Adicionales") }}</a>
    </div>
</li>

@include('partials.navigation.logged')
