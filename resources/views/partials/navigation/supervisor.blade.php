<li><a class="nav-link" href="{{ route('orders.list') }}">{{ __("Ordenes")  }}</a></li>
<li><a class="nav-link" href="{{ route('proformas.list') }}">{{ __("Proformas")  }}</a></li>
<li><a class="nav-link" href="{{ route('customers.list') }}">{{ __("Clientes")  }}</a></li>
@include('partials.navigation.logged')
