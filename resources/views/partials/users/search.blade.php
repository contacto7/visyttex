<form action="{{ route('users.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="name"
            class="col-sm-3 col-form-label"
        >
            Nombres
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="name"
                id="name"
                type="text"
                placeholder="{{ __("Ingrese el nombre") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="last_name"
            class="col-sm-3 col-form-label"
        >
            Apellidos
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="last_name"
                id="last_name"
                type="text"
                placeholder="{{ __("Ingrese el apellido") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-3 btn-group">
            <input
                class="form-control btn btn-visyttex"
                name="filter"
                type="submit"
                value="buscar"
            >
            <a
                class="form-control btn btn-visyttex"
                href="{{ route('users.create') }}"
            >
                Añadir
            </a>
        </div>
    </div>
</form>
