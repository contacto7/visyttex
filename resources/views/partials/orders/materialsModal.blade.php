@php
    use App\Http\Controllers\OrderController;

    $objeto_bases = array();
    $objeto_fondos = array();
    $objeto_rotulados = array();
    $objeto_sobrelaminados = array();
    $objeto_adicionales = array();

    //CALCULOS PARA MOSTRAR EL POPOVER
    $calculo_mod_tooltip = " ";
    $calculo_maquina_tooltip = " ";
    $objeto_calculo_bases = array();
    $objeto_calculo_fondos = array();
    $objeto_calculo_rotulados = array();
    $objeto_calculo_sobrelaminados = array();

    $work_cost = 0;
    $mod_cost = 0;
@endphp
@forelse($quotations as $quotation)
    @php
    //OBTENEMOS LOS DATOS DE MODELO
    $alto = $quotation->height;
    $ancho = $quotation->width;
    $area = $quotation->width*$quotation->height;
    $cantidad = $quotation->quantity;

    $base_codigo = $quotation->base->code;
    $base_codigo = $quotation->base->code;
    $fondo_codigo = $quotation->bottom->code;
    $rotulado_codigo = $quotation->labeled->code;
    $segundo_rotulado_codigo = $quotation->secondLabeled->code;
    $sobrelaminado_codigo = $quotation->overLabeled->code;
    $segundo_sobrelaminado_codigo = $quotation->secondOverLabeled->code;

    //$adicional_codigo = $quotation->additional->code;

    $merma_base = $quotation->decrease_base;
    $merma_fondo = $quotation->decrease_bottom;
    $merma_rotulado = $quotation->decrease_labeled;
    $merma_segundo_rotulado = $quotation->decrease_second_labeled;
    $merma_sobrelaminado = $quotation->decrease_over_labeled;
    $merma_segundo_sobrelaminado = $quotation->decrease_second_over_labeled;

    //SUMAMOS LOS VALORES EN LOS ARREGLOS CON SU CODIGO COMO INDICE Y GUARDAMOS EL CALCULO EN OTRO ARREGLO
    OrderController::addToObject($objeto_bases,$objeto_calculo_bases,$base_codigo, $area, $merma_base, $cantidad);
    OrderController::addToObject($objeto_fondos,$objeto_calculo_fondos,$fondo_codigo, $area, $merma_fondo, $cantidad);
    OrderController::addToObject($objeto_rotulados,$objeto_calculo_rotulados,$rotulado_codigo, $area, $merma_rotulado, $cantidad);
    //Segundo rotulado
    OrderController::addToObject($objeto_rotulados,$objeto_calculo_rotulados,$segundo_rotulado_codigo, $area, $merma_segundo_rotulado, $cantidad);
    OrderController::addToObject($objeto_sobrelaminados,$objeto_calculo_sobrelaminados,$sobrelaminado_codigo, $area, $merma_sobrelaminado, $cantidad);
    //Segundo sobrelaminado
    OrderController::addToObject($objeto_sobrelaminados,$objeto_calculo_sobrelaminados,$segundo_sobrelaminado_codigo, $area, $merma_segundo_sobrelaminado, $cantidad);

    //AUMENTAMOS EL COSTO DEL MOD Y TRABAJO
    $costo_maquina = $quotation->work->machine_price;
    $costo_mod = $quotation->mod->amount;


    $work_partial_cost = $costo_maquina*$area*$cantidad;
    $mod_partial_cost = $costo_mod*$area*$cantidad;

    $work_cost += $work_partial_cost;
    $mod_cost += $mod_partial_cost;

    //AUMENTAMOS EL TEXTO DE LOS MODS
    $calculo_maquina_tooltip .= " $costo_maquina*$area*$cantidad +";
    $calculo_mod_tooltip .= " $costo_mod*$area*$cantidad +";

    @endphp
@empty

@endforelse
@php
    //POPOVERS: QUITAMOS EL ÚLTIMO MAS DEL CALCULO Y AÑADIMOS EL RESULTADO
    $calculo_maquina_tooltip = rtrim($calculo_maquina_tooltip, "+ ");
    $calculo_mod_tooltip = rtrim($calculo_mod_tooltip, "+ ");
    $calculo_maquina_tooltip .= " = $work_cost";
    $calculo_mod_tooltip .= " = $mod_cost";
@endphp
<div style="margin-bottom: 15px; font-weight: bold"> {{ $proforma->code }}</div>
<div style="margin-bottom: 15px;"><b>Costo Trabajo:</b>  ${{ number_format($work_cost, 2, '.', "'") }}
    <a
        class="btn-outline-info"
        tabindex="0"
        role="button"
        data-container="body"
        data-toggle="popover"
        data-trigger="focus"
        data-placement="top"
        title="Cálculo de costo máquina = <br>&#8512; costo_maquina_señal* area_señal* cantidad_señal"
        data-content="{{ $calculo_maquina_tooltip }}">

        <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
    </a>
</div>
<div style="margin-bottom: 15px;">
    <b>Costo MOD:</b>  ${{ number_format($mod_cost, 2, '.', "'") }}
    <a
            class="btn-outline-info"
            tabindex="0"
            role="button"
            data-container="body"
            data-toggle="popover"
            data-trigger="focus"
            data-placement="top"
            title="Cálculo de MOD = <br>&#8512; mod_señal* area_señal* qty_señal"
            data-content="{{ $calculo_mod_tooltip }}">

        <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
    </a>
</div>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th scope="col">Tipo</th>
        <th scope="col">Código</th>
        <th scope="col">Area total (m2)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($objeto_bases as $codigo => $objeto)
        <tr>
            <td>Base</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}
                <a
                    class="btn-outline-info"
                    tabindex="0"
                    role="button"
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="top"
                    title="Área de bases = <br>&#8512; area_señal* (1+0.1*merma_señal)* qty_señal"
                    data-content="{{ $objeto_calculo_bases[$codigo]." = ".$objeto }}">

                    <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
                </a>
            </td>
        </tr>
    @endforeach
    @foreach($objeto_fondos as $codigo => $objeto)
        <tr>
            <td>Fondo</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}
                <a
                    class="btn-outline-info"
                    tabindex="0"
                    role="button"
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="top"
                    title="Área de fondos = <br>&#8512; area_señal* (1+0.1*merma_señal)* qty_señal"
                    data-content="{{ $objeto_calculo_fondos[$codigo]." = ".$objeto }}">

                    <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
                </a>
            </td>
        </tr>
    @endforeach
    @foreach($objeto_rotulados as $codigo => $objeto)
        <tr>
            <td>Rotulado</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}
                <a
                    class="btn-outline-info"
                    tabindex="0"
                    role="button"
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="top"
                    title="Área de rotulados = <br>&#8512; area_señal* (1+0.1*merma_señal)* qty_señal"
                    data-content="{{ $objeto_calculo_rotulados[$codigo]." = ".$objeto }}">

                    <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
                </a>
            </td>
        </tr>
    @endforeach
    @foreach($objeto_sobrelaminados as $codigo => $objeto)
        <tr>
            <td>Sobrelaminado</td>
            <td>{{ $codigo }}</td>
            <td>{{ number_format($objeto, 2, '.', "'") }}
                <a
                    class="btn-outline-info"
                    tabindex="0"
                    role="button"
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="top"
                    title="Área de sobrelaminados = <br>&#8512; area_señal* (1+0.1*merma_señal)* qty_señal"
                    data-content="{{ $objeto_calculo_sobrelaminados[$codigo]." = ".$objeto }}">

                    <i class="fa fa-info-circle" style="color: #3f9ae5"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            trigger: 'focus',
            html: true
        });
    });
</script>
