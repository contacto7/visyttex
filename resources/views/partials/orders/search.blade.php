<form action="{{ route('orders.search') }}" method="get" class="col-sm-12 form-search-villamares">
    <div class="form-group row">
        <label
            for="company_name"
            class="col-sm-3 col-form-label"
        >
            Razón social
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="company_name"
                id="company_name"
                type="text"
                placeholder="{{ __("Ingrese la razón social") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="tax_number"
            class="col-sm-3 col-form-label"
        >
            RUC
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="tax_number"
                id="tax_number"
                type="text"
                placeholder="{{ __("Ingrese el RUC") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <label
            for="code"
            class="col-sm-3 col-form-label"
        >
            Código de proforma
        </label>
        <div class="col-sm-9">
            <input
                class="form-control"
                name="code"
                id="code"
                type="text"
                placeholder="{{ __("Ingrese el código") }}"
            >
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-3 offset-sm-3 btn-group">
            <input
                class="form-control btn btn-visyttex"
                name="filter"
                type="submit"
                value="buscar"
            >
        </div>
    </div>
</form>
