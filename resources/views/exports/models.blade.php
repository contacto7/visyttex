<table>
    <thead>
    <tr>
        <th>Material</th>
        <th>Abreviatura</th>
        <th>Código</th>
        <th>Precio</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{{ $model->name }}</td>
            <td>{{ $model->code }}</td>
            <td>{{ $model->code_visyttex }}</td>
            <td>{{ $model->price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
