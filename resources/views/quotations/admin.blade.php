@extends('layouts.app')

@section('title')
    @include('partials.title', [
        'title' => __("SEÑAL"),
        'icon' => "file-text-o"
    ])
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <div class="row mt-3 mb-3">
            <table class="table table-striped table-light">
                <tbody>
                    <tr>
                        <th>SEÑAL</th>
                        <td>{{ $quotation->signal->name }}</td>
                    </tr>
                    <tr>
                        <th>TIPO</th>
                        <td>{{ $quotation->signal->signalType->name }}</td>
                    </tr>
                    <tr>
                        <th>CÓDIGO</th>
                        <td>{{ $quotation->proforma_code }}</td>
                    </tr>
                    <tr>
                        <th>DESCRIPCIÓN</th>
                        <td>{{ $quotation->description }}</td>
                    </tr>
                    <tr>
                        <th>ALTO</th>
                        <td>{{ $quotation->height }}</td>
                    </tr>
                    <tr>
                        <th>ANCHO</th>
                        <td>{{ $quotation->width }}</td>
                    </tr>
                    <tr>
                        <th>CANTIDAD</th>
                        <td>{{ $quotation->quantity }}</td>
                    </tr>
                    <tr>
                        <th>MONTO UNITARIO</th>
                        <td>{{ $quotation->currency->symbol }} {{ $quotation->unitary_amount }}</td>
                    </tr>
                    <tr>
                        <th>MONTO TOTAL</th>
                        <td>{{ $quotation->currency->symbol }}{{ $quotation->unitary_amount*$quotation->quantity }}</td>
                    </tr>
                    <tr>
                        <th>MARGEN</th>
                        <td>{{ $quotation->margin->percentage }}%</td>
                    </tr>
                    <tr>
                        <th>TRABAJO - PRECIO</th>
                        <td>{{ $quotation->work->name }} - ${{ $quotation->work->machine_price }}</td>
                    </tr>
                    <tr>
                        <th>BASE - PRECIO</th>
                        <td>{{ $quotation->base->name }} - ${{ $quotation->base->price }}</td>
                    </tr>
                    <tr>
                        <th>FONDO - PRECIO</th>
                        <td>{{ $quotation->bottom->name }} - ${{ $quotation->bottom->price }}</td>
                    </tr>
                    <tr>
                        <th>ROTULADO - PRECIO</th>
                        <td>{{ $quotation->labeled->name }} - ${{ $quotation->labeled->price }}</td>
                    </tr>
                    <tr>
                        <th>SEGUNDO ROTULADO - PRECIO</th>
                        <td>
                            @if($quotation->secondLabeled)
                            {{ $quotation->secondLabeled->name }}
                            -
                            ${{ $quotation->secondLabeled->price }}</td>
                            @else
                                NADA - $0
                            @endif
                    </tr>
                    <tr>
                        <th>SOBRELAMINADO - PRECIO</th>
                        <td>{{ $quotation->overLabeled->name }} - ${{ $quotation->overLabeled->price }}</td>
                    </tr>
                    <tr>
                        <th>SEGUNDO SOBRELAMINADO - PRECIO</th>
                        <td>{{ $quotation->secondOverLabeled->name }} - ${{ $quotation->secondOverLabeled->price }}</td>
                    </tr>
                    <tr>
                        <th>ADICIONAL - PRECIO</th>
                        <td>{{ $quotation->additional->name }} - ${{ $quotation->additional->price }}</td>
                    </tr>
                    <tr>
                        <th>EMBALAJES E INSUMOS</th>
                        <td>{{ $quotation->packaging_supplies }}</td>
                    </tr>
                    <tr>
                        <th>MERMA BASE</th>
                        <td>{{ $quotation->decrease_base }}%</td>
                    </tr>
                    <tr>
                        <th>MERMA FONDO</th>
                        <td>{{ $quotation->decrease_bottom }}%</td>
                    </tr>
                    <tr>
                        <th>MERMA ROTULADO</th>
                        <td>{{ $quotation->decrease_labeled }}%</td>
                    </tr>
                    <tr>
                        <th>MERMA SEGUNDO ROTULADO</th>
                        <td>{{ $quotation->decrease_second_labeled }}%</td>
                    </tr>
                    <tr>
                        <th>MERMA SOBRELAMINADO</th>
                        <td>{{ $quotation->decrease_over_labeled }}%</td>
                    </tr>
                    <tr>
                        <th>MERMA SEGUNDO SOBRELAMINADO</th>
                        <td>{{ $quotation->decrease_second_over_labeled }}%</td>
                    </tr>
                    <tr>
                        <th>MOD</th>
                        <td>
                            @if($quotation->mod->type == \App\Mod::BASE)
                                Base
                            @else
                                Con sticker
                            @endif
                            - ${{ $quotation->mod->amount }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
