<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('signal_id');
            $table->foreign('signal_id')->references('id')->on('signals');
            $table->string('proforma_code');
            $table->text('description')->nullable();
            $table->float('height');
            $table->float('width');
            $table->unsignedBigInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->integer('quantity');
            $table->float('unitary_amount');
            $table->unsignedBigInteger('margin_id');
            $table->foreign('margin_id')->references('id')->on('margins');
            $table->unsignedBigInteger('work_id');
            $table->foreign('work_id')->references('id')->on('works');
            $table->unsignedBigInteger('base_id');
            $table->foreign('base_id')->references('id')->on('bases');
            $table->unsignedBigInteger('bottom_id');
            $table->foreign('bottom_id')->references('id')->on('bottoms');
            $table->unsignedBigInteger('labeled_id')->nullable();
            $table->foreign('labeled_id')->references('id')->on('labeleds');
            $table->unsignedBigInteger('second_labeled_id')->nullable();
            $table->foreign('second_labeled_id')->references('id')->on('labeleds');
            $table->unsignedBigInteger('over_labeled_id')->nullable();
            $table->foreign('over_labeled_id')->references('id')->on('over_labeleds');
            $table->unsignedBigInteger('second_over_labeled_id')->nullable();
            $table->foreign('second_over_labeled_id')->references('id')->on('over_labeleds');
            $table->unsignedBigInteger('additional_id')->nullable();
            $table->foreign('additional_id')->references('id')->on('additionals');
            $table->unsignedBigInteger('mod_id');
            $table->foreign('mod_id')->references('id')->on('mods');
            $table->unsignedBigInteger('proforma_id');
            $table->foreign('proforma_id')->references('id')->on('proformas');
            $table->float('packaging_supplies');
            $table->unsignedInteger('decrease_base');
            $table->unsignedInteger('decrease_bottom');
            $table->unsignedInteger('decrease_labeled')->nullable();
            $table->unsignedInteger('decrease_second_labeled')->nullable();
            $table->unsignedInteger('decrease_over_labeled')->nullable();
            $table->unsignedInteger('decrease_second_over_labeled')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
