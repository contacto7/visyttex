<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExternalMargin;
use Faker\Generator as Faker;

$factory->define(ExternalMargin::class, function (Faker $faker) {
    return [
        'percentage' => $faker->numberBetween(35,37),
    ];
});
