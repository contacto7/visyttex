<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bottom;
use Faker\Generator as Faker;

$factory->define(Bottom::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => 10,
        'code' => $faker->numerify('##'),
        'code_visyttex' => $faker->bothify('##??##'),
    ];
});
