<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Labeled;
use Faker\Generator as Faker;

$factory->define(Labeled::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'text' => $faker->name,
        'price' => 10,
        'code' => $faker->numerify('##'),
        'code_visyttex' => $faker->bothify('##??##'),
    ];
});
