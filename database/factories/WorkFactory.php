<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Work;
use Faker\Generator as Faker;

$factory->define(Work::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'machine_price' => $faker->numberBetween(8,15),
        'code' => $faker->numerify('##'),
    ];
});
