<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Margin;
use Faker\Generator as Faker;

$factory->define(Margin::class, function (Faker $faker) {
    return [
        'percentage' => $faker->numberBetween(35,37),
        'type' => $faker->numberBetween(1,2),
    ];
});
