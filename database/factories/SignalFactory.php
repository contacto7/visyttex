<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Signal;
use Faker\Generator as Faker;

$factory->define(Signal::class, function (Faker $faker) {
    return [
        'signal_type_id'=>\App\SignalType::all()->random()->id,
        'name' => $faker->name,
        'description' => $faker->sentence,
        'picture'=> null,
        'code' => $faker->bothify('??##'),

    ];
});
