<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'proforma_id'=>\App\Proforma::all()->random()->id,
        'order_date' => $faker->dateTimeBetween('-30 days', '+30 days'),
        'delivered_date' => $faker->dateTimeBetween('+35 days', '+65 days'),
        'additional_details'=> $faker->sentence,
    ];
});
