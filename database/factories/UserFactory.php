<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->name;
    $last_name =  $faker->lastName;
    $fake_id = $faker->numberBetween(100000,999999);

    return [
        'role_id'=>\App\Role::all()->random()->id,
        'name' => $name,
        'last_name' => $last_name,
        'slug' => Str::slug($name." ".$last_name." ".$fake_id,'-'),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'phone'=> $faker->phoneNumber,
        'cellphone'=> $faker->phoneNumber,
        'position'=> $faker->word,
        'state'=> $faker->numberBetween(1,2),
        'remember_token' => Str::random(10),
    ];
});
