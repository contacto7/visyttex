<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OverLabeled;
use Faker\Generator as Faker;

$factory->define(OverLabeled::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => 10,
        'code' => $faker->numerify('##'),
        'code_visyttex' => $faker->bothify('##??##'),
    ];
});
