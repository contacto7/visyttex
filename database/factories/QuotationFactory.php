<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Quotation;
use Faker\Generator as Faker;

$factory->define(Quotation::class, function (Faker $faker) {
    return [
        'signal_id'=>\App\Signal::all()->random()->id,
        'proforma_code' => $faker->bothify('??######'),
        'description' => $faker->sentence,
        'height' => $faker->randomFloat(1,5,120),
        'width' => $faker->randomFloat(1,5,120),
        'currency_id'=>\App\Currency::all()->random()->id,
        'quantity' => $faker->numberBetween(1,1000),
        'unitary_amount' => $faker->randomFloat(1,12,30),
        'margin_id'=>\App\Margin::all()->random()->id,
        'work_id'=>\App\Work::all()->random()->id,
        'base_id'=>\App\Base::all()->random()->id,
        'bottom_id'=>\App\Bottom::all()->random()->id,
        'labeled_id'=>\App\Labeled::all()->random()->id,
        'second_labeled_id'=>\App\Labeled::all()->random()->id,
        'over_labeled_id'=>\App\OverLabeled::all()->random()->id,
        'second_over_labeled_id'=>\App\OverLabeled::all()->random()->id,
        'additional_id'=>\App\Additional::all()->random()->id,
        'mod_id'=>\App\Mod::all()->random()->id,
        'proforma_id'=>\App\Proforma::all()->random()->id,
        'packaging_supplies' => $faker->randomFloat(12,0.10,0.15),
        'decrease_base' => $faker->numberBetween(15,25),
        'decrease_bottom' => $faker->numberBetween(15,25),
        'decrease_labeled' => $faker->numberBetween(15,25),
        'decrease_second_labeled' => $faker->numberBetween(15,25),
        'decrease_over_labeled' => $faker->numberBetween(15,25),
        'decrease_second_over_labeled' => $faker->numberBetween(15,25),

    ];
});
