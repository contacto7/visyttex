<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Measure;
use Faker\Generator as Faker;

$factory->define(Measure::class, function (Faker $faker) {
    return [
        'height' => $faker->numberBetween(7,120),
        'width' => $faker->numberBetween(7,120),
        'code' => $faker->bothify('?##'),
    ];
});
