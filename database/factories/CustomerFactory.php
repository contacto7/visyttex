<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Customer::class, function (Faker $faker) {
    $name = $faker->name;
    $last_name =  $faker->lastName;

    return [
        'company_name' => $faker->company,
        'tax_number' => $faker->phoneNumber,
        'address' => $faker->address,
        'phone'=> $faker->phoneNumber,
        'user_id'=>\App\User::all()->random()->id,
        'contact_name' => $name,
        'contact_last_name' => $last_name,
        'contact_cellphone'=> $faker->phoneNumber,
        'contact_email' => $faker->unique()->safeEmail,
    ];
});
