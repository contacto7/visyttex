<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SignalType;
use Faker\Generator as Faker;

$factory->define(SignalType::class, function (Faker $faker) {
    return [
        'name'=> $faker->word,
        'description'=> $faker->sentence ,
        'picture'=> $faker->image(storage_path() . '/app/public/signal_types',200,200, 'transport', false),
    ];
});
