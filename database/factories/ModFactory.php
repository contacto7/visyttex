<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mod;
use Faker\Generator as Faker;

$factory->define(Mod::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'amount' => $faker->randomElement([2, 2.50]),
        'type'=> $faker->numberBetween(1,2),
        'work_id'=>\App\Work::all()->random()->id,
    ];
});
