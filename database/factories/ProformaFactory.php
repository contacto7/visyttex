<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Proforma;
use Faker\Generator as Faker;

$factory->define(Proforma::class, function (Faker $faker) {
    return [
        'code' => $faker->bothify('PRESUPUESTO N° #### - 2019'),
        'date' => $faker->dateTimeBetween('-30 days', '+30 days'),
        'proforma_name' => $faker->name." ".$faker->lastName,
        'proforma_mail' => $faker->email,
        'delivery_time' => $faker->numberBetween(5,45),
        'validity_time' => $faker->numberBetween(5,45),
        'additional_details' => $faker->sentence,
        'subtotal' => $faker->randomFloat(2,14,1500),
        'customer_id'=>\App\Customer::all()->random()->id,
        'user_id'=>\App\User::all()->random()->id,
    ];
});
