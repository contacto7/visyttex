<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        /*
        Storage::deleteDirectory('signals');
        Storage::deleteDirectory('signal_types');
        */

        if(!File::exists('signals')){
            Storage::makeDirectory('signals');
        }
        if(!File::exists('signal_types')){
            Storage::makeDirectory('signal_types');
        }

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'name' => 'administrador'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'trabajador'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'supervisor'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'vendedor externo'
        ]);

        //////////USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Trabajador',
            'email' => 'trabajador@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::WORKER,
            'state' => 1
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Supervisor',
            'email' => 'supervisor@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::SUPERVISOR,
            'state' => 1
        ]);

        //////////CUSTOMER
        factory(\App\Customer::class, 10)->create();

        //////////MARGIN
        factory(\App\Margin::class, 1)->create([
            'percentage' => 35,
            'type' => 1,
        ]);
        factory(\App\Margin::class, 1)->create([
            'percentage' => 40,
            'type' => 2,
        ]);

        //////////WORKS
        /*
        PREGUNTAR POR EL COSTO DE MÁQUINA DE CADA
        UNO DE LOS TRABAJOS
        */
        factory(\App\Work::class, 1)->create([
            'name' => 'Nada',
            'machine_price' => '00',
            'code' => '00',
        ]);
        factory(\App\Work::class, 1)->create([
            'name' => 'Impreso',
            'machine_price' => '10',
            'code' => '10',
        ]);
        factory(\App\Work::class, 1)->create([
            'name' => 'Rotulado',
            'machine_price' => '12',
            'code' => '20',
        ]);
        factory(\App\Work::class, 1)->create([
            'name' => 'Serigrafía',
            'machine_price' => '8',
            'code' => '30',
        ]);
        factory(\App\Work::class, 1)->create([
            'name' => 'Serigrafía-Rotulado',
            'machine_price' => '14',
            'code' => '40',
        ]);

        //////////BASE
        factory(\App\Base::class, 1)->create([
            'name' => 'NADA',
            'price' => 0.00,
            'code' => '00',
            'code_visyttex' => 'NN',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'ACRILICO TRANSPARENTE  DE 2 mm  M2',
            'price' => 10.00,
            'code' => 'A2',
            'code_visyttex' => 'ACR 2MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'ACRILICO TRANSPARENTE DE 3mm  M2',
            'price' => 10.00,
            'code' => 'A3',
            'code_visyttex' => 'ACR 3MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'ACRILICO TRANSPARENTE DE 5mm M2',
            'price' => 10.00,
            'code' => 'A5',
            'code_visyttex' => 'ACR 5MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO  DE 2.5 mm  M2',
            'price' => 10.00,
            'code' => 'F1',
            'code_visyttex' => 'FIB 2.5MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO DE 2.5 mm CON REFUERZO M2',
            'price' => 10.00,
            'code' => 'F2',
            'code_visyttex' => 'FIB 2.5MM CON REF',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO DE 3 mm  M2',
            'price' => 10.00,
            'code' => 'F3',
            'code_visyttex' => 'FIB 3MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO DE 3 mm CON REFUERZO M2',
            'price' => 33.9,
            'code' => 'F4',
            'code_visyttex' => 'FIB 3MM CON REF',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO DE 4mm CON REFUERZO M2 ',
            'price' => 33.9,
            'code' => 'F5',
            'code_visyttex' => 'FIB 4MM CON REF',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'FIBRA DE VIDRIO DE 6 mm CON REFUERZO M2',
            'price' => 10.00,
            'code' => 'F6',
            'code_visyttex' => 'FIB 6MM CON REF',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA GALVANIZADA DE 0.80 MM  M2',
            'price' => 10.00,
            'code' => 'G1',
            'code_visyttex' => 'PLAN GALV 0.8MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA GALVANIZADA DE 1.20 MM M2',
            'price' => 10.00,
            'code' => 'G2',
            'code_visyttex' => 'PLAN GALV 1.2MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA GALVANIZADA DE 1.5 MM  M2',
            'price' => 16.46,
            'code' => 'G3',
            'code_visyttex' => 'PLAN GALV 1.5MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA GALVANIZADA DE 2 MM M2',
            'price' => 22.06,
            'code' => 'G4',
            'code_visyttex' => 'PLAN GALV 2MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA GALVANIZADA  DE 2.9 MM M2',
            'price' => 10.00,
            'code' => 'G5',
            'code_visyttex' => 'PLAN GALV 2.9MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA NEGRA DE  0.4 mm M2',
            'price' => 10.00,
            'code' => 'N1',
            'code_visyttex' => 'PLAN NEGRA 0.4MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA NEGRA DE 1/32  M2',
            'price' => 10.00,
            'code' => 'N2',
            'code_visyttex' => 'PLAN NEGRA 1/32',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PLANCHA NEGRA DE 1.20 MM',
            'price' => 7.55,
            'code' => 'N3',
            'code_visyttex' => 'PLAN NEGRA 1.2MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'POLIESTIRENO BLANCO DE  0.80 MM  M2',
            'price' => 10.00,
            'code' => 'P1',
            'code_visyttex' => 'PLIEST BLANCO 0.8MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'POLIESTIRENO BLANCO DE 1.00 MM  M2',
            'price' => 10.00,
            'code' => 'P2',
            'code_visyttex' => 'PLIEST BLANCO 1MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'POLIESTIRENO BLANCO DE  1.5 mm M2',
            'price' => 10.00,
            'code' => 'P3',
            'code_visyttex' => 'PLIEST BLANCO 1.5MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO ALUMINIO DE 3mm  1.22 x 2.44 mts  3M M2',
            'price' => 10.00,
            'code' => 'S1',
            'code_visyttex' => 'SUST 3MM 3M',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO DE ALUMINIO DE 3MM 1.22 x 2.44 mts ENNE M2',
            'price' => 11,
            'code' => 'S2',
            'code_visyttex' => 'SUST 3MM ENNE',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO ALUMINIO DE 3mm ALT M2',
            'price' => 15,
            'code' => 'S3',
            'code_visyttex' => 'SUST 3MM ALT',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO DE ALUMINIO DE 4MM  1.22 x 2.44 mts  ENNE M2',
            'price' => 12,
            'code' => 'S4',
            'code_visyttex' => 'SUST 4MM ENNE',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO ALUMINIO DE 4mm 1.50 x 5 MTS CORRALES M2',
            'price' => 17,
            'code' => 'S5',
            'code_visyttex' => 'SUST 4MM CORR',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'SUSTRATO DE ALUMINIO DE 5MM  1.22 x 2.44 mts  ENNE M2',
            'price' => 14,
            'code' => 'S6',
            'code_visyttex' => 'SUST 5MM ENNE',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'TRIPLAY DE 4 mm M2',
            'price' => 10.00,
            'code' => 'T4',
            'code_visyttex' => 'TRIP 4MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'TRIPLAY DE 6MM  M2',
            'price' => 10.00,
            'code' => 'T6',
            'code_visyttex' => 'TRIP 6MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'TRIPLAY DE 8MM M2',
            'price' => 10.00,
            'code' => 'T8',
            'code_visyttex' => 'TRIP 8MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'TRIPLAY DE 18MM M2',
            'price' => 10.00,
            'code' => 'T9',
            'code_visyttex' => 'TRIP 18MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PVC BLANCO  DE  2MM  1.22 x 2.44 mts M2',
            'price' => 8.5,
            'code' => 'V2',
            'code_visyttex' => 'PVC 2MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PVC BLANCO DE 3MM  1.22 x 2.44 mts   M2',
            'price' => 9.9,
            'code' => 'V3',
            'code_visyttex' => 'PVC 3MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PVC BLANCO 5MM  1.22 x 2.44 mts M2',
            'price' => 17.1,
            'code' => 'V5',
            'code_visyttex' => 'PVC 5MM',
        ]);
        factory(\App\Base::class, 1)->create([
            'name' => 'PVC BLANCO DE  10MM  1.22 x 2.44 mts M2',
            'price' => 10.00,
            'code' => 'V1',
            'code_visyttex' => 'PVC 10MM',
        ]);

        //////////BOTTOM
        factory(\App\Bottom::class, 1)->create([
            'name' => 'NADA',
            'price' => 00,
            'code' => '000',
            'code_visyttex' => 'NN',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. OMNICUBE AMA/LIM T-11513 AVERY 36" X MT2 IMPORTACION',
            'price' => 30.82,
            'code' => 'B01',
            'code_visyttex' => 'OMN AMA/LIM',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. OMNICUBE BLANCO T-11500 AVERY 36" X MT2 IMPORTACION',
            'price' => 24.36,
            'code' => 'B02',
            'code_visyttex' => 'OMN BLANCO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI BLANCO T-500B AVERY 24" X MT2 IMPORTACION',
            'price' => 11.84,
            'code' => 'B03',
            'code_visyttex' => 'GI BLANCO AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI BLANCO M-0500D AVERY 48" X MT2 IMPORTACION',
            'price' => 9.62,
            'code' => 'B04',
            'code_visyttex' => 'GI BLANCO AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI ROJO T-1508-A AVERY 24" X MT2 IMPORTACION',
            'price' => 23.67,
            'code' => 'B05',
            'code_visyttex' => 'GI ROJO AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI VERDE T-1507-A AVERY 36" X MT2 IMPORTACION',
            'price' => 12.33,
            'code' => 'B06',
            'code_visyttex' => 'GI VERDE AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI AMARILLO T-1501-A AVERY 24" X MT2 IMPORTACION',
            'price' => 11.84,
            'code' => 'B07',
            'code_visyttex' => 'GI AMARILLO AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC. GI NARANJA T-1504-A AVERY 48" X MT2 IMPORTACION',
            'price' => 11.84,
            'code' => 'B08',
            'code_visyttex' => 'GI NARANJA  AVERY',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 AZUL 4095 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D00',
            'code_visyttex' => 'DG3 AZUL',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 AMARILLO FLUORESCENTE 4081 3M 36" X MT2',
            'price' => 58.5,
            'code' => 'D01',
            'code_visyttex' => 'DG3 AMA FLUO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 AMARILLO LIMON 4083 3M 36" X MT2',
            'price' => 58.5,
            'code' => 'D02',
            'code_visyttex' => 'DG3 AMA/LIM',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 NARANJA FLUORESCENTE 4084 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D03',
            'code_visyttex' => 'DG3 NARANJA',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 BLANCO 4090 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D04',
            'code_visyttex' => 'DG3 BLANCO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 ROJO 4092 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D05',
            'code_visyttex' => 'DG3 ROJO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 VERDE 4097 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D06',
            'code_visyttex' => 'DG3 VERDE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'LAM. REFLEC TIPO XI DG3 AMARILLO 4091 3M 36" X MT2',
            'price' => 53.7,
            'code' => 'D07',
            'code_visyttex' => 'DG3 AMARILLO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC KOREANO ROJO',
            'price' => 10,
            'code' => 'E01',
            'code_visyttex' => 'EC ROJO KOR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC KOREANO AZUL',
            'price' => 10,
            'code' => 'E02',
            'code_visyttex' => 'EC AZUL KOR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC KOREANO VERDE',
            'price' => 10,
            'code' => 'E04',
            'code_visyttex' => 'EC VERDE KOR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC 3M ROJO',
            'price' => 10,
            'code' => 'E31',
            'code_visyttex' => 'EC ROJO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC 3M AZUL',
            'price' => 10,
            'code' => 'E32',
            'code_visyttex' => 'EC AZUL 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'EC 3M VERDE',
            'price' => 10,
            'code' => 'E34',
            'code_visyttex' => 'EC VERDE 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'FOTOLUMINICENTE 1 HORA M2',
            'price' => 10,
            'code' => 'F01',
            'code_visyttex' => 'FOTOL 1HR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'FOTOLUMINICENTE 3 HORAS M2',
            'price' => 10,
            'code' => 'F03',
            'code_visyttex' => 'FOTOL 3HR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'FOTOLUMINICENTE  5 HORAS M2',
            'price' => 12.5,
            'code' => 'F05',
            'code_visyttex' => 'FOTOL 5HR',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. CHINO  24" X  M2  ',
            'price' => 5,
            'code' => 'I00',
            'code_visyttex' => 'GI BLANCO CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI ROJO LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I01',
            'code_visyttex' => 'GI ROJO CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI AZUL LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I02',
            'code_visyttex' => 'GI AZUL CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI AMARILLO LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I03',
            'code_visyttex' => 'GI AMARILLO CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. CHINO   24" X M2 ',
            'price' => 5,
            'code' => 'I04',
            'code_visyttex' => 'GI VERDE CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  NARANJA LAM. REFLEC. CHINO  24" X M2',
            'price' => 5,
            'code' => 'I05',
            'code_visyttex' => 'GI NARANJA CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI NEGRO LAM. REFLEC. CHINO   24" X M2',
            'price' => 5,
            'code' => 'I06',
            'code_visyttex' => 'GI NEGRO CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI CELESTE LAM. REFLEC. CHINO   24" X M2',
            'price' => 5,
            'code' => 'I07',
            'code_visyttex' => 'GI CELESTE CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. 3M 3290 24" X  M2',
            'price' => 16.5,
            'code' => 'I30',
            'code_visyttex' => 'GI BLANCO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI ROJO LAM. REFLEC. 3M  3272 24" X M2',
            'price' => 16.5,
            'code' => 'I31',
            'code_visyttex' => 'GI ROJO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI AZUL LAM. REFLEC. 3M 3275  48" X M2',
            'price' => 16.5,
            'code' => 'I32',
            'code_visyttex' => 'GI AZUL 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI AMARILLO LAM. REFLEC. 3M 3271 24" X M2',
            'price' => 16.5,
            'code' => 'I33',
            'code_visyttex' => 'GI AMARILLO 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. 3M 3277 48" X M2',
            'price' => 16.5,
            'code' => 'I34',
            'code_visyttex' => 'GI VERDE 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI NARANJA LAM. REFLEC. 3M CW84NL  24" X M2',
            'price' => 16.5,
            'code' => 'I35',
            'code_visyttex' => 'GI NARANJA 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI NEGRO LAM. REFLEC. 3M 580-85  24" X M2',
            'price' => 29.41,
            'code' => 'I36',
            'code_visyttex' => 'GI NEGRO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. 3M 3290 48" X  M2',
            'price' => 16.5,
            'code' => 'I37',
            'code_visyttex' => 'GI BLANCO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. 3M 3277 36" X M2',
            'price' => 16.5,
            'code' => 'I38',
            'code_visyttex' => 'GI VERDE 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  BLANCO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV0',
            'code_visyttex' => 'GI BLANCO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  ROJO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV1',
            'code_visyttex' => 'GI ROJO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  AZUL LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV2',
            'code_visyttex' => 'GI AZUL',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  AMARILLO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV3',
            'code_visyttex' => 'GI AMARILLO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  VERDE LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV4',
            'code_visyttex' => 'GI VERDE ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  NARANJA LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV5',
            'code_visyttex' => 'GI NARANJA',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  NEGRO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV6',
            'code_visyttex' => 'GI NEGRO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GI  CELESTE LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV7',
            'code_visyttex' => 'GI CELESTE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP BLANCO LAM. REFLEC.3M 3930  M2',
            'price' => 31.13,
            'code' => 'J30',
            'code_visyttex' => 'HIP BLANCO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP ROJO LAM. REFLEC.3M  3932 M2',
            'price' => 31.13,
            'code' => 'J31',
            'code_visyttex' => 'HIP ROJO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP AZUL LAM. REFLEC.3M 3935 M2',
            'price' => 31.13,
            'code' => 'J32',
            'code_visyttex' => 'HIP AZUL 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP AMARILLO LAM. REFLEC.3M 3931 M2',
            'price' => 31.13,
            'code' => 'J33',
            'code_visyttex' => 'HIP AMARILLO 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP VERDE LAM. REFLEC.3M 3937M2',
            'price' => 31.13,
            'code' => 'J34',
            'code_visyttex' => 'HIP VERDE 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP NARANJA LAM. REFLEC.3M 3934M2',
            'price' => 31.13,
            'code' => 'J35',
            'code_visyttex' => 'HIP NARANJA 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP BLANCO LAM. REFLEC.ENNE RS - 3000 48" X M2',
            'price' => 20.56,
            'code' => 'JM0',
            'code_visyttex' => 'HIP BLANCO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP ROJO LAM. REFLEC.ENNE RS-3005   48" X M2',
            'price' => 20.56,
            'code' => 'JM1',
            'code_visyttex' => 'HIP ROJO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP AZUL LAM. REFLEC.ENNE  RS-3003  48" X M2',
            'price' => 20.56,
            'code' => 'JM2',
            'code_visyttex' => 'HIP AZUL ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP AMARILLO LAM. REFLEC.ENNE RS-3001 48" X M2',
            'price' => 20.56,
            'code' => 'JM3',
            'code_visyttex' => 'HIP AMARILLO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP VERDE LAM. REFLEC.ENNE. RS-3004 48" X M2',
            'price' => 20.56,
            'code' => 'JM4',
            'code_visyttex' => 'HIP VERDE ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP NARANJA LAM. REFLEC.ENNE. RS-3002  48" X M2',
            'price' => 20.56,
            'code' => 'JM5',
            'code_visyttex' => 'HIP NARANJA ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP BLANCO FLEXIBLE LAM. REFLEC ENNE RF-3000 48"XM2',
            'price' => 17.54,
            'code' => 'JM6',
            'code_visyttex' => 'HIP FLEX BLANCO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP NARANJA FLUORECENTE LAM. REFLEC.ENNE.  RS-3013 48" X M2',
            'price' => 28.05,
            'code' => 'JM7',
            'code_visyttex' => 'HIP NARANJA FLUO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP AMARILLO/LIMON LAM. REFLEC. ENNE RS-3012 48" X M2',
            'price' => 28.05,
            'code' => 'JM8',
            'code_visyttex' => 'HIP AMA/LIM ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'HIP  VERDE FLUORESCENTE LAM. REFLEC. ENNE   M2',
            'price' => 10,
            'code' => 'JM9',
            'code_visyttex' => 'HIP VERDE FLUO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P00',
            'code_visyttex' => 'GIP BLANCO CHINO ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P01',
            'code_visyttex' => 'GIP ROJO CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P02',
            'code_visyttex' => 'GIP AZUL CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P03',
            'code_visyttex' => 'GIP AMARILLO CHINO ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P04',
            'code_visyttex' => 'GIP VERDE CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P05',
            'code_visyttex' => 'GIP NARANJA CHINO ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P06',
            'code_visyttex' => 'GIP NEGRO CHINO ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P07',
            'code_visyttex' => 'GIP CELESTE CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC.3M  3430  M2',
            'price' => 10,
            'code' => 'P30',
            'code_visyttex' => 'GIP BLANCO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC. 3M 3432 M2',
            'price' => 10,
            'code' => 'P31',
            'code_visyttex' => 'GIP ROJO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. 3M  3435 3M M2',
            'price' => 10,
            'code' => 'P32',
            'code_visyttex' => 'GIP AZUL 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. 3M 3431 M2',
            'price' => 10,
            'code' => 'P33',
            'code_visyttex' => 'GIP AMARILLO 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. 3M 3437 M2',
            'price' => 10,
            'code' => 'P34',
            'code_visyttex' => 'GIP VERDE 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P35',
            'code_visyttex' => 'GIP NARANJA 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P36',
            'code_visyttex' => 'GIP NEGRO 3M ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P37',
            'code_visyttex' => 'GIP CELESTE 3M',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM0',
            'code_visyttex' => 'GIP BLANCO 3NNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC.ENNE M2',
            'price' => 10,
            'code' => 'PM1',
            'code_visyttex' => 'GIP ROJO ENNE ',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM2',
            'code_visyttex' => 'GP AZUL ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM3',
            'code_visyttex' => 'GIP AMARILLO ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM4',
            'code_visyttex' => 'GIP VERDE ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM5',
            'code_visyttex' => 'GIP NARANJA ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP MARRON LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM6',
            'code_visyttex' => 'GIP MARRON ENNE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV0',
            'code_visyttex' => 'GIP BLANCO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV1',
            'code_visyttex' => 'GIP ROJO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV2',
            'code_visyttex' => 'GIP AZUL',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV3',
            'code_visyttex' => 'GIP AMARILLO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV4',
            'code_visyttex' => 'GIP VERDE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV5',
            'code_visyttex' => 'GIP NARANJA',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV6',
            'code_visyttex' => 'GIP NEGRO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV7',
            'code_visyttex' => 'GIP CELESTE',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL PARA ROTULADO CHINO M2',
            'price' => 10,
            'code' => 'R00',
            'code_visyttex' => 'VINIL CHINO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL PARA ROTULADO ARCLARD M2',
            'price' => 10,
            'code' => 'R02',
            'code_visyttex' => 'VINIL ARCLAD',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL ROJO MCALL M2',
            'price' => 3.5,
            'code' => 'RM1',
            'code_visyttex' => 'VINIL ROJO MCALL',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL VERDE MCALL M2',
            'price' => 3.5,
            'code' => 'RM2',
            'code_visyttex' => 'VINIL VERDE MCALL',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ40C M2',
            'price' => 10,
            'code' => 'V01',
            'code_visyttex' => 'VINIL IJ40',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ 170 M2',
            'price' => 10,
            'code' => 'V02',
            'code_visyttex' => 'VINIL IJ170',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ 180 M2',
            'price' => 10,
            'code' => 'V03',
            'code_visyttex' => 'VINIL IJ180',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ21-10 M2',
            'price' => 10,
            'code' => 'V04',
            'code_visyttex' => 'VINIL IJ21',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL IMPRESO 230 M2',
            'price' => 9,
            'code' => 'V05',
            'code_visyttex' => 'VINIL 230',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL FUNDIDO 7125 M2',
            'price' => 10,
            'code' => 'V06',
            'code_visyttex' => 'VINIL FUND 7125',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL TRANSPARENTE 231 M2',
            'price' => 10,
            'code' => 'V07',
            'code_visyttex' => 'VINIL TRANSP 231',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'FC 50',
            'price' => 10,
            'code' => 'V08',
            'code_visyttex' => 'VINIL FC50',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL BLANCO MATE PARA IMPRESION ARCLARD M2',
            'price' => 10,
            'code' => 'VI0',
            'code_visyttex' => 'VINIL MATE BLANCO',
        ]);
        factory(\App\Bottom::class, 1)->create([
            'name' => 'VINIL TRANSPARENTE MATE ARCLAD M2',
            'price' => 10,
            'code' => 'VI1',
            'code_visyttex' => 'VINIL TRANS MATE ARCLAD ',
        ]);

        //////////LABELED
        factory(\App\Labeled::class, 1)->create([
            'name' => 'NADA',
            'price' => 00,
            'code' => 'NN',
            'code_visyttex' => 'Nada',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. CHINO  24" X  M2  ',
            'price' => 5,
            'code' => 'I00',
            'code_visyttex' => 'GI BLANCO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI ROJO LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I01',
            'code_visyttex' => 'GI ROJO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI AZUL LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I02',
            'code_visyttex' => 'GI AZUL CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI AMARILLO LAM. REFLEC. CHINO  24" X M2  ',
            'price' => 5,
            'code' => 'I03',
            'code_visyttex' => 'GI AMARILLO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. CHINO   24" X M2 ',
            'price' => 5,
            'code' => 'I04',
            'code_visyttex' => 'GI VERDE CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  NARANJA LAM. REFLEC. CHINO  24" X M2',
            'price' => 5,
            'code' => 'I05',
            'code_visyttex' => 'GI  NARANJA CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI NEGRO LAM. REFLEC. CHINO   24" X M2',
            'price' => 5,
            'code' => 'I06',
            'code_visyttex' => 'GI NEGRO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI CELESTE LAM. REFLEC. CHINO   24" X M2',
            'price' => 5,
            'code' => 'I07',
            'code_visyttex' => 'GI CELESTE CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. 3M 3290 24" X  M2',
            'price' => 16.5,
            'code' => 'I30',
            'code_visyttex' => 'GI BLANCO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI ROJO LAM. REFLEC. 3M  3272 24" X M2',
            'price' => 16.5,
            'code' => 'I31',
            'code_visyttex' => 'GI ROJO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI AZUL LAM. REFLEC. 3M 3275  48" X M2',
            'price' => 16.5,
            'code' => 'I32',
            'code_visyttex' => 'GI AZUL 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI AMARILLO LAM. REFLEC. 3M 3271 24" X M2',
            'price' => 16.5,
            'code' => 'I33',
            'code_visyttex' => 'GI AMARILLO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. 3M 3277 48" X M2',
            'price' => 16.5,
            'code' => 'I34',
            'code_visyttex' => 'GI VERDE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI NARANJA LAM. REFLEC. 3M CW84NL  24" X M2',
            'price' => 16.5,
            'code' => 'I35',
            'code_visyttex' => 'GI NARANJA 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI NEGRO LAM. REFLEC. 3M 580-85  24" X M2',
            'price' => 29.41,
            'code' => 'I36',
            'code_visyttex' => 'GI NEGRO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI BLANCO LAM. REFLEC. 3M 3290 48" X  M2',
            'price' => 16.5,
            'code' => 'I37',
            'code_visyttex' => 'GI BLANCO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI VERDE LAM. REFLEC. 3M 3277 36" X M2',
            'price' => 16.5,
            'code' => 'I38',
            'code_visyttex' => 'GI VERDE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  BLANCO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV0',
            'code_visyttex' => 'GI  BLANCO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  ROJO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV1',
            'code_visyttex' => 'GI  ROJO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  AZUL LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV2',
            'code_visyttex' => 'GI  AZUL VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  AMARILLO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV3',
            'code_visyttex' => 'GI  AMARILLO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  VERDE LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV4',
            'code_visyttex' => 'GI  VERDE VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  NARANJA LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV5',
            'code_visyttex' => 'GI  NARANJA VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  NEGRO LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV6',
            'code_visyttex' => 'GI  NEGRO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GI  CELESTE LAM. REFLEC. VARIOS M2',
            'price' => 5,
            'code' => 'IV7',
            'code_visyttex' => 'GI  CELESTE VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P00',
            'code_visyttex' => 'GIP BLANCO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P01',
            'code_visyttex' => 'GIP ROJO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P02',
            'code_visyttex' => 'GIP AZUL CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P03',
            'code_visyttex' => 'GIP AMARILLO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P04',
            'code_visyttex' => 'GIP VERDE CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P05',
            'code_visyttex' => 'GIP NARANJA CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P06',
            'code_visyttex' => 'GIP NEGRO CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC. CHINO M2  ',
            'price' => 10,
            'code' => 'P07',
            'code_visyttex' => 'GIP CELESTE CHINO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC.3M  3430  M2',
            'price' => 10,
            'code' => 'P30',
            'code_visyttex' => 'GIP BLANCO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC. 3M 3432 M2',
            'price' => 10,
            'code' => 'P31',
            'code_visyttex' => 'GIP ROJO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. 3M  3435 3M M2',
            'price' => 10,
            'code' => 'P32',
            'code_visyttex' => 'GIP AZUL 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. 3M 3431 M2',
            'price' => 10,
            'code' => 'P33',
            'code_visyttex' => 'GIP AMARILLO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. 3M 3437 M2',
            'price' => 10,
            'code' => 'P34',
            'code_visyttex' => 'GIP VERDE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P35',
            'code_visyttex' => 'GIP NARANJA 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P36',
            'code_visyttex' => 'GIP NEGRO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC.3M M2',
            'price' => 10,
            'code' => 'P37',
            'code_visyttex' => 'GIP CELESTE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM0',
            'code_visyttex' => 'GIP BLANCO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC.ENNE M2',
            'price' => 10,
            'code' => 'PM1',
            'code_visyttex' => 'GIP ROJO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM2',
            'code_visyttex' => 'GIP AZUL ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM3',
            'code_visyttex' => 'GIP AMARILLO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM4',
            'code_visyttex' => 'GIP VERDE ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM5',
            'code_visyttex' => 'GIP NARANJA ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP MARRON LAM. REFLEC. ENNE M2',
            'price' => 10,
            'code' => 'PM6',
            'code_visyttex' => 'GIP MARRON ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP BLANCO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV0',
            'code_visyttex' => 'GIP BLANCO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP ROJO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV1',
            'code_visyttex' => 'GIP ROJO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AZUL LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV2',
            'code_visyttex' => 'GIP AZUL VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP AMARILLO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV3',
            'code_visyttex' => 'GIP AMARILLO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP VERDE LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV4',
            'code_visyttex' => 'GIP VERDE VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NARANJA LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV5',
            'code_visyttex' => 'GIP NARANJA VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP NEGRO LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV6',
            'code_visyttex' => 'GIP NEGRO VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'GIP CELESTE LAM. REFLEC.VARIOS  M2',
            'price' => 10,
            'code' => 'PV7',
            'code_visyttex' => 'GIP CELESTE VARIOS',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP BLANCO LAM. REFLEC.3M 3930  M2',
            'price' => 31.13,
            'code' => 'J30',
            'code_visyttex' => 'HIP BLANCO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP ROJO LAM. REFLEC.3M  3932 M2',
            'price' => 31.13,
            'code' => 'J31',
            'code_visyttex' => 'HIP ROJO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP AZUL LAM. REFLEC.3M 3935 M2',
            'price' => 31.13,
            'code' => 'J32',
            'code_visyttex' => 'HIP AZUL 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP AMARILLO LAM. REFLEC.3M 3931 M2',
            'price' => 31.13,
            'code' => 'J33',
            'code_visyttex' => 'HIP AMARILLO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP VERDE LAM. REFLEC.3M 3937M2',
            'price' => 31.13,
            'code' => 'J34',
            'code_visyttex' => 'HIP VERDE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP NARANJA LAM. REFLEC.3M 3934M2',
            'price' => 31.13,
            'code' => 'J35',
            'code_visyttex' => 'HIP NARANJA 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP BLANCO LAM. REFLEC.ENNE RS - 3000 48" X M2',
            'price' => 20.56,
            'code' => 'JM0',
            'code_visyttex' => 'HIP BLANCO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP ROJO LAM. REFLEC.ENNE RS-3005   48" X M2',
            'price' => 20.56,
            'code' => 'JM1',
            'code_visyttex' => 'HIP ROJO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP AZUL LAM. REFLEC.ENNE  RS-3003  48" X M2',
            'price' => 20.56,
            'code' => 'JM2',
            'code_visyttex' => 'HIP AZUL ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP AMARILLO LAM. REFLEC.ENNE RS-3001 48" X M2',
            'price' => 20.56,
            'code' => 'JM3',
            'code_visyttex' => 'HIP AMARILLO ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP VERDE LAM. REFLEC.ENNE. RS-3004 48" X M2',
            'price' => 20.56,
            'code' => 'JM4',
            'code_visyttex' => 'HIP VERDE ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP NARANJA LAM. REFLEC.ENNE. RS-3002  48" X M2',
            'price' => 20.56,
            'code' => 'JM5',
            'code_visyttex' => 'HIP NARANJA ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HI BLANCO FLEXIBLE LAM. REFLEC ENNE RF-3000 48"XM2',
            'price' => 17.54,
            'code' => 'JM6',
            'code_visyttex' => 'HI BLANCO FLEX ENNE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP NARANJA FLUORECENTE LAM. REFLEC.ENNE.  RS-3013 48" X M2',
            'price' => 28.05,
            'code' => 'JM7',
            'code_visyttex' => 'HIP NARANJA REFLEC',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP AMARILLO/LIMON LAM. REFLEC. ENNE RS-3012 48" X M2',
            'price' => 28.05,
            'code' => 'JM8',
            'code_visyttex' => 'HIP AMARILLO/LIMON REFLEC',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'HIP  VERDE FLUORESCENTE LAM. REFLEC. ENNE   M2',
            'price' => 10,
            'code' => 'JM9',
            'code_visyttex' => 'HIP  VERDE REFLEC',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 AZUL LAM. REFLEC. 3M 4095 M2',
            'price' => 10,
            'code' => 'D00',
            'code_visyttex' => 'DG3 AZUL 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 AMARILLO FLUORECENTE LAM. REFLEC. 3M 4081 M2',
            'price' => 58.5,
            'code' => 'D01',
            'code_visyttex' => 'DG3 AMARILLO REFLEC',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 AMARILLO LIMON LAM. REFLEC. 3M 4083 36" X M2',
            'price' => 58.5,
            'code' => 'D02',
            'code_visyttex' => 'DG3 AMARILLO REFLEC',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 NARANJA LAM. REFLEC. 3M 4084 M2',
            'price' => 53.7,
            'code' => 'D03',
            'code_visyttex' => 'DG3 NARANJA 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 BLANCO LAM. REFLEC. 3M 4090 M2',
            'price' => 53.7,
            'code' => 'D04',
            'code_visyttex' => 'DG3 BLANCO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 ROJO LAM. REFLEC. 3M 4092 M2',
            'price' => 53.7,
            'code' => 'D05',
            'code_visyttex' => 'DG3 ROJO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 VERDE LAM. REFLEC. 3M 4097 M2',
            'price' => 53.7,
            'code' => 'D06',
            'code_visyttex' => 'DG3 VERDE 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'DG3 AMARILLO LAM. REFLEC. 3M 4091 M2',
            'price' => 53.7,
            'code' => 'D07',
            'code_visyttex' => 'DG3 AMARILLO 3M',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC KOREANO ROJO',
            'price' => 10,
            'code' => 'E01',
            'code_visyttex' => 'EC KOREANO ROJO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC KOREANO AZUL',
            'price' => 10,
            'code' => 'E02',
            'code_visyttex' => 'EC KOREANO AZUL',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC KOREANO VERDE',
            'price' => 10,
            'code' => 'E04',
            'code_visyttex' => 'EC KOREANO VERDE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC 3M ROJO',
            'price' => 10,
            'code' => 'E31',
            'code_visyttex' => 'EC 3M ROJO',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC 3M AZUL',
            'price' => 10,
            'code' => 'E32',
            'code_visyttex' => 'EC 3M AZUL',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'EC 3M VERDE',
            'price' => 10,
            'code' => 'E34',
            'code_visyttex' => 'EC 3M VERDE',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'FOTOLUMINICENTE 1 HORA M2',
            'price' => 10,
            'code' => 'F01',
            'code_visyttex' => 'FOTOLUMINICENTE 1 HORA M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'FOTOLUMINICENTE 3 HORAS M2',
            'price' => 10,
            'code' => 'F03',
            'code_visyttex' => 'FOTOLUMINICENTE 3 HORAS M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'FOTOLUMINICENTE  5 HORAS M2',
            'price' => 12.5,
            'code' => 'F05',
            'code_visyttex' => 'FOTOLUMINICENTE  5 HORAS M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL PARA ROTULADO CHINO M2',
            'price' => 10,
            'code' => 'R00',
            'code_visyttex' => 'VINIL PARA M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL ROJO MCALL M2',
            'price' => 3.5,
            'code' => 'RM1',
            'code_visyttex' => 'VINIL ROJO MCALL M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL VERDE MCALL M2',
            'price' => 3.5,
            'code' => 'RM2',
            'code_visyttex' => 'VINIL VERDE MCALL M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL PARA ROTULADO ARCLARD M2',
            'price' => 10,
            'code' => 'R02',
            'code_visyttex' => 'VINIL PARA M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL BLANCO MATE PARA IMPRESION ARCLARD M2',
            'price' => 10,
            'code' => 'VI0',
            'code_visyttex' => 'VINIL BLANCO IMPRESION',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL TRANSPARENTE MATE ARCLAD M2',
            'price' => 10,
            'code' => 'VI1',
            'code_visyttex' => 'VINIL TRANSPARENTE M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ40C M2',
            'price' => 10,
            'code' => 'V01',
            'code_visyttex' => 'VINIL IMPRESO IJ40C M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ 170 M2',
            'price' => 10,
            'code' => 'V02',
            'code_visyttex' => 'VINIL IMPRESO M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ 180 M2',
            'price' => 10,
            'code' => 'V03',
            'code_visyttex' => 'VINIL IMPRESO M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL IMPRESO IJ21-10 M2',
            'price' => 10,
            'code' => 'V04',
            'code_visyttex' => 'VINIL IMPRESO M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL IMPRESO 230 M2',
            'price' => 9,
            'code' => 'V05',
            'code_visyttex' => 'VINIL IMPRESO 230 M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL FUNDIDO 7125 M2',
            'price' => 10,
            'code' => 'V06',
            'code_visyttex' => 'VINIL FUNDIDO 7125 M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'VINIL TRANSPARENTE 231 M2',
            'price' => 10,
            'code' => 'V07',
            'code_visyttex' => 'VINIL TRANSPARENTE 231 M2',
        ]);
        factory(\App\Labeled::class, 1)->create([
            'name' => 'FC 50',
            'price' => 5.95,
            'code' => 'V08',
            'code_visyttex' => 'FC 50',
        ]);


        //////////OVER LABELED
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'NADA',
            'price' => 00,
            'code' => 'NN',
            'code_visyttex' => 'NN',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Sobrelaminado 8518 M2',
            'price' => 10,
            'code' => '10',
            'code_visyttex' => 'SOBR 8518',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Sobrelaminado 8519 M2',
            'price' => 10,
            'code' => '12',
            'code_visyttex' => 'SOBR 8519 ',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Sobrelaminado 8520 M2',
            'price' => 10,
            'code' => '14',
            'code_visyttex' => 'SOBR 8520',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Sobrelaminado IJ21-114 M2',
            'price' => 10,
            'code' => '16',
            'code_visyttex' => 'SOBR IJ21-114',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'IM3205 M2',
            'price' => 10,
            'code' => '18',
            'code_visyttex' => 'SOBR IM3205 ',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'General Formulations Sobrelaminado 231 M2',
            'price' => 7,
            'code' => '20',
            'code_visyttex' => 'SOBR FORMUL 231',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Sobrelamiando Arclard M2',
            'price' => 10,
            'code' => '22',
            'code_visyttex' => 'SOBR ARCLAD',
        ]);
        factory(\App\OverLabeled::class, 1)->create([
            'name' => 'Briteline Sobrelaminado OLUV-GLS54( UV, 4 y) M2',
            'price' => 10,
            'code' => '24',
            'code_visyttex' => 'SOBR BRITELINE',
        ]);



        //////////ADDITIONAL
        factory(\App\Additional::class, 1)->create([
            'name' => 'NADA',
            'price' => 00,
            'code' => 'NN',
            'code_visyttex' => 'NN',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'TROQUELADO M2',
            'price' => 10,
            'code' => 'TR',
            'code_visyttex' => 'TROQ',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'PERFORACIONES M2',
            'price' => 10,
            'code' => 'PF',
            'code_visyttex' => 'PERF',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'BAÑO ACERADO M2',
            'price' => 10,
            'code' => 'CH',
            'code_visyttex' => 'BAÑ ACE',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'DOS CARAS M2',
            'price' => 10,
            'code' => '2C',
            'code_visyttex' => 'DOS CARAS',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'TRES CARAS M2',
            'price' => 10,
            'code' => '3C',
            'code_visyttex' => 'TRES CARAS',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'POSTE M2',
            'price' => 10,
            'code' => 'PO',
            'code_visyttex' => 'POSTE',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'MARCO M2',
            'price' => 10,
            'code' => 'MA',
            'code_visyttex' => 'MARCO',
        ]);
        factory(\App\Additional::class, 1)->create([
            'name' => 'CABALLETE M2',
            'price' => 10,
            'code' => 'CA',
            'code_visyttex' => 'CABALLETE',
        ]);



        //////////MEASURE
        factory(\App\Measure::class, 1)->create([
            'height' => 20,
            'width' => 30,
            'code' => 'E01',
        ]);
        factory(\App\Measure::class, 1)->create([
            'height' => 40,
            'width' => 60,
            'code' => 'E02',
        ]);
        factory(\App\Measure::class, 1)->create([
            'height' => 60,
            'width' => 90,
            'code' => 'E03',
        ]);
        factory(\App\Measure::class, 1)->create([
            'height' => 80,
            'width' => 120,
            'code' => 'E04',
        ]);
        factory(\App\Measure::class, 1)->create([
            'height' => 20,
            'width' => 20,
            'code' => 'E05',
        ]);

        //////////MOD
        factory(\App\Mod::class, 1)->create([
            'name' => 'Ninguno',
            'amount' => 0,
            'type' => \App\Mod::BASE,
            'work_id' => 1,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'base',
            'amount' => 2.50,
            'type' => \App\Mod::BASE,
            'work_id' => 2,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'sticker',
            'amount' => 2.00,
            'type' => \App\Mod::STICKER,
            'work_id' => 2,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'base',
            'amount' => 4.00,
            'type' => \App\Mod::BASE,
            'work_id' => 3,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'sticker',
            'amount' => 3.00,
            'type' => \App\Mod::STICKER,
            'work_id' => 3,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'base',
            'amount' => 2.50,
            'type' => \App\Mod::BASE,
            'work_id' => 4,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'sticker',
            'amount' => 2.00,
            'type' => \App\Mod::STICKER,
            'work_id' => 4,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'base',
            'amount' => 2.50,
            'type' => \App\Mod::BASE,
            'work_id' => 5,
        ]);
        factory(\App\Mod::class, 1)->create([
            'name' => 'sticker',
            'amount' => 2.00,
            'type' => \App\Mod::STICKER,
            'work_id' => 5,
        ]);

        //////////CURRENCIES
        factory(\App\Currency::class, 1)->create([
            'name' => 'Dólares',
            'symbol' => '$',
            'currency_code' => 'USD',
        ]);
        factory(\App\Currency::class, 1)->create([
            'name' => 'Soles',
            'symbol' => 'S/',
            'currency_code' => 'PEN',
        ]);

        //////////SIGNAL TYPE
        ///
        /// SEÑALES VIALES
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Reglamentarias',
            'description' => 'Las señales reglamentarias tienen por finalidad notificar a los usuarios de las vías las prioridades en el uso de las mismas.',
            'picture' => 'reglamentarias.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Preventivas',
            'description' => 'Las señales preventivas, denominadas además de advertencia de peligro, tienen como propósito advertir a los usuarios.',
            'picture' => 'preventivas.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Informativas',
            'description' => 'Tienen por objeto guiar al usuario de la vía, suministrandole información diversa.',
            'picture' => 'informativas.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'De Obra',
            'description' => 'Estas señales son temporales y sirven para informar a vehículos y peatones que hay una obra cerca e indicarles las rutas alternas.',
            'picture' => 'de_obra.png',
        ]);
        ///
        /// SEÑALES DE SEGURIDAD
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Equipo contra incendios',
            'description' => 'Las señales de equipos de lucha contra incendios, nos indican la situación de alguno de los dispositivos de extinción.',
            'picture' => 'contra_incendios.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Advertencia',
            'description' => 'as señales de advertencia , indican a los usuarios proximidad y la naturaleza de un peligro difícil de ser percibido a tiempo.',
            'picture' => 'advertencia.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Obligación',
            'description' => 'La señal de obligación es una señal en forma de panel, o una señal luminosa, que obliga a un comportamiento determinado.',
            'picture' => 'obligacion.png',
        ]);
        factory(\App\SignalType::class, 1)->create([
            'name' => 'Evacuación y emergencia',
            'description' => 'Las señales de evacuación guían en los recorridos de las personas que se encuentran en una situación de emergencia.',
            'picture' => 'evacuacion.png',
        ]);

        //////////SIGNALS
        ///REGLAMENTARIAS
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Pare',
            'picture' => 'pare.png',
            'code' => 'R-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ceda el paso',
            'picture' => 'ceda_el_paso.png',
            'code' => 'R-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Dirección obligatoria',
            'picture' => 'direccion_obligatoria.png',
            'code' => 'R-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'No entre',
            'picture' => 'no_entre.png',
            'code' => 'R-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Giro solamente a la izquierda',
            'picture' => 'giro_solamente_a_la_izquierda.png',
            'code' => 'R-5',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Carril exclusivo para volteo obligado a la izquierda',
            'picture' => 'carril_exclusivo_para_volteo_obligado_a_la_izquierda.png',
            'code' => 'R5-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Carril permitido para volteo y para seguir de frente',
            'picture' => 'carril_permitido_para_volteo_y_para_seguir_de_frente.png',
            'code' => 'R5-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Carril exclusivo para volteo obligado y carril de volteo con seguir de frente',
            'picture' => 'carril_exclusivo_para_volteo_obligado_y_carril_de_volteo_con_seguir_de_frente.png',
            'code' => 'R5-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Volteo a la izquierda en ambos sentidos',
            'picture' => 'volteo_a_la_izquierda_en_ambos_sentidos.png',
            'code' => 'R5-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido voltear a la izquierda',
            'picture' => 'prohibido_voltear_a_la_izquierda.png',
            'code' => 'R-6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido girar con luz roja a la izquierda',
            'picture' => 'prohibido_girar_con_luz_roja_a_la_izquierda.png',
            'code' => 'R6-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Giro solamente a la derecha',
            'picture' => 'giro_solamente_a_la_derecha.png',
            'code' => 'R-7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido voltear a la derecha',
            'picture' => 'prohibido_voltear_a_la_derecha.png',
            'code' => 'R-8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido girar a la derecha con luz roja',
            'picture' => 'prohibido_girar_a_la_derecha_con_luz_roja.png',
            'code' => 'R8-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Giro solamente en U',
            'picture' => 'giro_solamente_en_u.png',
            'code' => 'R-9',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido voltear en U',
            'picture' => 'prohibido_voltear_en_u.png',
            'code' => 'R-10',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación en ambos sentidos',
            'picture' => 'circulacion_en_ambos_sentidos.png',
            'code' => 'R-11',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación en tres carriles, uno en contraflujo',
            'picture' => 'circulacion_en_tres_carriles_uno_en_contraflujo.png',
            'code' => 'R11-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación en tres carriles, dos en contraflujo',
            'picture' => 'circulacion_en_tres_carriles_dos_en_contraflujo.png',
            'code' => 'R11-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido cambiar de carril',
            'picture' => 'prohibido_cambiar_de_carril.png',
            'code' => 'R-12',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación obligatoria',
            'picture' => 'circulacion_obligatoria.png',
            'code' => 'R-14',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Tránsito en un sentido',
            'picture' => 'transito_en_un_sentido.png',
            'code' => 'R14-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Tránsito en ambos sentidos',
            'picture' => 'transito_en_ambos_sentidos.png',
            'code' => 'R14-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido adelantar',
            'picture' => 'prohibido_adelantar.png',
            'code' => 'R-16',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Fin de zona de prohibido adelantar',
            'picture' => 'fin_de_zona_de_prohibido_adelantar.png',
            'code' => 'R16-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de vehículos automotores',
            'picture' => 'prohibido_circulacion_de_vehiculos_automotores.png',
            'code' => 'R-17',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Vehículos pesados a la derecha',
            'picture' => 'vehiculos_pesados_a_la_derecha.png',
            'code' => 'R-18',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de vehículos de carga',
            'picture' => 'prohibido_circulacion_de_vehiculos_de_carga.png',
            'code' => 'R-19',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Peatones deben circular por la izquierda',
            'picture' => 'peatones_deben_circular_por_la_izquierda.png',
            'code' => 'R-20',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido el paso y/o la circulación de peatones',
            'picture' => 'prohibido_el_paso_y_o_la_circulacion_de_peatones.png',
            'code' => 'R-21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido la circulación de bicicletas y motociclos',
            'picture' => 'prohibido_la_circulacion_de_bicicletas_y_motociclos.png',
            'code' => 'R-22',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido la circulación de triciclos',
            'picture' => 'prohibido_la_circulacion_de_triciclos.png',
            'code' => 'R22-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido la circulación de motocicletas',
            'picture' => 'prohibido_la_circulacion_de_motocicletas.png',
            'code' => 'R-23',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'No tractores',
            'picture' => 'no_tractores.png',
            'code' => 'R-24',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de vehículos de tracción animal',
            'picture' => 'prohibido_circulacion_de_vehiculos_de_traccion_animal.png',
            'code' => 'R-25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de carretillas manuales',
            'picture' => 'prohibido_circulacion_de_carretillas_manuales.png',
            'code' => 'R25-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de cuatrimotos',
            'picture' => 'prohibido_circulacion_de_cuatrimotos.png',
            'code' => 'R25-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de buses',
            'picture' => 'prohibido_circulacion_de_buses.png',
            'code' => 'R25-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de cuatrimotos',
            'picture' => 'pohibido_circulacion_de_cuatrimotos.png',
            'code' => 'R25-D',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Permitido estacionar',
            'picture' => 'permitido_estacionar.png',
            'code' => 'R-26',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido estacionar',
            'picture' => 'prohibido_estacionar.png',
            'code' => 'R-27',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido estacionar zona de remolque',
            'picture' => 'prohibido_estacionar_zona_de_remolque.png',
            'code' => 'R27-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido detenerse',
            'picture' => 'prohibido_detenerse.png',
            'code' => 'R-28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido el uso de la bocina',
            'picture' => 'prohibido_el_uso_de_la_bocina.png',
            'code' => 'R-29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida 40 Km/H',
            'picture' => 'velocidad_maxima_permitida_40_km_h.png',
            'code' => 'R30-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida 100 Km/H',
            'picture' => 'velocidad_maxima_permitida_100_km_h.png',
            'code' => 'R30-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad mínima permitida 60 Km/H',
            'picture' => 'velocidad_minima_permitida_60_km_h.png',
            'code' => 'R30-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida de salida 50 Km/H',
            'picture' => 'velocidad_maxima_permitida_de_salida_50_km_h.png',
            'code' => 'R30-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida para camión 80 Km/H',
            'picture' => 'velocidad_maxima_permitida_para_camion_80_km_h.png',
            'code' => 'R30-D',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida para bus 90 Km/H',
            'picture' => 'velocidad_maxima_permitida_para_bus_90_km_h.png',
            'code' => 'R30-E',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima permitida en curva 40 Km/H',
            'picture' => 'velocidad_maxima_permitida_en_curva_40_km_h.png',
            'code' => 'R30-F',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Velocidad máxima según tipo de vehículo',
            'picture' => 'velocidad_maxima_segun_tipo_de_vehiculo.png',
            'code' => 'R30-G',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Peso máximo permitido por eje',
            'picture' => 'peso_maximo_permitido_por_eje.png',
            'code' => 'R-31',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Peso máximo bruto permitido por vehículo',
            'picture' => 'peso_maximo_bruto_permitido_por_vehiculo.png',
            'code' => 'R-32',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Largo máximo permitido',
            'picture' => 'largo_maximo_permitido.png',
            'code' => 'R-33',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación sólo de buses',
            'code' => 'R-34',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Altura máxima permitida',
            'code' => 'R-35',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ancho máximo permitido',
            'code' => 'R-36',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'CONTROL',
            'code' => 'R-37',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Circulación con luces bajas',
            'code' => 'R-40',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ciclovía',
            'code' => 'R-42',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ciclovía Converse la derecha',
            'code' => 'R42-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ciclovía Obligatorio descender de la bicicleta',
            'code' => 'R42-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Ciclovía Circulación no compartida Bicicleta - Peatón',
            'code' => 'R42-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Uso obligatorio de cadenas',
            'code' => 'R-43',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Paradero prohibido',
            'code' => 'R-44',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de mototaxis',
            'code' => 'R-45',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido circulación de motocarga',
            'code' => 'R45-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Paradero',
            'code' => 'R-47',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Zona de carga y descarga',
            'code' => 'R-48',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Mantener distancia de seguridad',
            'code' => 'R-49',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Preferencia al sentido contrario',
            'code' => 'R-50',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Prohibido la carga y descarga',
            'code' => 'R-52',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'No bloquear cruce',
            'code' => 'R-53',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Sólo motocicletas',
            'code' => 'R-54',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Sólo mototaxi',
            'code' => 'R54-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Sólo motocarga',
            'code' => 'R54-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Vía segregada para buses',
            'code' => 'R55-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Vía segregada para buses',
            'code' => 'R55-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Sólo transporte público',
            'code' => 'R-56',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Vía segregada motorizados bicicletas',
            'code' => 'R58-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Vía segregada motorizados bicicletas',
            'code' => 'R58-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Estacionamiento sólo taxis',
            'code' => 'R-62',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Permitido girar con luz roja',
            'code' => 'R64-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Permitido girar con luz roja',
            'code' => 'R54-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 1,
            'name' => 'Señal Reglamentaria Personalizada',
            'code' => 'R-PE',
        ]);


        ///PREVENTIVAS
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva pronunciada a la derecha',
            'code' => 'P1-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva pronunciada a la izquierda',
            'code' => 'P1-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva a la derecha',
            'code' => 'P2-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva a la izquierda',
            'code' => 'P2-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva y contracurva pronunciada a la derecha',
            'code' => 'P3-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva y contracurva pronunciada a la izquierda',
            'code' => 'P3-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva y contracurva pronunciada a la derecha',
            'code' => 'P4-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva y contracurva pronunciada a la izquierda',
            'code' => 'P4-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Camino siunuoso a la derecha',
            'code' => 'P5-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Camino siunuoso a la izquierda',
            'code' => 'P5-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva en U a la derecha',
            'code' => 'P5-2A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva en U a la izquierda',
            'code' => 'P5-2B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce de vías a nivel',
            'code' => 'P6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Intersección escalonada primera derecha',
            'code' => 'P6-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Intersección escalonada primera izquierda',
            'code' => 'P6-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Intersección en T',
            'code' => 'P7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Intersección en Y',
            'code' => 'P8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Empate en ángulo recto con vía lateral a la derecha',
            'code' => 'P9-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Curva y contracurva pronunciada a la izquierda',
            'code' => 'P9-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Empalme en ángulo agudo a la derecha',
            'code' => 'P10-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Empalme en ángulo agudo a la izquierda',
            'code' => 'P10-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Intersección rotatoria',
            'code' => 'P15',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Incorporación de tránsito a la derecha',
            'code' => 'P16-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Incorporación de tránsito a la izquierda',
            'code' => 'P16-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Refucción de calzada en ambos lados',
            'code' => 'P17-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Reducción de calzada al lado derecho',
            'code' => 'P17-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Reducción de calzada al lado izquierdo',
            'code' => 'P17-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Reducción del carril externo al lado derecho',
            'code' => 'P18-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Reducción del carril externo al lado izquierdo',
            'code' => 'P18-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ensanchamiento de calzada en ambos lados',
            'code' => 'P21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ensanchamiento de calzada a la derecha',
            'code' => 'P21-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ensanchamiento de calzada a la iquierda',
            'code' => 'P21-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Carril adicional',
            'code' => 'P22-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Dos sentidos de tránsito',
            'code' => 'P25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Tres carriles (uno en contraflujo)',
            'code' => 'P25-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Tres carriles (dos en contraflujo)',
            'code' => 'P25-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Iniciación de vía con separador (dos sentidos)',
            'code' => 'P28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Iniciación de vía con separador (un sentido)',
            'code' => 'P28-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Iniciación de vía con separador (dos sentidos)',
            'code' => 'P29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Iniciación de vía con separador (un sentido)',
            'code' => 'P29-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Final de la vía pavimentada',
            'code' => 'P31',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Final de la vía',
            'code' => 'P31-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad reductor de velocidad tipo resalto',
            'code' => 'P33-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ubicación de reductor de velocidad tipo resalto',
            'code' => 'P33-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad de baden',
            'code' => 'P34',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ubicación de baden',
            'code' => 'P34-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Fuerte pendiente en descenso',
            'code' => 'P35',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Fuerte pendiente en ascenso',
            'code' => 'P35-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Superficie deslizante',
            'code' => 'P36',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Altura máxima permitida',
            'code' => 'P38',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Túnel',
            'code' => 'P41',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce ferroviario a nivel sin barreras',
            'code' => 'P42',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce ferroviario a nivel con barreras',
            'code' => 'P43',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce ferroviario a nivel con barreras',
            'code' => 'P44',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce ferroviario a nivel No tocar pito',
            'code' => 'P44-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Vuelo de aviones a baja altura',
            'code' => 'P45',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ciclistas en la vía',
            'code' => 'P46',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce de ciclistas',
            'code' => 'P46-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ubicación de cruce de ciclistas',
            'code' => 'P46-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Vehículos en la ciclovía',
            'code' => 'P46-C',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Zona de presencia de peatones',
            'code' => 'P48',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad cruce peatonal',
            'code' => 'P48-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Cruce peatonal',
            'code' => 'P48-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Zona escolar',
            'code' => 'P49',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad a zona escolar',
            'code' => 'P49-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ubicación de zona escolar',
            'code' => 'P49-B',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Niños jugando',
            'code' => 'P50',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Maquinaria agrícola en la vía',
            'code' => 'P51',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Salida de vehículos de bomberos',
            'code' => 'P52',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Animales en la vía',
            'code' => 'P53',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad a semádoro',
            'code' => 'P55',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Zona urbana',
            'code' => 'P57',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad a pare',
            'code' => 'P58',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Proximidad a ceda el paso',
            'code' => 'P59',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Zona escolar',
            'code' => 'P60',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Delineador de curva horizontal Chevron',
            'code' => 'P61',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Peso bruto máximo permitido',
            'code' => 'P62',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Ráfaga de viento lateral',
            'code' => 'P66',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Zona de arenamiento en la vía',
            'code' => 'P66-A',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 2,
            'name' => 'Señal Preventiva Personalizada',
            'code' => 'P-PE',
        ]);


        ///INFORMATIVAS
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 3,
            'name' => 'Señal Informativa Personalizada',
            'code' => 'I-PE',
        ]);

        ///DE OBRA
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 4,
            'name' => 'Señal de Obra Personalizada',
            'code' => 'O-PE',
        ]);



        ///EQUIPO CONTRA INCENDIOS
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor',
            'code' => 'ECI-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor derecha',
            'code' => 'ECI-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor izquierda',
            'code' => 'ECI-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor polvo químico seco PQS',
            'code' => 'ECI-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor gas carbónico CO2',
            'code' => 'ECI-5',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor agua presurizada HO2',
            'code' => 'ECI-6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor uso exclusivo metales combustibles clase D',
            'code' => 'ECI-7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor químico húmedo clase K',
            'code' => 'ECI-8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor rodante',
            'code' => 'ECI-9',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Manguera contra incendios',
            'code' => 'ECI-10',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Mangueras de incendios derecha',
            'code' => 'ECI-11',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Mangueras de incendios izquierda',
            'code' => 'ECI-12',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Hidrante',
            'code' => 'ECI-13',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Alarma contra incendios',
            'code' => 'ECI-14',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Avisador sonoro',
            'code' => 'ECI-15',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Teléfono de emergencia',
            'code' => 'ECI-16',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Escalera portátil',
            'code' => 'ECI-17',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Arena',
            'code' => 'ECI-18',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Arena para casos de incendio',
            'code' => 'ECI-19',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Manta apaga fuegos',
            'code' => 'ECI-20',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Puerta contra fuego',
            'code' => 'ECI-21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Equipo autónomo contra incendios',
            'code' => 'ECI-22',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Conexión siamesa para rociadores automáticos',
            'code' => 'ECI-23',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Conexión sencilla para rociadores automáticos',
            'code' => 'ECI-24',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Válvula de control para rociadores automáticos',
            'code' => 'ECI-25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Conexión para sistema contra incendio',
            'code' => 'ECI-26',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Conexión combinada para rociadores automáticos y sistemas de gabinete',
            'code' => 'ECI-27',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Válvula para corte de gas',
            'code' => 'ECI-28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Panel eléctrico para cierre de energía',
            'code' => 'ECI-29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Use la escalera en caso de incendio izquierda hacia arriba',
            'code' => 'ECI-30',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Use la escalera en caso de incendio derecha hacia arriba',
            'code' => 'ECI-31',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Use la escalera en caso de incendio derecha hacia abajo',
            'code' => 'ECI-32',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Use la escalera en caso de incendio izquierda hacia abajo',
            'code' => 'ECI-33',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Salida de emergencia',
            'code' => 'ECI-34',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Salida de emergencia',
            'code' => 'ECI-35',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Salida de emergencia derecha',
            'code' => 'ECI-36',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Salida de emergencia izquierda',
            'code' => 'ECI-37',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Salida accesible de emergencia',
            'code' => 'ECI-38',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Uso exclusivo de bomberos',
            'code' => 'ECI-39',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor',
            'code' => 'ECI-40',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Extintor rodante',
            'code' => 'ECI-41',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Manguera contra incendios',
            'code' => 'ECI-42',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Teléfono de emergencia',
            'code' => 'ECI-43',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Alarma contra incendios',
            'code' => 'ECI-44',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Avisador sonoro',
            'code' => 'ECI-45',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Válvula para corte de gas',
            'code' => 'ECI-46',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Panel eléctrico para cierre de energía',
            'code' => 'ECI-47',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 5,
            'name' => 'Señal de Equipo Contra Incendios Personalizada',
            'code' => 'ECI-PE',
        ]);


        ///ADVERTENCIA
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo eléctrico',
            'code' => 'AD-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Peligro de muerte alto voltaje',
            'code' => 'AD-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Riesgo de descargas eléctricas',
            'code' => 'AD-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Sustancias o materias tóxicas',
            'code' => 'AD-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Peligro de muerte',
            'code' => 'AD-5',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Sustancias o materiales inflamables',
            'code' => 'AD-6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Peligro inflamable',
            'code' => 'AD-7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Carga suspendida en altura',
            'code' => 'AD-8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Radiaciones no ionizantes',
            'code' => 'AD-9',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Frecuencia de radio',
            'code' => 'AD-10',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado con sus manos',
            'code' => 'AD-11',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Peligro ácido corrosivo',
            'code' => 'AD-12',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado piso mojado',
            'code' => 'AD-13',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado piso resbalozo',
            'code' => 'AD-14',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo de radiación',
            'code' => 'AD-15',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención peligro de obstáculos',
            'code' => 'AD-16',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo biológico',
            'code' => 'AD-17',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención baja temperatura',
            'code' => 'AD-18',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo de accidentes',
            'code' => 'AD-19',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención material explosivo',
            'code' => 'AD-20',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo de explosión',
            'code' => 'AD-21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención agente oxidante',
            'code' => 'AD-22',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención campo magnético potente',
            'code' => 'AD-23',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención radiación láser',
            'code' => 'AD-24',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado superficie caliente',
            'code' => 'AD-25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado tránsito de montacargas',
            'code' => 'AD-26',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado balones de gas',
            'code' => 'AD-27',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado riesgo de ser aplastado',
            'code' => 'AD-28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado arranque automático',
            'code' => 'AD-29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado caída de objetos',
            'code' => 'AD-30',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado grúas trabajando',
            'code' => 'AD-31',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado con el perro',
            'code' => 'AD-32',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado gas comprimido',
            'code' => 'AD-33',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado hombres trabajando',
            'code' => 'AD-34',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención riesgo de caída de rocas',
            'code' => 'AD-35',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención caída de objetos',
            'code' => 'AD-36',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado atmósfera explosiva',
            'code' => 'AD-37',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Atención peligro de caídas',
            'code' => 'AD-38',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Cuidado riesgo de asfixia',
            'code' => 'AD-39',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 6,
            'name' => 'Señal de Advertencia Personalizada',
            'code' => 'AD-PE',
        ]);


        ///OBLIGACION
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de casco de seguridad',
            'code' => 'OB-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de protección auditiva',
            'code' => 'OB-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de botas de seguridad',
            'code' => 'OB-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de botas aislantes',
            'code' => 'OB-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de máscara de soldar',
            'code' => 'OB-5',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de guantes de seguridad',
            'code' => 'OB-6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de guantes aislantes',
            'code' => 'OB-7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de protección ocular',
            'code' => 'OB-8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de mascarilla',
            'code' => 'OB-9',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de protector facial',
            'code' => 'OB-10',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de arnés de seguridad',
            'code' => 'OB-11',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de traje de seguridad',
            'code' => 'OB-12',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de protección auditiva y máscara de gas',
            'code' => 'OB-13',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de casco y protección auditiva',
            'code' => 'OB-14',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de casco y lentes de seguridad',
            'code' => 'OB-15',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de protección ocular y auditiva',
            'code' => 'OB-16',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de máscaras de gas, protección auditiva y casco',
            'code' => 'OB-17',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de casco, protección auditiva y ocular',
            'code' => 'OB-18',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de máscara de gas',
            'code' => 'OB-19',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de casco de seguridad y máscara de gas',
            'code' => 'OB-20',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de equipo de aire autocontenido',
            'code' => 'OB-21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio mantener sujetados los cilindros',
            'code' => 'OB-22',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Es obligatorio lavarse las manos',
            'code' => 'OB-23',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Es obligatorio asegurar después de utilizar',
            'code' => 'OB-24',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Es obligatorio desconectar después de utilizar',
            'code' => 'OB-25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Es obligatorio tocas la bocina antes de traspasar',
            'code' => 'OB-26',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio del gorro',
            'code' => 'OB-27',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de mascarilla y gorro',
            'code' => 'OB-28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Uso obligatorio de mandil y manguitos',
            'code' => 'OB-29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Usar el pasamano',
            'code' => 'OB-30',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 7,
            'name' => 'Señal de Obligación Personalizada',
            'code' => 'OB-PE',
        ]);


        ///EVACUACION Y EMERGENCIA
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Zona segura en caso de sismos',
            'code' => 'EYE-1',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida',
            'code' => 'EYE-2',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de emergencia',
            'code' => 'EYE-3',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de socorro deslizar a la derecha para abrir',
            'code' => 'EYE-4',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de socorro deslizar a la izquierda para abrir',
            'code' => 'EYE-5',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de emergencia (salida del recinto)',
            'code' => 'EYE-6',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de emergencia (salida del recinto)',
            'code' => 'EYE-7',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Punto de reunión en caso de emergencia',
            'code' => 'EYE-8',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de socorro presionar la barra para abrir',
            'code' => 'EYE-9',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida de socorro empujar para abrir',
            'code' => 'EYE-10',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida minusválidos',
            'code' => 'EYE-11',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválidos',
            'code' => 'EYE-12',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-13',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-14',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-15',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-16',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-17',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-18',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación minusválido',
            'code' => 'EYE-19',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-20',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-21',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-22',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-23',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-24',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación minusválidos',
            'code' => 'EYE-25',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-26',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-27',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-28',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-29',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-30',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-31',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evación',
            'code' => 'EYE-32',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Lavaojos de emergencia',
            'code' => 'EYE-33',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ducha de emergencia',
            'code' => 'EYE-34',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Camilla de emergencia',
            'code' => 'EYE-35',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Teléfono de emergencia',
            'code' => 'EYE-36',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Primeros auxilios',
            'code' => 'EYE-37',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Sala de primeros auxilios',
            'code' => 'EYE-38',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Enfermería',
            'code' => 'EYE-39',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Empuje para abrir',
            'code' => 'EYE-40',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Jale para abrir',
            'code' => 'EYE-41',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Zona segura en caso de sismos',
            'code' => 'EYE-42',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Salida',
            'code' => 'EYE-43',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-44',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-45',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-46',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-47',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-48',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ruta de evacuación',
            'code' => 'EYE-49',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Lavaojos de emergencia',
            'code' => 'EYE-50',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Ducha de emergencia',
            'code' => 'EYE-51',
        ]);
        factory(\App\Signal::class, 1)->create([
            'signal_type_id' => 8,
            'name' => 'Señal de Evacuación y Emergencia Personalizada',
            'code' => 'EYE-PE',
        ]);

        //////////PROFORMAS & QUOTATIONS
        factory(\App\Proforma::class, 10)
            ->create()
            ->each(function (\App\Proforma $pro) {
            factory(\App\Quotation::class, 5)
                ->create([
                    'proforma_id'=>$pro->id,
                ]);
        });

        //////////ORDERS
        factory(\App\Order::class, 10)->create();


    }
}
